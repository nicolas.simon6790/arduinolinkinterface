/**
 * 
 */
package arduinoTools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Classe utilisée pour créer un fichier CSV
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 19:45:36     2021 : integration-sonar 
 * Mon Nov 29 22:33:41     2021 : 59-enregistrement-de-la-valeur-des-capteurs 
 * 
 *
 */
public class CSVWriter {

	/**
	 * Référence vers l'objet File
	 */
	private FileWriter csvFile;

	/**
	 * Nom et chemin du fichier
	 */
	private String filePath;

	/**
	 * Indique si la première ligne a été écrite ou non
	 */
	private boolean isFirstLineWritten;

	/**
	 * Constructeur de la classe
	 * 
	 * @param path     Chemin du fichier CSV
	 * @param fileName Nom du fichier CSV
	 * @throws CsvWriterFileException
	 */
	public CSVWriter(String path, String fileName) throws CsvWriterFileException {
		this.filePath = arduinoTools.SystemTools.getCurJARPath() + "/" + path;
		File rep = new File(filePath);

		if (!rep.exists())
			rep.mkdir();

		filePath = filePath + "/" + fileName;

		/* Créer fichier CSV */
		try {
			csvFile = new FileWriter(new File(filePath), false);
		} catch (IOException e) {
			throw new CsvWriterFileException("\nErreur lors de l'ouverture du fichier " + filePath);
		}

		isFirstLineWritten = false;
	}

	/**
	 * Ferme le fichier CSV
	 * 
	 * @throws CsvWriterFileException Exception levée en cas de problème lors de la
	 *                                fermeture du fichier
	 */
	public void close() throws CsvWriterFileException {

		/* Fermeture du fichier */
		try {
			csvFile.close();
		} catch (IOException e) {
			throw new CsvWriterFileException("\nErreur lors de la fermeture du fichier " + this.filePath);
		}
	}

	/**
	 * Ecrit la ligne de titre, contenant les noms de colonens
	 * 
	 * @param items Liste contenant les titres de colonnes
	 * @throws CsvWriterFileException
	 */
	public void createTitleLine(List<String> titleList) throws CsvWriterFileException {
		if (!isFirstLineWritten) {
			String line = "";
			for (int i = 0; i < titleList.size(); i++)
				line = line.concat(titleList.get(i)).concat(";");

			line = line.concat("\n");

			try {
				csvFile.append(line);
			} catch (IOException e) {
				throw new CsvWriterFileException("\nErreur lors de l'ecriture dans le fichier " + this.filePath);
			}
			isFirstLineWritten = true;
		}
	}

	/**
	 * Ajoute une nouvelle ligne dans le fichier CSV
	 * 
	 * @param data Tableau contenant les données de la ligne à ajouter
	 * @throws CsvWriterFileException
	 */
	public void addLine(String[] data) throws CsvWriterFileException {
		if (isFirstLineWritten) {
			String line = "";
			for (int i = 0; i < data.length; i++)
				line = line.concat(data[i]).concat(";");

			line += "\n";

			try {
				csvFile.append(line);
			} catch (IOException e) {
				throw new CsvWriterFileException("\nErreur lors de l'ecriture dans le fichier " + this.filePath);
			}
		}
	}


	/**
	 * Classe gérant les exceptions levées lors de l'ouverture ou la fermeture du
	 * fichier CSV
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 19:45:36     2021 : integration-sonar 
 * Mon Nov 29 22:33:41     2021 : 59-enregistrement-de-la-valeur-des-capteurs 
 * 
	 * 
	 */
	public class CsvWriterFileException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Constructeur
		 * 
		 * @param s Nom du fichier CSV
		 */
		public CsvWriterFileException(String s) {
			super(s);
		}

	}
}
