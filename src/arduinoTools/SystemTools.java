package arduinoTools;

import java.io.File;
import java.net.URISyntaxException;

import arduinoConsoleInterface.Main;

/**
 * Classe contenant des fonctions utiles aux autres classes
 * 
 * @date Wed Oct 27 18:30:14     2021
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 20:48:49     2021 : integration-sonar 
 * Wed Oct 27 18:30:14     2021 : 41-corrections-affichage-2 
 * 
 *
 */
public class SystemTools {

	private static String OS = System.getProperty("os.name").toLowerCase();
	private static final boolean IS_WINDOWS = (OS.indexOf("win") >= 0);
	private static final boolean IS_LINUX = OS.indexOf("linux") >= 0;

	private SystemTools() {
	}

	/**
	 * Renvoie le répertoire contenant le fichier JAR
	 * 
	 * @return Chemin du répertoire
	 */
	public static String getCurJARPath() {
		String str = null;
		try {
			str = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();

			if (IS_WINDOWS)
				str = str.substring(0, str.lastIndexOf("\\"));
			else if (IS_LINUX)
				str = str.substring(0, str.lastIndexOf("/"));

		} catch (URISyntaxException e) {
			System.out.println(e.getMessage());
			return null;
		}

		return str;
	}

	/**
	 * Vérifie si la string d'entrée est un nombre ou non
	 * 
	 * @param str String à vérifier
	 * @return TRUE si c'est un nombre, FALSE sinon
	 */
	public static boolean isNumeric(String str) {
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return false;
		}

		return true;
	}


}
