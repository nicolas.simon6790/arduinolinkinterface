/**
 * 
 */
package arduinoTools;

import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;

/**
 * Classe statique fournissant une interface pour logger les actions du
 * programmme. Le log est effectué par le singleton LogManager.
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 19:45:38     2021 : integration-sonar 
 * 
 *
 */
public class Logger {

	public enum LogLevel {
		VERB, INFO, WARNING, ERROR
	}

	/**
	 * Référence statique vers le logManager
	 */
	private static LogManager manager = new LogManager();


	/**
	 * Ecrit un texte dans le fichier de log. Le nom de la classe appelante est
	 * définit dynamiquement.
	 * 
	 * @param instance Type de la classe appelante
	 * @param lvl      Niveau d'information
	 * @param text     Texte à logger
	 */
	@SuppressWarnings("rawtypes")
	public static void write(Class instance, LogLevel lvl, String text) {

		if (getMinLogLvlDisplayed() == LogLevel.VERB)
			manager.write(instance.getName(), lvl, text);
		else if (getMinLogLvlDisplayed() == LogLevel.INFO) {
			if (lvl != LogLevel.VERB)
				manager.write(instance.getName(), lvl, text);
		}
		else if (getMinLogLvlDisplayed() == LogLevel.WARNING) {
			if (lvl == LogLevel.WARNING || lvl == LogLevel.ERROR)
				manager.write(instance.getName(), lvl, text);
		}
		else if (lvl == LogLevel.ERROR)
			manager.write(instance.getName(), lvl, text);

	}

	private static LogLevel getMinLogLvlDisplayed() {
		String levelRead;
		try {
			levelRead = ArduinoConfigLoader.getConfLoader().getConfigValue("MinLogLvlDisplayed");
		} catch (ArduinoConfigLoaderException e) {
			return LogLevel.INFO;
		}

		if (levelRead.equals("ERROR"))
			return LogLevel.ERROR;
		else if (levelRead.equals("VERB"))
			return LogLevel.VERB;
		else if (levelRead.equals("WARNING"))
			return LogLevel.WARNING;
		else
			return LogLevel.INFO;
	}

	/**
	 * Ecrit un texte dans le fichier de log. Le nom de la classe appelante est
	 * définit statiquement.
	 * 
	 * @param instance Nom de la classe appelante
	 * @param lvl      Niveau d'information
	 * @param text     Texte à logger
	 */
	public static void write(String instance, LogLevel lvl, String text) {
		manager.write(instance, lvl, text);
	}


	/**
	 * Ferme le fichier de log. Doit être appelée avant la fermeture de
	 * l'application
	 */
	public static void close() {
		manager.close();
	}
}
