/**
 * 
 */
package arduinoTools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;
import arduinoTools.Logger.LogLevel;

/**
 * Classe utilisée pour logger les actions du programme.
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Sun Jan  9 20:44:34     2022 : 67-lancement-de-la-generation-de-code-en-ligne-de-commande 
 * Tue Dec 21 19:45:37     2021 : integration-sonar 
 * 
 *
 */
public class LogManager {

	/**
	 * Référence vers le fichier de log
	 */
	private FileWriter logFile;

	/**
	 * Mémorisation du chemin du ficher de log
	 */
	private String filePath;

	/**
	 * Référence vers le formattage de l'heure
	 */
	private SimpleDateFormat timeFormat;

	/**
	 * Référence vers le formattage de la date
	 */
	private DateFormat fullDateFormat;

	/**
	 * Constructeur de la classe
	 * 
	 * @param filePath Chemin du fichier de log
	 * 
	 */
	public LogManager() {
		try {
			this.filePath = getFilePath();

			logFile = new FileWriter(new File(filePath), false);
			
			this.timeFormat = new SimpleDateFormat("HH:mm:ss,SSS");

			this.fullDateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
			logFile.append("Debut de log : " + fullDateFormat.format(new Date()) + "\n\n");
		} catch (IOException e) {
			System.err.println("\nErreur lors de l'ouverture du fichier " + filePath);
		}
	}

	/**
	 * Renvoie le chemin du fichier de log
	 * 
	 * @return Chemin du fichier
	 */
	private String getFilePath() {
		String file;

		try {
			file = ArduinoConfigLoader.getConfLoader().getConfigValue("LogFilePath");
		} catch (ArduinoConfigLoaderException e) {
			System.err.println(e.getMessage());
			return "arduinoLog.log";
		}

		return arduinoTools.SystemTools.getCurJARPath() + "\\" + file;
	}

	/**
	 * Ecrit le texte dans le fichier de log
	 * 
	 * @param instance Nom de la classe appelante
	 * @param lvl      Niveau d'information
	 * @param text     Texte à logger
	 */
	public void write(String instance, LogLevel lvl, String text) {
		try {
			logFile.append(timeFormat.format(new Date()) + " - " + lvl + " - " + instance + " - " + text + "\n");
		} catch (IOException e) {
			System.err.println("\nErreur lors de l'ecriture dans le fichier " + filePath);
		}
	}

	/**
	 * Ferme le fichier de log
	 */
	public void close() {
		try {
			logFile.append("\nFin de log : " + fullDateFormat.format(new Date()));
			logFile.close();
		} catch (IOException e) {
			System.err.println("\nErreur lors de la fermeture du fichier " + filePath);
		}
	}

}
