/**
 * 
 */
package arduinoTools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import arduinoTools.Logger.LogLevel;

/**
 * Classe utilisée pour charger la configuration de l'application au démarrage.
 * On utilise un singleton pour ne charger le fichier qu'une seule fois
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 17:38:44     2022 : 84-envoi-requete-avec-parametre-configurable 
 * Mon May  9 15:45:06     2022 : 81-ajout-parametre-de-configuration 
 * Tue Dec 21 19:45:35     2021 : integration-sonar 
 * 
 *
 */
public class ArduinoConfigLoader {

	/**
	 * Référence statique vers l'objet ArduinoConfigLoader
	 */
	private static ArduinoConfigLoader loader = null;

	/**
	 * Nom du fichier de configuration à charger
	 */
	private static final String configFileName = "arduino.cfg";

	/**
	 * Liste contenant les eléments de configuration lus depuis le fichier
	 */
	private List<ArduinoConfigElement> configList;

	/**
	 * Constructeur de la classe
	 * 
	 * @throws ArduinoConfigLoaderException
	 */
	private ArduinoConfigLoader() throws ArduinoConfigLoaderException {

		configList = new ArrayList<>();

		try (BufferedReader in = new BufferedReader(
				new FileReader(arduinoTools.SystemTools.getCurJARPath() + "/" + configFileName))) {

			String line;
			while ((line = in.readLine()) != null) {

				/* Détection ligne vide et commentaire */
				if (!line.equals("") && !line.startsWith("#")) {
					int delimiterIndex = line.indexOf("=");

					if (delimiterIndex != -1) {
						String s1 = line.substring(0, delimiterIndex);
						String s2 = line.substring(delimiterIndex + 1);
						configList.add(new ArduinoConfigElement(s1, s2));
					} else {
						throw new ArduinoConfigLoaderException(
								"Syntaxe du fichier incorrecte, symbole '=' non trouvé...");
					}
				}
			}
		} catch (FileNotFoundException e) {
			throw new ArduinoConfigLoaderException("Fichier de configuration non trouve...");
		} catch (IOException e) {
			Logger.write("ArduinoConfigLoader", LogLevel.ERROR, e.getMessage());
		}
	}

	/**
	 * Méthode statique permettant de récupérer la référence vers le singleton
	 * 
	 * @return Référence vers l'objet ArduinoConfigLoader
	 */
	public static ArduinoConfigLoader getConfLoader() {
		if (loader == null)
			try {
				loader = new ArduinoConfigLoader();
			} catch (ArduinoConfigLoaderException e) {
				Logger.write("ArduinoConfigLoader", LogLevel.ERROR, e.getMessage());
			}
			
		return loader;
	}

	/**
	 * Renvoie la valeur du champ de configuration demandé
	 * 
	 * @param configName
	 * 
	 * @return Valeur du champ
	 * @throws ArduinoConfigLoaderException Quand le champ demandé n'est pas trouvé
	 */
	public String getConfigValue(String configName) throws ArduinoConfigLoaderException {
		int i = 0;

		while (i < configList.size() && !configList.get(i).getName().equals(configName))
			i++;

		if (i >= configList.size())
			throw new ArduinoConfigLoaderException("Element " + configName + " non trouve dans le fichier...");

		return configList.get(i).getValue();
	}

	/**
	 * Renvoie la valeur booléenne du champ de configuration demandé
	 * 
	 * @param configName
	 * 
	 * @return Valeur du champ (true/false)
	 * @throws ArduinoConfigLoaderException Quand le champ demandé n'est pas trouvé
	 */
	public boolean getConfigValueBool(String configName) throws ArduinoConfigLoaderException {

		return getConfigValue(configName).equals("yes");
	}

	/**
	 * Classe représentant un élément de configuration de l'arduino, représenté par
	 * son nom et sa valeur
	 * 
	 */
	private class ArduinoConfigElement {

		private String name;
		private String value;

		/**
		 * Constructeur de la classe
		 * 
		 * @param name
		 * @param value
		 */
		public ArduinoConfigElement(String name, String value) {
			this.name = name;
			this.value = value;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

	}

	/**
	 * Exception levée lors d'une erreur de lecture du fichier de configuration
	 * 
	 */
	public class ArduinoConfigLoaderException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Constructeur de l'exception
		 * 
		 * @param msg Message d'erreur
		 */
		public ArduinoConfigLoaderException(String msg) {
			super("Erreur lors de la lecture du fichier de configuration : " + msg);
		}
	}

}
