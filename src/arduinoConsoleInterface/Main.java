package arduinoConsoleInterface;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.File;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import arduinoLinkInterface.Communication.CommManager.AutoCommManager;
import arduinoLinkInterface.Communication.CommManager.ManualCommManager;
import arduinoLinkInterface.Communication.CommManager.ManualCommManager.ManualTransmissionEmptyException;
import arduinoLinkInterface.Communication.CommManager.ManualCommManager.ManualTransmissionNotValidException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoTools.SystemTools;
import arduinoXmlConfig.Generator.FrameDataCodeGenerator;
import arduinoXmlConfig.Generator.InteractiveBiteCodeGenerator;
import arduinoXmlConfig.Generator.PeriodicDataCodeGenerator;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.XmlConfigParser.XmlFileNotFoundException;
import arduinoXmlConfig.Parser.XmlConfigParser.XmlOpeningException;
import arduinoXmlConfig.Parser.XmlConfigParser.XmlParserException;


/**
 * Classe contenant la fonction main
 * 
 * @date Wed Oct 27 18:44:45     2021
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Jan 18 20:54:16     2022 : 68-corrections-sonar 
 * Sun Jan  9 20:44:34     2022 : 67-lancement-de-la-generation-de-code-en-ligne-de-commande 
 * Tue Dec 21 19:45:23     2021 : integration-sonar 
 * Wed Oct 27 18:44:45     2021 : 41-corrections-affichage-2 
 * 
 *
 */
public class Main {

	private static Scanner sc;
	private static XmlConfigParser configParser;
	
	/**
	 * Liste des configurations disponibles
	 */
	private static String[] liste;

	private static final String FLUSH_CONSOLE = "\033[H\033[2J";
	private static final String APP_NAME = "Arduino Link Interface\n";

	private static final String MAIN_MENU = "Mode de communication :\n    1 : Communication automatique\n    2 : Communication manuelle\n    3 : Generation du code C++\n\n    q : Quitter";


	/**
	 * Fonction Main
	 * @param args
	 */
	public static void main(String[] args) {
		
		/* Verification des arguments reçus : 0 ou 2 */

		/* Option -genfile : Lancement direct de la génération de code */
		if(args.length == 2) {
			if (args[0].equals("-genfile"))
				startGenFileOption(args[1]);
		}

		/* Pas d'option : passage en mode interactif */
		else if (args.length == 0) {
		
			Logger.write("Main", LogLevel.INFO, "Demarrage en mode interactif");

			sc = new Scanner(System.in);

			int config = configChoice();
	
			if ((config != -1) && (createParser(liste[config - 1]))) {
				startMode();
				System.out.println("\nAu revoir !!\n");
			}

			sc.close();
	
		}

		Logger.close();
	}

	/**
	 * Lance le mode d'exécution choisi
	 */
	private static void startMode() {

		/* Effacement de la console */
		System.out.print(FLUSH_CONSOLE);
		System.out.flush();
		System.out.println(APP_NAME);

		System.out.println(MAIN_MENU);

		String mode = sc.next();

		while (!mode.equals("1") && !mode.equals("2") && !mode.equals("3") && !mode.equals("q")) {
			System.out.print(FLUSH_CONSOLE);
			System.out.flush();
			System.out.println(APP_NAME);

			System.out.println("Les seuls modes valides sont 1, 2 et 3.\n");

			System.out.println(MAIN_MENU);
			mode = sc.next();
		}

		if (mode.equals("1"))
			startCommAuto();
		else if (mode.equals("2"))
			startCommManu();
		else if (mode.equals("3"))
			startXmlCodeGeneration();
	}

	/**
	 * Démarre le mode -genfile
	 */
	private static void startGenFileOption(String args) {

		Logger.write("Main", LogLevel.INFO, "Mode genfile : generation du code C++ en ligne de commande");
		System.out.println("Generation du code C++ a partir du fichier " + args + "\n");

		if (createParser(args)) {
			if (!startXmlCodeGeneration())
				System.exit(-1);
		} else
			System.exit(-1);
	}

	/**
	 * Recherche des configurations disponibles et choix de celle à utiliser
	 * 
	 * @return Identifiant de la configuration à utiliser
	 */
	private static int configChoice() {

		/* Liste des configurations disponibles */
		File repertoire = new File(arduinoTools.SystemTools.getCurJARPath() + "/../XmlProjectConfig");
		Logger.write("Main", LogLevel.INFO, "Recherche des configurations dans le repertoire "
				+ arduinoTools.SystemTools.getCurJARPath() + "/XmlProjectConfig");
		liste = repertoire.list();

		if (liste.length == 0) {
			System.out.println("Aucune configuration disponible...");
			Logger.write("Main", LogLevel.ERROR, "Aucune configuration disponible...");
			Logger.close();
			return -1;
		}

		/* Choix de la configuration à utiliser */
		boolean nextStep = false;
		boolean wasInvalid = false;
		int config = 1;

		while (!nextStep) {

			/* Effacement de la console */
			System.out.print(FLUSH_CONSOLE);
			System.out.flush();
			System.out.println(APP_NAME);

			if (wasInvalid)
				System.out.println("Choix invalide\n");

			System.out.println("Choix de la configuration : ");
			for (int i = 0; i < liste.length; i++)
				System.out.println("    " + (i + 1) + " : " + liste[i].substring(0, liste[i].length() - 4));

			System.out.println("\n    q : Quitter");

			String conf = sc.nextLine();

			if (conf.equals("q")) {
				System.out.println("\nAu revoir !!\n");
				Logger.close();
				sc.close();
				return -1;
			} else if (SystemTools.isNumeric(conf)) {
				config = Integer.parseInt(conf);
				if (config >= 1 && config <= liste.length)
					nextStep = true;
				else
					wasInvalid = true;
			} else
				wasInvalid = true;
		}

		return config;
	}

	/**
	 * Instancie le parser XML à partir du nom du fichier de config à utiliser
	 * 
	 * @param configName Fichier de config
	 * @throws XmlParserException
	 */
	private static boolean createParser(String configName) {

		try {
			configParser = new XmlConfigParser(
					arduinoTools.SystemTools.getCurJARPath() + "/../XmlProjectConfig/" + configName);
		} catch (XmlParserException | XmlFileNotFoundException | XmlOpeningException e) {
			Logger.write("Main", LogLevel.ERROR, e.getMessage());
			System.out.println("Erreur : " + e.getMessage());
			return false;
		}

		Logger.write("Main", LogLevel.INFO, "Configuration chargee : " + configName);
		return true;
	}
	
	/**
	 * Démarre la communication automatique
	 */
	private static void startCommAuto() {

		AutoCommManager commManager = new AutoCommManager(configParser);

		if (commManager.getSerialPort().getOpenPort()) {

			/* Démarrage de l'affichage automatique */
			final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

			final AutoModeRunnable autoMode = new AutoModeRunnable(commManager);

			@SuppressWarnings("unused")
			final ScheduledFuture<?> task = executor.scheduleAtFixedRate(autoMode, 2, 2, SECONDS);

			/*
			 * si il n'y a pas de branchement, on ne fait pas le while : on sort de
			 * startComm et on dit au revoir
			 */
			String str = sc.nextLine();
			while(!str.contentEquals("q"))
			{
				/* On envoie les caractères reçus au runnable d'exécution */
				autoMode.processRcvString(str);

				str = sc.nextLine();
			}

			executor.shutdown();
			commManager.stopBite();
			commManager.closeComm();
		}
	}
	
	/**
	 * Démarre la communication manuelle
	 */
	private static void startCommManu() {
		
		ManualCommManager commManager = new ManualCommManager(configParser);
		String str;
		
		//si il n'y a pas de branchement, on ne fait pas le while : on sort de startComm et on dit au revoir
		if(commManager.getSerialPort().getOpenPort()) {
			boolean quit = false;
			while(!quit)
			{
				str = sc.nextLine();
				if(str.contentEquals("q"))
					quit = true;
				else
					try {
						commManager.sendCommand(str);
					} catch (ManualTransmissionNotValidException | ManualTransmissionEmptyException e) {
						System.err.println(e.getMessage());
					}
			}

			commManager.closeComm();
		}
	}
	
	/**
	 * Lance la génération automatique du code
	 * 
	 * @return TRUE si la génération est finie correctment, FALSE sinon
	 */
	private static boolean startXmlCodeGeneration() {

		boolean retval = true;

		PeriodicDataCodeGenerator periodicDataCodeGen = new PeriodicDataCodeGenerator(configParser);
		if (!periodicDataCodeGen.generatePeriodicData())
			retval = false;
		
		FrameDataCodeGenerator frameDataCodeGen = new FrameDataCodeGenerator(configParser);
		if (!frameDataCodeGen.generateFrameData())
			retval = false;

		InteractiveBiteCodeGenerator interactiveDataCodeGen = new InteractiveBiteCodeGenerator(configParser);
		if (!interactiveDataCodeGen.generateInteractiveData())
			retval = false;

		return retval;
	}
	

}
