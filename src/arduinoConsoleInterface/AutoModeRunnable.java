/**
 * 
 */
package arduinoConsoleInterface;

import arduinoLinkInterface.Communication.CommManager.AutoCommManager;
import arduinoLinkInterface.Communication.PeriodicData.PeriodicDataStorage.PeriodicDataStorageElement;
import arduinoLinkInterface.Communication.PeriodicData.TimePeriodicData;
import arduinoTools.ArduinoConfigLoader;
import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoTools.SystemTools;

/**
 * Classe implémentant l'interface Runnable : correspond au thread qui va
 * afficher périodiquement le menu et les données périodiques en mode
 * automatique.
 * 
 * @date Thu Oct 28 11:33:51     2021
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Mon May  9 15:45:20     2022 : 81-ajout-parametre-de-configuration 
 * Fri May  6 19:44:25     2022 : 79-modifications-affichage-arduino 
 * Tue Jan 18 21:33:16     2022 : 68-corrections-sonar 
 * Tue Dec 21 19:45:22     2021 : integration-sonar 
 * Thu Oct 28 11:33:51     2021 : 41-corrections-affichage-2 
 * 
 *
 */
public class AutoModeRunnable implements Runnable {

	private static final String INDENT25 = "                         "; // 25 espaces

	/**
	 * Référence vers le commManager utilisé
	 */
	private AutoCommManager commManager;

	/**
	 * Information temporaire secondaire, spécifique à la version console
	 */
	String secondaryInfo;

	/**
	 * Indique si le BITE a été détecté actif au cycle précédent
	 */
	boolean biteDetectedActive;

	/**
	 * Affichage des données périodiques
	 */
	private boolean displayPeriodicData;

	/**
	 * Affichage date
	 */
	private boolean displayDate;

	/**
	 * Affichage date en format long ou court
	 */
	private boolean displayDateLongFormat;

	/**
	 * Constructeur de la classe
	 */
	public AutoModeRunnable(AutoCommManager commManager) {
		this.commManager = commManager;
		this.secondaryInfo = null;
		this.biteDetectedActive = false;

		try {
			this.displayPeriodicData = ArduinoConfigLoader.getConfLoader().getConfigValueBool("DisplayPeriodicData");
			this.displayDate = ArduinoConfigLoader.getConfLoader().getConfigValueBool("DisplayDate");
			this.displayDateLongFormat = ArduinoConfigLoader.getConfLoader().getConfigValueBool("DateLongFormat");
		} catch (ArduinoConfigLoaderException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}
	}

	/**
	 * Affiche périodiquement le menu et les données périodiques
	 */
	@Override
	public void run() {

		/* Effacement de la console */
		System.out.print("\033[H\033[2J");
		System.out.flush();
		System.out.println("Arduino Link Interface");
		
		/* Détection Arduino connecté */
		if (commManager.isBiteActive()) {
			System.out.println("Arduino connecte");

			if (!biteDetectedActive) {
				Logger.write(getClass(), LogLevel.INFO, "Arduino connecté");
				biteDetectedActive = true;
			}

			/* Affichage du menu */
			String[] menuText = commManager.getCurrentMenuText();

			System.out.println("\n" + menuText[0] + " :");
			for (int i = 1; i < menuText.length; i++)
				System.out.println("    " + i + " : " + menuText[i]);

			if (!commManager.isStartItem())
				System.out.println("\n    r : Retour");

			System.out.println("\n    q : Quitter");

			/* Affichage de la ligne d'information */
			System.out.println("\n" + commManager.getTempInfo());

			/* Affichage de l'heure de l'Arduino */
			displayDateTime();

			/* Affichage des donnée périodiques */
			displayPeriodicData();
		}
		else {
			System.out.println("Aucun arduino connecte");
			Logger.write(getClass(), LogLevel.WARNING, "Aucun arduino connecte");
			biteDetectedActive = false;
		}

		/* Information secondaire */
		if (secondaryInfo != null) {
			System.out.println("\n\n" + secondaryInfo);
			secondaryInfo = null;
		}

		/* Infos de debug */
		if (commManager.isDisplayDebugData()) {
			System.out.println("\n\nDebug : ");
			String[] debugData = commManager.getDebugDataDisplay().getDebugData();
			for (int i = 0; i < debugData.length; i++)
				System.out.println(debugData[i]);
		}
	}

	/**
	 * Traitement de la commande utilisateur recue
	 * 
	 * @param str
	 */
	public void processRcvString(String str) {

		/* Si on reçoit un entier => choix dans le menu */
		if (SystemTools.isNumeric(str)) {
			commManager.processSelection(Integer.parseInt(str));
			run();
		}
		/*
		 * Si on a reçu "r" et qu'on n'est pas sur le menu principal on retourne au menu
		 * précédent
		 */
		else if (str.contentEquals("r") && !commManager.isStartItem()) {
			if (commManager.goToPreviousMenu())
				run();
		}
		else {
			secondaryInfo = "Choix non valide";
			run();
		}
	}

	/**
	 * Affichage de la date et de l'heure
	 */
	private void displayDateTime() {

		if (displayDate) {
			TimePeriodicData timeData = commManager.getStoredArduinoTime();
			if (timeData != null) {
				if (timeData.isDateTimeValid()) {
					System.out.println("\nDate et heure de l'Arduino : "
							+ formatDateString(timeData.getDay(), timeData.getMonth(), timeData.getYear(),
									timeData.getDayName())
							+ "  "
							+ formatTimeString(timeData.getHours(), timeData.getMinutes(), timeData.getSeconds()));
				} else
					System.out.println("Heure recue non valide");
			}
			else
				System.out.println("Heure de l'Arduino non recue...");
		}
	}

	/**
	 * Affichage des données périodiques
	 */
	private void displayPeriodicData() {

		if (displayPeriodicData) {

			PeriodicDataStorageElement[] data = commManager.getAllStoredPeriodicData();
			System.out.println("\nDonnees periodiques : ");
			System.out.println(
					"-----------------------------------------------------------------------------------------------");
			System.out.println(
					"-          Type           |   Courante    |           MAX           |           MIN           -");
			System.out.println(
					"--------------------------|---------------|-------------------------|--------------------------");

			for (int i = 0; i < data.length; i++) {

				String title = data[i].getData().getDisplayTitle();

				String curValue = getPeriodicDataValueCurrent(data[i]);
				String maxValue = getPeriodicDataValueMax(data[i]);
				String minValue = getPeriodicDataValueMin(data[i]);

				/* Affichage */
				System.out.println("- " + title + INDENT25.substring(0, INDENT25.length() - title.length() - 1) + "| "
						+ curValue + INDENT25.substring(0, INDENT25.length() - curValue.length() - 11) + "| " + maxValue
						+ INDENT25.substring(0, INDENT25.length() - maxValue.length() - 1) + "| " + minValue
						+ INDENT25.substring(0, INDENT25.length() - minValue.length() - 1) + "-");
			}

			System.out.println(
					"-----------------------------------------------------------------------------------------------");
		}
	}

	/**
	 * Récupération de la valeur min
	 * 
	 * @param data Donnée périodique
	 * @return Valeur min formatée
	 */
	private String getPeriodicDataValueMin(PeriodicDataStorageElement data) {

		String minValue = "N/A";
		if ((data.getData_min() != null) && (data.getData_min().isValid())) {
			minValue = data.getData_min().getData();

			if (data.getData_min().getTime().isTimeValid())
				minValue += INDENT25.substring(0, INDENT25.length() - minValue.length() - 15) + "("
						+ formatTimeString(data.getData_min().getTime().getHours(),
								data.getData_min().getTime().getMinutes(), data.getData_min().getTime().getSeconds())
						+ ") ";
		}

		return minValue;
	}

	/**
	 * Récupération de la valeur max
	 * 
	 * @param data Donnée périodique
	 * @return Valeur max formatée
	 */
	private String getPeriodicDataValueMax(PeriodicDataStorageElement data) {

		String maxValue = "N/A";
		if ((data.getData_max() != null) && (data.getData_max().isValid())) {
			maxValue = data.getData_max().getData();

			if (data.getData_max().getTime().isTimeValid())
				maxValue += INDENT25.substring(0, INDENT25.length() - maxValue.length() - 15) + "("
						+ formatTimeString(data.getData_max().getTime().getHours(),
								data.getData_max().getTime().getMinutes(), data.getData_max().getTime().getSeconds())
						+ ")";
		}

		return maxValue;
	}

	/**
	 * Récupération de la valeur courabte
	 * 
	 * @param data Donnée périodique
	 * @return Valeur courante formatée
	 */
	private String getPeriodicDataValueCurrent(PeriodicDataStorageElement data) {

		String curValue;
		if (data.getData().isValid())
			curValue = data.getData().getData()
					+ INDENT25.substring(0, INDENT25.length() - 17 - data.getData().getData().length())
					+ data.getData().getSuffix();
		else
			curValue = "invalide";

		return curValue;
	}

	/**
	 * Formatte une string contenant l'heure à partir des valeurs d'entrée
	 * 
	 * @param hours
	 * @param minutes
	 * @param seconds
	 * @return String formatée
	 */
	private String formatTimeString(int hours, int minutes, int seconds) {

		String hh = String.valueOf(hours);
		String mm = String.valueOf(minutes);
		String ss = String.valueOf(seconds);

		if (hh.length() == 1)
			hh = "0" + hh;
		if (mm.length() == 1)
			mm = "0" + mm;
		if (ss.length() == 1)
			ss = "0" + ss;

		return hh + ":" + mm + ":" + ss;
	}

	/**
	 * Formatte une string contenant la date à partir des valeurs d'entrée
	 * 
	 * @param day
	 * @param month
	 * @param year
	 * @param dayName
	 * @return String formatée
	 */
	private String formatDateString(int day, int month, int year, int dayName) {

		String ddname;

		if (displayDateLongFormat)
			ddname = TimePeriodicData.getDayName(dayName);
		else
			ddname = "";

		String dd = String.valueOf(day);
		
		String mm;
		String separator;
		if (displayDateLongFormat) {
			mm = TimePeriodicData.getMonthName(month - 1);
			separator = " ";
		} else {
			mm = String.valueOf(month);
			separator = "/";
		}

		String yyyy = String.valueOf(year);

		if (dd.length() == 1)
			dd = "0" + dd;
		if (mm.length() == 1)
			mm = "0" + mm;

		return ddname + " " + dd + separator + mm + separator + yyyy;
	}

}
