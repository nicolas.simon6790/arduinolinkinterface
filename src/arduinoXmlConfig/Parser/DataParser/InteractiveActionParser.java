/**
 * 
 */
package arduinoXmlConfig.Parser.DataParser;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import arduinoXmlConfig.XmlConfigDefinition;


/**
 * Classe servant à parser les actions du BITE interactif du fichier XML. Doit
 * être instanciée par la classe XmlConfigParser
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:09     2021 : integration-sonar 
 * 
 *
 */
public class InteractiveActionParser extends DataParser {

	/**
	 * Définit les types de réponses possibles
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:09     2021 : integration-sonar 
 * 
	 *
	 */
	public enum AnswerType {
		BOOL, INTEGER
	}

	/**
	 * Constructeur de la classe
	 * 
	 * @param nodeList Noeud de la config XML à parser
	 */
	public InteractiveActionParser(NodeList nodeList) {
		super(nodeList, "BITE Actions", XmlConfigDefinition.InteractiveData.ActionsConfig.ElementName);
	}

	/**
	 * 
	 * @return Configuration à partir de la conf XML
	 * @throws InteractiveActionAnswerConfigNotFoundException Si la configuration
	 *                                                        n'est pas trouvée
	 */
	public InteractiveActionAnswerConfig getAnswerConfig() throws InteractiveActionAnswerConfigNotFoundException {

		NodeList list = ((Element) dataNodeList.item(currentDataIndex)).getChildNodes();

		int index = 0;
		while (!list.item(index).getNodeName()
				.equals(XmlConfigDefinition.InteractiveData.ActionsConfig.AnswerConfig.ConfigName)) {

			index++;
			if (list.item(index) == null)
				throw new InteractiveActionAnswerConfigNotFoundException(currentDataIndex);
		}

		AnswerType type = AnswerType.valueOf(((Element) list.item(index))
				.getAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.AnswerConfig.AnswerTypeName));

		String pattern = ((Element) list.item(index))
				.getAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.AnswerConfig.PatternName);

		String valueTrue = null;
		String valueFalse = null;

		if (type == AnswerType.BOOL) {
			valueTrue = ((Element) list.item(index))
					.getAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.AnswerConfig.ValueTrueName);
			valueFalse = ((Element) list.item(index))
					.getAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.AnswerConfig.ValueFalseName);
		}

		return new InteractiveActionAnswerConfig(type, pattern, valueFalse, valueTrue);
	}

	/**
	 * Classe contenant les données nécéssaires au traitement de la réponse à une
	 * action
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:09     2021 : integration-sonar 
 * 
	 */
	public class InteractiveActionAnswerConfig {

		/**
		 * Type de la réponse
		 */
		AnswerType type;

		/**
		 * Phrase de réponse préformattée
		 */
		String pattern;

		/**
		 * Element variable de formatage quand la réponse est FALSE
		 */
		String answerFalse;

		/**
		 * Element variable de formatage quand la réponse est TRUE
		 */
		String answerTrue;

		/**
		 * @param type
		 * @param pattern
		 * @param answerFalse
		 * @param answerTrue
		 */
		public InteractiveActionAnswerConfig(AnswerType type, String pattern, String answerFalse, String answerTrue) {
			super();
			this.type = type;
			this.pattern = pattern;
			this.answerFalse = answerFalse;
			this.answerTrue = answerTrue;
		}

		/**
		 * @return the type
		 */
		public AnswerType getType() {
			return type;
		}

		/**
		 * @return the pattern
		 */
		public String getPattern() {
			return pattern;
		}

		/**
		 * @return the answerFalse
		 */
		public String getAnswerFalse() {
			return answerFalse;
		}

		/**
		 * @return the answerTrue
		 */
		public String getAnswerTrue() {
			return answerTrue;
		}

	}

	/**
	 * Classe gérant l'exception levée quand la configuration des réponses n'est pas
	 * trouvée
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:09     2021 : integration-sonar 
 * 
	 *
	 */
	public class InteractiveActionAnswerConfigNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Quand la config des réponses n'est pas trouvé dans la config XML, on
		 * enregistre le message d'erreur.
		 * 
		 */
		public InteractiveActionAnswerConfigNotFoundException(int id) {
			super("La configuration de la réponse n'existe pas dans la conf XML pour l'action #" + id);
		}
	}
}
