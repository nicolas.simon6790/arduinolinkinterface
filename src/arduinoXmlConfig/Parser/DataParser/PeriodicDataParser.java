/**
 * 
 */
package arduinoXmlConfig.Parser.DataParser;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.XmlConfigDefinition;

/**
 * Classe servant à parser les données périodique du fichier XML.
 * Doit être instanciée par la classe XmlConfigParser
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr 12 18:28:11     2022 : 76-rendre-generique-le-code-des-donnees-periodiques 
 * Mon Apr  4 20:04:44     2022 : 73-mise-a-jour-code-auto-suite-a-passage-en-allocation-statique 
 * Tue Dec 21 19:45:39     2021 : integration-sonar 
 * 
 *
 */
public class PeriodicDataParser extends DataParser {
	
	/**
	 * Types possibles pour la valeur d'un capteur
	 * 
	 */
	public enum SensorValueType {
		CURRENT, MAX, MIN
	}

	/**
	 * Types possibles pour le ServiceType
	 */
	public enum ServiceType {
		NONE, SENSORS, DATE_TIME
	}

	/**
	 * Numéro de l'élément actuel
	 */
	private byte elementIndex;

	/**
	 * Type de valeur de l'élément pointé
	 */
	private SensorValueType currentValueType;

	/**
	 * Constructeur de la classe
	 * 
	 * @param nodeList Noeud de la config XML à parser
	 */
	public PeriodicDataParser(NodeList nodeList) {
		super(nodeList, "BITE périodique", XmlConfigDefinition.PeriodicData.ElementName);

		this.elementIndex = 1;
		this.currentValueType = SensorValueType.CURRENT;
	}
	


	
	/**
	 * Renvoie la configuration C++ de la donnée périodique actuelle
	 * 
	 * @return Reference vers la config C++
	 * @throws PeriodicDataCppConfigNotFoundException
	 */
	public PeriodicDataCppConfig getPeriodicDataCppConfig() throws PeriodicDataCppConfigNotFoundException{
		
		NodeList list = ((Element) dataNodeList.item(currentDataIndex)).getChildNodes();

		int index = 0;
		while (!list.item(index).getNodeName().equals(XmlConfigDefinition.PeriodicData.CppConfigName)) {
			if(list.item(index) == null)
				throw new PeriodicDataCppConfigNotFoundException(currentDataIndex);
			index++;
		}
		
		String periodMinMaxStr = ((Element) list.item(index)).getAttribute("TransmissionPeriodMinMax");
		int periodminmax;
		if (periodMinMaxStr.equals(""))
			periodminmax = 0;
		else
			periodminmax = Integer.parseInt(periodMinMaxStr);

		return new PeriodicDataCppConfig(
				Byte.parseByte(((Element) list.item(index)).getAttribute("Group")),
				Byte.parseByte(((Element) list.item(index)).getAttribute("ID")),
				Integer.parseInt(((Element) list.item(index)).getAttribute("TransmissionPeriod")),
				periodminmax, ((Element) list.item(index)).getAttribute("ServiceType"));
	}
	
	/**
	 * Déplace l'index vers la prochaine données du fichier
	 * 
	 * @return TRUE si la prochaine donnée a été trouvée, FAUX sinon
	 */
	@Override
	public boolean goToNextData() {

		if (isSensorMaxValueActive())
			elementIndex++;

		if (isSensorMinValueActive())
			elementIndex++;

		elementIndex++;
		return super.goToNextData();
	}

	/**
	 * Retourne à la première donnée du fichier
	 * 
	 * @throws DataParserException
	 */
	@Override
	public void resetCurrentDataIndex() throws DataParserException {
		elementIndex = 1;
		super.resetCurrentDataIndex();
	}

	/**
	 * Renvoie l'ID calculé de l'élément actuel en fonction du type de valeur
	 * demandé
	 * 
	 * @param type Type de valeur
	 * @return ID calculé
	 */
	public byte getElementID(SensorValueType type) {

		byte id;

		switch (type) {
		case CURRENT:
		default:
			id = elementIndex;
			break;
		case MAX:
			if (isSensorMaxValueActive())
				id = (byte) (elementIndex + 1);
			else
				id = 0;
			break;
		case MIN:
			if (isSensorMaxValueActive() && isSensorMinValueActive())
				id = (byte) (elementIndex + 2);
			else if (isSensorMinValueActive())
				id = (byte) (elementIndex + 1);
			else
				id = 0;
			break;
		}

		return id;
	}

	/**
	 * Déplace le pointeur vers l'action correspondant à l'ID donné
	 * 
	 * @param id ID à rechercher
	 * @throws DataIDNotFoundException Si l'ID n'a pas été trouvé dans la conf XML
	 */
	@Override
	public void findDataByID(byte id) throws DataIDNotFoundException {

		try {
			resetCurrentDataIndex();
			boolean found = false;

			while (!found) {
				found = true;
				if (getElementID(SensorValueType.CURRENT) == id)
					currentValueType = SensorValueType.CURRENT;
				else if (getElementID(SensorValueType.MAX) == id)
					currentValueType = SensorValueType.MAX;
				else if (getElementID(SensorValueType.MIN) == id)
					currentValueType = SensorValueType.MIN;
				else {
					found = false;
					if (!goToNextData())
						throw new DataIDNotFoundException(id);
				}
			}
		} catch (DataParserException | NumberFormatException e) {
			throw new DataIDNotFoundException(id);
		}
	}

	/**
	 * Renvoie TRUE si la valeur max du capteur est active, FALSE sinon
	 * 
	 */
	public boolean isSensorMaxValueActive() {
		String isActive;
		try {
			isActive = getDataAttribute("MaxValueActive");
			return isActive.equals("true");
		} catch (DataAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}

		return false;
	}

	/**
	 * Renvoie TRUE si la valeur min du capteur est active, FALSE sinon
	 * 
	 */
	public boolean isSensorMinValueActive() {
		String isActive;
		try {
			isActive = getDataAttribute("MinValueActive");
			return isActive.equals("true");
		} catch (DataAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}

		return false;
	}

	public ServiceType getServiceType() throws PeriodicDataCppConfigNotFoundException {

		ServiceType servicetype = ServiceType.NONE;
		String type = getPeriodicDataCppConfig().getServiceType();

		if (type.equals("SENSORS"))
			servicetype = ServiceType.SENSORS;
		else if (type.equals("DATE_TIME"))
			servicetype = ServiceType.DATE_TIME;

		return servicetype;
	}

	
	/**
	 * @return the currentValueType
	 */
	public SensorValueType getCurrentValueType() {
		return currentValueType;
	}

	/**
	 * Classe contenant les paramètres C++ provenant d'une donnée périodique
	 *
	 */
	public class PeriodicDataCppConfig{
		byte group;
		byte id;
		int period;
		int periodMinMax;
		String serviceType;
		
		/**
		 * @param group
		 * @param id
		 * @param period
		 */
		public PeriodicDataCppConfig(byte group, byte id, int period, int periodMinMax, String serviceType) {
			super();
			this.group = group;
			this.id = id;
			this.period = period;
			this.periodMinMax = periodMinMax;
			this.serviceType = serviceType;
		}


		/**
		 * @return the group
		 */
		public byte getGroup() {
			return group;
		}

		/**
		 * @return the id
		 */
		public byte getId() {
			return id;
		}


		/**
		 * @return the period
		 */
		public int getPeriod() {
			return period;
		}

		/**
		 * @return the periodMinMax
		 */
		public int getPeriodMinMax() {
			return periodMinMax;
		}

		/**
		 * @return the serviceType
		 */
		public String getServiceType() {
			return serviceType;
		}
		
	}
	

	
	/**
	 * Classe gérant l'exception levée quand la configuration C++ n'est pas trouvée
	 *
	 */
	public class PeriodicDataCppConfigNotFoundException extends Exception{
		
		private static final long serialVersionUID = 1L;
		
		/**
		 * Quand la config C++ n'est pas trouvé dans la config XML, on enregistre le message d'erreur. 
		 * 
		 */
		public PeriodicDataCppConfigNotFoundException(int id) {
			super("La configuration C++ n'existe pas dans la conf XML pour la donnée périodique #" + id);
		}
	}
}
