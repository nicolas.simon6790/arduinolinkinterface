/**
 * 
 */
package arduinoXmlConfig.Parser.DataParser;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Superclasse pour data parser
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:08     2021 : integration-sonar 
 * 
 *
 */
public class DataParser {

	protected NodeList dataNodeList;
	protected int currentDataIndex;
	String dataName;
	String xmlElementName;

	/**
	 * Constructeur de la classe
	 * 
	 * @param nodeList Noeud de la config XML à parser
	 * @param dataName Nom des données parsées par la classe
	 */
	public DataParser(NodeList nodeList, String dataName, String xmlElementName) {
		this.currentDataIndex = 0;
		this.dataNodeList = nodeList;
		this.dataName = dataName;
		this.xmlElementName = xmlElementName;
	}

	/**
	 * 
	 * @return Nombre de données définies dans le fichier XML
	 */
	public int getDataCount() {
		int cpt = 0;

		for (int i = 0; i < dataNodeList.getLength(); i++) {
			if (dataNodeList.item(i).getNodeType() == Node.ELEMENT_NODE)
				cpt++;
		}

		return cpt;
	}

	/**
	 * 
	 * @param attribute Nom de l'attribut
	 * @return Valeur de l'attribut
	 * @throws CommonAttributeNotFoundException Quand l'attribut demandé n'existe
	 *                                          pas
	 */
	public String getCommonAttribute(String attribute) throws CommonAttributeNotFoundException {

		String attributeValue = ((Element) dataNodeList).getAttribute(attribute);

		if (attributeValue == null || attributeValue.equals(""))
			throw new CommonAttributeNotFoundException(attribute);

		return attributeValue;
	}

	/**
	 * Déplace l'index vers la prochaine données du fichier
	 * 
	 * @return TRUE si la prochaine donnée a été trouvée, FAUX sinon
	 */
	public boolean goToNextData() {

		do {
			currentDataIndex++;
			if (dataNodeList.item(currentDataIndex) == null)
				return false;
		} while (!dataNodeList.item(currentDataIndex).getNodeName().equals(xmlElementName));

		return true;

	}

	/**
	 * Retourne à la première donnée du fichier
	 * 
	 * @throws DataParserException
	 */
	public void resetCurrentDataIndex() throws DataParserException {

		if (getDataCount() == 0)
			throw new DataParserException("il n'y a pas de données à générer");

		currentDataIndex = 0;

		while (!dataNodeList.item(currentDataIndex).getNodeName().equals(xmlElementName))
			currentDataIndex++;
	}

	/**
	 * Renvoie la valeur de l'attribut demandé pour la donnée en cours
	 * 
	 * @param attribute Nom de l'attribut à rechercher
	 * @return Valeur de l'attribut
	 * @throws DataAttributeNotFoundException Quand l'attribut n'existe pas dans le
	 *                                        fichier XML
	 */
	public String getDataAttribute(String attribute) throws DataAttributeNotFoundException {

		String attributeValue = ((Element) dataNodeList.item(currentDataIndex)).getAttribute(attribute);

		if (attributeValue == null || attributeValue.equals(""))
			throw new DataAttributeNotFoundException(attribute, currentDataIndex);

		return attributeValue;
	}

	/**
	 * Déplace le pointeur vers l'action correspondant à l'ID donné
	 * 
	 * @param id ID à rechercher
	 * @throws DataIDNotFoundException Si l'ID n'a pas été trouvé dans la conf XML
	 */
	public void findDataByID(byte id) throws DataIDNotFoundException {

		try {
			resetCurrentDataIndex();

			while (Integer.parseInt(getDataAttribute("CMD_ID")) != id) {
				if (!goToNextData())
					throw new DataIDNotFoundException(id);
			}

		} catch (DataParserException | NumberFormatException | DataAttributeNotFoundException e) {
			throw new DataIDNotFoundException(id);
		}
	}

	/**
	 * Renvoie un DataParser vers le sous-élément présent à l'index actuel
	 * 
	 * @return Parser vers le sous-élément
	 */
	public DataParser getSubElementDataParser() {

		return new DataParser(dataNodeList.item(currentDataIndex).getChildNodes(), "BITE Menu",
				xmlElementName);
	}


	/**
	 * 
	 * Classe gérant l'exception levée quand l'attribut demandé n'existe pas pas la
	 * conf commune
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:08     2021 : integration-sonar 
 * 
	 *
	 */
	public class CommonAttributeNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Quand l'attribut demandé n'est pas trouvé dans la config commune, on
		 * enregistre le message d'erreur.
		 * 
		 * @param attName Nom de l'attribut recherché
		 */
		public CommonAttributeNotFoundException(String attName) {
			super("L'attribut " + attName + " n'existe pas dans la conf XML " + dataName);
		}
	}

	/**
	 * Classe gérant l'exception levée quand l'analyse des données ne se termine pas
	 * correctement
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:08     2021 : integration-sonar 
 * 
	 *
	 */
	public class DataParserException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Quand l'analyse du fichier XML ne se termine pas correctement, on enregistre
		 * le message d'erreur.
		 * 
		 * @param s Message d'erreur
		 */
		public DataParserException(String s) {
			super("\nErreur d'analyse des données " + dataName + " : " + s);
		}
	}

	/**
	 * 
	 * Classe gérant l'exception levée quand l'attribut demandé n'existe pas
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:08     2021 : integration-sonar 
 * 
	 *
	 */
	public class DataAttributeNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Quand l'attribut demandé n'est pas trouvé dans la donnée en cours, on
		 * enregistre le message d'erreur.
		 * 
		 * @param attName Nom de l'attribut recherché
		 * @param ID      Index de la donnée périodique en cause
		 */
		public DataAttributeNotFoundException(String attName, int id) {
			super("L'attribut " + attName + " n'existe pas dans la conf XML " + dataName + " #" + id);
		}
	}

	/**
	 * 
	 * Classe gérant l'exception levée quand l'ID demandé n'existe pas
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 21:03:08     2021 : integration-sonar 
 * 
	 *
	 */
	public class DataIDNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Quand l'ID demandé n'est pas trouvé dans la config XML, on enregistre le
		 * message d'erreur.
		 * 
		 * @param ID Index recherché
		 */
		public DataIDNotFoundException(byte id) {
			super("Aucune donnée correspondant à l'ID " + id + " n'existe dans la configuration " + dataName);
		}
	}

}
