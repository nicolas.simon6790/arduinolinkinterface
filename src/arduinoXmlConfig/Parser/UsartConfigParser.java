/**
 * 
 */
package arduinoXmlConfig.Parser;

import org.w3c.dom.NodeList;

import arduinoLinkInterface.PortManager.SerialPortConfiguration;
import arduinoTools.ArduinoConfigLoader;
import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Parser.DataParser.DataParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;

/**
 * Classe servant à parser la configuration Usart. Doit être instanciée par la
 * classe XmlConfigParser
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 19:45:40     2021 : integration-sonar 
 * 
 *
 */
public class UsartConfigParser {

	private DataParser dataParser;

	/**
	 * Constructeur de la classe
	 * 
	 * @param nodeList Noeud XML avec la configuration USART
	 */
	public UsartConfigParser(NodeList nodeList) {
		dataParser = new DataParser(nodeList, "Usart Config", "");
	}

	/**
	 * Renvoie la configuration lue dans le fichier XML
	 * 
	 * @return Reference vers la configuration
	 * 
	 * @throws CommonAttributeNotFoundException En cas d'erreur dans la
	 *                                          configuration XML
	 */
	public SerialPortConfiguration getUsartConfiguration() throws CommonAttributeNotFoundException {

		int baudrate = Integer.parseInt(getAttribute(XmlConfigDefinition.UsartConfig.BaudrateConfig));
		int databits = Integer.parseInt(getAttribute(XmlConfigDefinition.UsartConfig.DataBitsConfig));
		int stopbits = Integer.parseInt(getAttribute(XmlConfigDefinition.UsartConfig.StopBitsConfig));
		
		int parity;
		switch (getAttribute(XmlConfigDefinition.UsartConfig.ParityConfig)) {
		default:
		case "None":
			parity = 0;
			break;
		case "Odd":
			parity = 1;
			break;
		case "Even":
			parity = 2;
			break;
		}

		return new SerialPortConfiguration(getCOMPortFromConfigFile(), baudrate, databits, stopbits, parity);
	}

	/**
	 * Renvoie la valeur de l'attribut demandé.
	 * 
	 * @param attribute
	 * @return attribute value
	 * @throws CommonAttributeNotFoundException Si l'attribut demandé n'existe pas
	 */
	private String getAttribute(String attribute) throws CommonAttributeNotFoundException {

		return dataParser.getCommonAttribute(attribute);
	}

	/**
	 * Récupère le port série à utiliser dans le fichier de configuration
	 * 
	 * @return Port série
	 */
	private String getCOMPortFromConfigFile() {

		String port;
		try {
			port = ArduinoConfigLoader.getConfLoader().getConfigValue("COMPort");
		} catch (ArduinoConfigLoaderException e1) {
			Logger.write(getClass(), LogLevel.WARNING,
					"Port série à utiliser non trouvé, utilisation du port COM1 par défaut");
			return "COM1";
		}

		return port;
	}

}
