/**
 * 
 */
package arduinoXmlConfig.Parser;

import org.w3c.dom.NodeList;

import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Parser.DataParser.DataParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;

/**
 * Classe servant à parser la configuration de la trame.
 * Doit être instanciée par la classe XmlConfigParser
 * 
 * @author Nicolas
 *
 */
public class FrameConfigParser {
	
	private DataParser dataParser;
	
	/**
	 * Constructeur de la classe
	 * 
	 * @param nodeList Noeud XML avec la configuration de la trame
	 */
	public FrameConfigParser(NodeList nodeList) {
		dataParser = new DataParser(nodeList, "Frame Data", "");
	}
	
	/**
	 * Renvoie la valeur du caractère STX
	 * 
	 * @return STX value
	 * @throws CommonAttributeNotFoundException Si la valeur de STX n'est pas
	 *                                          configurée
	 */
	public int getSTX() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.STXAttributeName));
	}
	
	/**
	 * Renvoie la valeur du caractère ETX
	 * 
	 * @return ETX value
	 * @throws CommonAttributeNotFoundException Si la valeur de ETX n'est pas
	 *                                          configurée
	 */
	public int getETX() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.ETXAttributeName));
	}
	
	/**
	 * Renvoie la valeur du TYPE DATA
	 * 
	 * @return TYPE_DATA value
	 * @throws CommonAttributeNotFoundException Si la valeur de TYPE_DATA n'est pas
	 *                                          configurée
	 */
	public int getTypeData() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.TYPE_DATAAttributeName));
	}
	
	/**
	 * Renvoie la valeur du TYPE REQ
	 * 
	 * @return TYPE_REQ value
	 * @throws CommonAttributeNotFoundException Si la valeur de TYPE_REQ n'est pas
	 *                                          configurée
	 */
	public int getTypeReq() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.TYPE_REQAttributeName));
	}

	/**
	 * Renvoie la valeur du TYPE ANS
	 * 
	 * @return TYPE_ANS value
	 * @throws CommonAttributeNotFoundException Si la valeur de TYPE_ANS n'est pas
	 *                                          configurée
	 */
	public int getTypeAns() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.TYPE_ANSAttributeName));
	}
	
	/**
	 * Renvoie la valeur du TYPE READY
	 * 
	 * @return TYPE_READY value
	 * @throws CommonAttributeNotFoundException Si la valeur de TYPE_READY n'est pas
	 *                                          configurée
	 */
	public int getTypeReady() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.TYPE_READYAttributeName));
	}
	
	/**
	 * Renvoie la valeur du TYPE DEBUG
	 * 
	 * @return TYPE_DEBUG value
	 * @throws CommonAttributeNotFoundException Si la valeur de TYPE_DEBUG n'est pas
	 *                                          configurée
	 */
	public int getTypeDebug() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.TYPE_DEBUGAttributeName));
	}

	/**
	 * Renvoie la valeur du STATUS ACK
	 * 
	 * @return STATUS_ACK value
	 * @throws CommonAttributeNotFoundException Si la valeur de STATUS_ACK n'est pas
	 *                                          configurée
	 */
	public int getStatusAck() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.STATUS_ACKAttributeName));
	}

	/**
	 * Renvoie la valeur du STATUS NAK
	 * 
	 * @return STATUS_NAK value
	 * @throws CommonAttributeNotFoundException Si la valeur de STATUS_NAK n'est pas
	 *                                          configurée
	 */
	public int getStatusNak() throws CommonAttributeNotFoundException {
		return Integer.parseInt(getAttribute(XmlConfigDefinition.FrameConfig.STATUS_NAKAttributeName));
	}

	/**
	 * Renvoie la valeur de l'attribut demandé.
	 * 
	 * @param attribute
	 * @return attribute value
	 * @throws CommonAttributeNotFoundException Si l'attribut demandé n'existe pas
	 */
	private String getAttribute(String attribute) throws CommonAttributeNotFoundException {
		
		return dataParser.getCommonAttribute(attribute);
	}

}
