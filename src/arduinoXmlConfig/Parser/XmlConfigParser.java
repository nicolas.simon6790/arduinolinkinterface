/**
 * Package contenant les classes pour décoder le fichier de configuration et générer le code C++ associé
 */
package arduinoXmlConfig.Parser;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Parser.DataParser.DataParser;
import arduinoXmlConfig.Parser.DataParser.InteractiveActionParser;
import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser;

/**
 * Classe servant à lire le fichier XML de configuration
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Wed Jan 26 20:58:02     2022 : 68-corrections-sonar 
 * Sun Jan  9 20:44:35     2022 : 67-lancement-de-la-generation-de-code-en-ligne-de-commande 
 * Tue Dec 21 19:45:40     2021 : integration-sonar 
 * Sun Dec 19 18:34:56     2021 : 61-compatibilite-de-version-et-identification-arduino 
 * 
 *
 */
public class XmlConfigParser {
	
	private NodeList racineNoeuds;
	
	private String xmlFilePath;
	
	private PeriodicDataParser periodicDataParser;
	private InteractiveActionParser interactiveDataActionsParser;
	private DataParser interactiveMenuParser;
	private FrameConfigParser frameConfigParser;
	private UsartConfigParser usartConfigParser;
	
	private Document document;
	
	private static final String NO_FIELD_STR = "Il n'y a pas de champ ";
	private static final String MORE_THAN_1_FIELD_STR = "Il y a plus d'un champ ";
	
	/**
	 * Constructeur de la classe
	 * 
	 * @param filePath Chemin du fichier XML
	 * @throws XmlParserException       Si le fichier contient des erreurs
	 * @throws XmlFileNotFoundException Si le fichier n'existe pas
	 * @throws XmlOpeningException      Si le fichier ne peut pas être ouvert
	 */
	public XmlConfigParser(String filePath) throws XmlParserException, XmlFileNotFoundException, XmlOpeningException {
		
		document = null;
		xmlFilePath = filePath;
		
		/* Vérification de l'existence du fichier */
		File xmlFile = new File(xmlFilePath);
		if (!xmlFile.exists()) {
			throw new XmlFileNotFoundException();
		}

		try {
			document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new XmlOpeningException();
		}

		racineNoeuds = document.getDocumentElement().getChildNodes();
		
		/*
		 * Il y a 4 champs principaux : FrameConfig, PeriodicDataConfig et
		 * InteractiveConfig On vérifie qu'ils existent et qu'ils sont uniques. Le champ
		 * UsartConfig est optionnel. On ne lève pas d'exception.
		 */
		
		checkPeriodicData();
		checkFrameConfig();
		checkUsartConfig();
		checkInteractiveData();

	}
	

	/**
	 * Vérification de l'existence de la conf des données périodiques dans le
	 * fichier XML
	 * 
	 * @throws XmlParserException
	 */
	private void checkPeriodicData() throws XmlParserException {

		int periodicDataElementIndex = 0;
		int cpt = 0;

		for (int i = 0; i < racineNoeuds.getLength(); i++) {
			if (racineNoeuds.item(i).getNodeName().equals(XmlConfigDefinition.PeriodicData.ConfigName)) {
				cpt++;
				periodicDataElementIndex = i;
			}
		}

		if (cpt == 0)
			throw new XmlParserException(NO_FIELD_STR + XmlConfigDefinition.PeriodicData.ConfigName);
		else if (cpt > 1)
			throw new XmlParserException(MORE_THAN_1_FIELD_STR + XmlConfigDefinition.PeriodicData.ConfigName);
		else
			periodicDataParser = new PeriodicDataParser(racineNoeuds.item(periodicDataElementIndex).getChildNodes());
	}
	
	/**
	 * Vérification de l'existence de la conf des trames dans le fichier XML
	 * 
	 * @throws XmlParserException
	 */
	private void checkFrameConfig() throws XmlParserException {

		int frameConfigElementIndex = 0;
		int cpt = 0;

		for (int i = 0; i < racineNoeuds.getLength(); i++) {
			if (racineNoeuds.item(i).getNodeName().equals(XmlConfigDefinition.FrameConfig.ConfigName)) {
				cpt++;
				frameConfigElementIndex = i;
			}
		}

		if (cpt == 0)
			throw new XmlParserException(NO_FIELD_STR + XmlConfigDefinition.FrameConfig.ConfigName);
		else if (cpt > 1)
			throw new XmlParserException(MORE_THAN_1_FIELD_STR + XmlConfigDefinition.FrameConfig.ConfigName);
		else
			frameConfigParser = new FrameConfigParser(racineNoeuds.item(frameConfigElementIndex).getChildNodes());
	}

	/**
	 * Vérification de l'existence de la conf Usart dans le fichier XML
	 */
	private void checkUsartConfig() {

		int usartConfigElementIndex = 0;
		int cpt = 0;

		for (int i = 0; i < racineNoeuds.getLength(); i++) {
			if (racineNoeuds.item(i).getNodeName().equals(XmlConfigDefinition.UsartConfig.ConfigName)) {
				cpt++;
				usartConfigElementIndex = i;
			}
		}

		if (cpt == 0 || cpt > 1)
			usartConfigParser = null;
		else
			usartConfigParser = new UsartConfigParser(racineNoeuds.item(usartConfigElementIndex).getChildNodes());
	}

	/**
	 * Vérification de l'existence de la conf du BITE interactif dans le fichier XML
	 * 
	 * @throws XmlParserException
	 */
	private void checkInteractiveData() throws XmlParserException {

		int cpt = 0;
		int interactiveDataElementIndex = 0;

		for (int i = 0; i < racineNoeuds.getLength(); i++) {
			if (racineNoeuds.item(i).getNodeName().equals(XmlConfigDefinition.InteractiveData.ConfigName)) {
				cpt++;
				interactiveDataElementIndex = i;
			}
		}

		if (cpt == 0)
			throw new XmlParserException(NO_FIELD_STR + XmlConfigDefinition.InteractiveData.ConfigName);
		else if (cpt > 1)
			throw new XmlParserException(MORE_THAN_1_FIELD_STR + XmlConfigDefinition.InteractiveData.ConfigName);
		else {

			/* Il y a 2 éléments pour le BITE interactif, ActionsConfig et MenuConfig */
			NodeList interactiveNodeList = racineNoeuds.item(interactiveDataElementIndex).getChildNodes();

			checkActionsConfig(interactiveNodeList);
			checkMenuConfig(interactiveNodeList);
		}
	}

	/**
	 * Vérification de l'existence de la conf des actions dans le fichier XML
	 * 
	 * @param interactiveNodeList
	 * @throws XmlParserException
	 */
	private void checkActionsConfig(NodeList interactiveNodeList) throws XmlParserException {

		int cpt = 0;
		int interactiveDataElementIndex = 0;

		for (int i = 0; i < interactiveNodeList.getLength(); i++) {
			if (interactiveNodeList.item(i).getNodeName()
					.equals(XmlConfigDefinition.InteractiveData.ActionsConfig.ConfigName)) {
				cpt++;
				interactiveDataElementIndex = i;
			}
		}

		if (cpt == 0)
			throw new XmlParserException(NO_FIELD_STR + XmlConfigDefinition.InteractiveData.ActionsConfig.ConfigName);
		else if (cpt > 1)
			throw new XmlParserException(
					MORE_THAN_1_FIELD_STR + XmlConfigDefinition.InteractiveData.ActionsConfig.ConfigName);
		else
			interactiveDataActionsParser = new InteractiveActionParser(
					interactiveNodeList.item(interactiveDataElementIndex).getChildNodes());
	}

	/**
	 * Vérification de l'existence de la conf des menus dans le fichier XML
	 * 
	 * @param interactiveNodeList
	 * @throws XmlParserException
	 */
	private void checkMenuConfig(NodeList interactiveNodeList) throws XmlParserException {

		int cpt = 0;
		int interactiveDataElementIndex = 0;

		for (int i = 0; i < interactiveNodeList.getLength(); i++) {
			if (interactiveNodeList.item(i).getNodeName()
					.equals(XmlConfigDefinition.InteractiveData.MenuConfig.ConfigName)) {
				cpt++;
				interactiveDataElementIndex = i;
			}
		}

		if (cpt == 0)
			throw new XmlParserException(NO_FIELD_STR + XmlConfigDefinition.InteractiveData.MenuConfig.ConfigName);
		else if (cpt > 1)
			throw new XmlParserException(
					MORE_THAN_1_FIELD_STR + XmlConfigDefinition.InteractiveData.MenuConfig.ConfigName);
		else
			interactiveMenuParser = new DataParser(
					interactiveNodeList.item(interactiveDataElementIndex).getChildNodes(), "BITE Menu",
					XmlConfigDefinition.InteractiveData.MenuConfig.ElementName);
	}

	/**
	 * @return the periodicDataParser
	 */
	public PeriodicDataParser getPeriodicDataParser() {
		return periodicDataParser;
	}





	/**
	 * @return the frameConfigParser
	 */
	public FrameConfigParser getFrameConfigParser() {
		return frameConfigParser;
	}

	/**
	 * @return the usartConfigParser
	 */
	public UsartConfigParser getUsartConfigParser() {
		return usartConfigParser;
	}



	/**
	 * @return the interactiveDataActionsParser
	 */
	public InteractiveActionParser getInteractiveDataActionsParser() {
		return interactiveDataActionsParser;
	}

	/**
	 * @return the interactiveMenuParser
	 */
	public DataParser getInteractiveMenuParser() {
		return interactiveMenuParser;
	}

	/**
	 * Renvoie la version majeure de la conf XML
	 * 
	 * @return Version MAJOR
	 * @throws PrimaryAttributeNotFoundException
	 */
	public String getXmlMajorVersion() throws PrimaryAttributeNotFoundException {
		return getAttribute("XmlVersionMajor");
	}

	/**
	 * Renvoie la version mineure de la conf XML
	 * 
	 * @return Version MINOR
	 * @throws PrimaryAttributeNotFoundException
	 */
	public String getXmlMinorVersion() throws PrimaryAttributeNotFoundException {
		return getAttribute("XmlVersionMinor");
	}

	/**
	 * 
	 * @param attribute Nom de l'attribut
	 * @return Valeur de l'attribut
	 * @throws PrimaryAttributeNotFoundException Quand l'attribut demandé n'existe
	 *                                           pas
	 * 
	 */
	private String getAttribute(String attribute) throws PrimaryAttributeNotFoundException {

		String attributeValue = ((Element) racineNoeuds).getAttribute(attribute);

		if (attributeValue == null || attributeValue.equals(""))
			throw new PrimaryAttributeNotFoundException(attribute);

		return attributeValue;
	}

	/**
	 * 
	 * Classe gérant l'exception levée quand l'attribut demandé n'existe pas pas la
	 * conf primaire
	 * 
	 */
	public class PrimaryAttributeNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Quand l'attribut demandé n'est pas trouvé dans la config commune, on
		 * enregistre le message d'erreur.
		 * 
		 * @param attName Nom de l'attribut recherché
		 */
		public PrimaryAttributeNotFoundException(String attName) {
			super("L'attribut " + attName + " n'existe pas dans la conf XML primaire");
		}
	}

	/**
	 * Classe gérant l'exception levée quand l'analyse du fichier de configuration
	 * ne se termine pas correctement
	 * 
	 *
	 */
	public class XmlParserException extends Exception{

		private static final long serialVersionUID = 1L;
		
		/**
		 * Quand l'analyse du fichier XML ne se termine pas correctement, 
		 * on enregistre le message d'erreur.
		 * 
		 * @param s Message d'erreur
		 */
		public XmlParserException(String s) {
			super("Erreur d'analyse du fichier " + xmlFilePath + " : " + s);
		}
	}

	/**
	 * Classe gérant l'exception levée quand le fichier XML demandé n'existe pas
	 * 
	 */
	public class XmlFileNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Quand le fichier XML n'existe pas, on enregistre le message d'erreur.
		 * 
		 * @param s Message d'erreur
		 */
		public XmlFileNotFoundException() {
			super("Le fichier " + xmlFilePath + " est introuvable.");
		}
	}
	
	/**
	 * Classe gérant l'exception levée quand l'ouverture du fichier XML ne se
	 * termine pas correctement
	 * 
	 */
	public class XmlOpeningException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Quand l'ouverture du fichier XML ne se termine pas correctement, on
		 * enregistre le message d'erreur.
		 * 
		 * @param s Message d'erreur
		 */
		public XmlOpeningException() {
			super("Impossible d''ouvrir le fichier XML " + xmlFilePath);
		}
	}

	
}
