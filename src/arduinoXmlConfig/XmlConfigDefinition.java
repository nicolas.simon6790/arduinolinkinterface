/**
 * 
 */
package arduinoXmlConfig;

/**
 * Classe definissant les champs utilisés dans la conf XML
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Jun  7 17:05:01     2022 : 70-generation-de-la-taille-max-de-la-retval-du-bite-interactif 
 * Thu May 26 17:10:22     2022 : 84-envoi-requete-avec-parametre-configurable 
 * Tue Apr 12 18:46:12     2022 : 76-rendre-generique-le-code-des-donnees-periodiques 
 * Mon Apr  4 20:04:44     2022 : 73-mise-a-jour-code-auto-suite-a-passage-en-allocation-statique 
 * Thu Dec 23 17:16:13     2021 : 64-ameliorations-generation-de-code-bite 
 * Tue Dec 21 21:03:10     2021 : integration-sonar 
 * Sun Dec 19 18:34:57     2021 : 61-compatibilite-de-version-et-identification-arduino 
 * Wed Dec  1 16:56:12     2021 : 60-ajout-changement-valeur-wdg-dans-bite 
 * 
 *
 */
public class XmlConfigDefinition {
	

	public class PeriodicData{
		public static final String ConfigName = "PeriodicDataConfig";
		public static final String ElementName = "PeriodicData";
		public static final String CppConfigName = "CppConfig";
		public static final String DisplayTitleAttributeName = "DisplayTitle";

		public class CppFile{
			public static final String Name = "BitePeriodicData_cnf";
			public static final String HeaderComment = "BITE periodic data configuration file";
			public static final String IncludePath = "\"../../sensor/devices/generic/";
			
			public class FunctionsConfig{
				public static final String FunctionRetType = "uint8_t";
				public static final String FunctionParam = "uint8_t* dataPtr";
				public static final String FunctionSuffix = "_periodicData_fct";
				
				public class FunctionsDoxygenComment{
					
					private FunctionsDoxygenComment() {
					}

					public static final String Abstract = "Function for the periodic data : ";
					public static final String Param = "@param [out] dataPtr Pointer to the periodic data value";
					public static final String Return = "@return Validity of the data (0xFF = valid, 0x00 = invalid)";
				}
			}
			
			public class TableConfig{

				private TableConfig() {
				}
				public static final String TableType = "T_BITE_Periodic_data";
				public static final String TableName = "Bite_Periodic_data_tab";
				public static final String TableComment = "BITE periodic data configuration table";
			}

			public class DefineConfig{

				private DefineConfig() {
				}
				public static final String TransmissionPeriodDefineName = "BITE_MGT_PERIODIC_DATA_TRANSMIT_PERIOD_MS";
				public static final String TransmissionPeriodDefineComment = "Transmission period of the periodic data";
				public static final String TableSizeDefineName = "BITE_MGT_PERIODIC_CNF_TABLE_SIZE";
				public static final String TableSizeDefineComment = "Number of periodic data to send"; 
				public static final String ParamSizeDefineName = "BITE_MGT_PERIODIC_CNF_MAX_PARAM_SIZE";
				public static final String ParamSizeDefineComment = "Max size for param table";
				public static final String TransmissionPeriodAttributeName = "TransmissionPeriod";
			}			
		}
	}
	

	public class FrameConfig{
		public static final String ConfigName = "FrameConfig";
		public static final String STXAttributeName = "STX";
		public static final String ETXAttributeName = "ETX";
		public static final String TYPE_DATAAttributeName = "TYPE_DATA";
		public static final String TYPE_REQAttributeName = "TYPE_REQ";
		public static final String TYPE_ANSAttributeName = "TYPE_ANS";
		public static final String TYPE_READYAttributeName = "TYPE_READY";
		public static final String TYPE_DEBUGAttributeName = "TYPE_DEBUG";
		public static final String STATUS_ACKAttributeName = "STATUS_ACK";
		public static final String STATUS_NAKAttributeName = "STATUS_NAK";
		
		public class CppFile{

			private CppFile() {
			}
			public static final String Name = "BiteManager_frameConfig_cnf";
			public static final String HeaderComment = "BITE frame data configuration file";
			public static final String STXDefineName = "CHAR_STX";
			public static final String ETXDefineName = "CHAR_ETX";
			public static final String STXComment = "STX character definition";
			public static final String ETXComment = "ETX character definition";
			public static final String TypeDataDefineName = "BITE_MGT_CMD_TYPE_DAT";
			public static final String TypeReqDefineName = "BITE_MGT_CMD_TYPE_REQ";
			public static final String TypeAnsDefineName = "BITE_MGT_CMD_TYPE_ANS";
			public static final String TypeReadyDefineName = "BITE_MGT_CMD_TYPE_READY";
			public static final String TypeDebugDefineName = "BITE_MGT_CMD_TYPE_DEBUG";
			public static final String StatusAckDefineName = "BITE_MGT_CMD_STATUS_ACK";
			public static final String StatusNakDefineName = "BITE_MGT_CMD_STATUS_NAK";
			public static final String TypeDataComment = "TYPE_DATA character definition";
			public static final String TypeReqComment = "TYPE_REQ character definition";
			public static final String TypeAnsComment = "TYPE_ANS character definition";
			public static final String TypeReadyComment = "TYPE_READY character definition";
			public static final String TypeDebugComment = "TYPE_DEBUG character definition";
			public static final String StatusAckComment = "STATUS_ACK character definition";
			public static final String StatusNakComment = "STATUS_NAK character definition";
			public static final String XmlVersionMajorDefineName = "BITE_VERSION_MAJOR";
			public static final String XmlVersionMinorDefineName = "BITE_VERSION_MINOR";
			public static final String XmlVersionMajorComment = "BITE major version";
			public static final String XmlVersionMinorComment = "BITE minor version";
		}
	}


	public class InteractiveData {

		public static final String ConfigName = "InteractiveConfig";

		public class ActionsConfig {

			public static final String ConfigName = "ActionsConfig";
			public static final String ElementName = "InteractiveDataAction";
			public static final String CmdIdAttributeName = "CMD_ID";
			public static final String ActionFunctionAttributeName = "ActionFunction";
			public static final String RetSizeAttributeName = "RetValueSize";
			public static final String NeedAnswerAttributeName = "NeedAnswer";

			public class AnswerConfig {

				private AnswerConfig() {
				}
				public static final String ConfigName = "AnswerProcessing";
				public static final String AnswerTypeName = "AnswerType";
				public static final String PatternName = "InfoStringPattern";
				public static final String ValueFalseName = "ValueFalse";
				public static final String ValueTrueName = "ValueTrue";
			}

			public class CppFile {

				private CppFile() {
				}
				public static final String Name = "BiteInteractive_cnf";
				public static final String HeaderComment = "Interactive BITE configuration file";
				public static final String IdMacroName = "BITE_INTERACTIVE_ID_";
				public static final String IdMacroComment = "Bite Interactive command ID";

				public class TableConfig {

					private TableConfig() {
					}
					public static final String TableType = "T_BITE_Interactive_data";
					public static final String TableName = "bite_Interactive_data_tab";
					public static final String TableComment = "BITE interactive data configuration table";
					public static final String TableSizeDefineName = "BITE_INTERACTIVE_DATA_TAB_SIZE";
					public static final String TableSizeDefineComment = "BITE interactive data configuration table size";
					public static final String RetvalSizeDefineName = "BITE_INTERACTIVE_DATA_RETVAL_MAX_SIZE";
					public static final String RetvalSizeDefineComment = "BITE interactive data max retval size";
				}
			}
		}

		public class MenuConfig {

			private MenuConfig() {
			}

			public static final String ConfigName = "MenuConfig";

			public static final String ElementName = "Item";
			public static final int MaxStackDepth = 20;
			public static final String ItemNameAttribute = "ItemName";
			public static final String ActionIDAttribute = "ActionID";
			public static final String ActionParamAttribute = "ActionParam";
			public static final String UserParamAttribute = "UserParam";
			public static final String UserParamTextAttribute = "UserParamText";
		}
	}


	public class UsartConfig {

		public static final String ConfigName = "UsartConfig";
		public static final String PortConfig = "Port";
		public static final String BaudrateConfig = "Baudrate";
		public static final String DataBitsConfig = "DataBits";
		public static final String StopBitsConfig = "StopBits";
		public static final String ParityConfig = "Parity";

		public class UsartDefaultConfig {

			private UsartDefaultConfig() {
			}
			public static final String port = "COM3";
			public static final int baudrate = 57600;
			public static final int stopBits = 1;
			public static final int dataBits = 8;
			public static final int parity = 0;
		}

		public class CppFile {

			private CppFile() {
			}
			public static final String baudrateType = "T_usart_baudrates";
			public static final String dataBitsType = "T_usart_data_bits";
			public static final String stopBitsType = "T_usart_stop_bits";
			public static final String parityType = "T_usart_parity";
			public static final String baudrateConstName = "BITE_MGT_USART_BAUDRATE";
			public static final String baudrateCommentName = "Baudrate used for Usart communication";
			public static final String stopBitsConstName = "BITE_MGT_USART_STOPBITS";
			public static final String stopBitsCommentName = "Stop bits setting used for Usart communication";
			public static final String dataBitsConstName = "BITE_MGT_USART_DATABITS";
			public static final String dataBitsCommentName = "Data bits setting used for Usart communication";
			public static final String parityConstName = "BITE_MGT_USART_PARITY";
			public static final String parityCommentName = "Parity setting used for Usart communication";
		}
	}
}
