/**
 * 
 */
package arduinoXmlConfig.Generator;

import java.util.ArrayList;
import java.util.List;

import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Generator.CppWriter.CppWriterFileException;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataParserException;
import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser.PeriodicDataCppConfigNotFoundException;
import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser.SensorValueType;

/**
 * Classe en charge de la génération du code C++ des données périodiques 
 * à partir de la configuration XML
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Jun  7 16:34:03     2022 : 85-mauvaise-generation-de-la-table-des-data-periodiques 
 * Tue Apr 12 18:46:12     2022 : 76-rendre-generique-le-code-des-donnees-periodiques 
 * Mon Apr  4 20:04:43     2022 : 73-mise-a-jour-code-auto-suite-a-passage-en-allocation-statique 
 * Sun Jan  9 20:44:35     2022 : 67-lancement-de-la-generation-de-code-en-ligne-de-commande 
 * Fri Jan  7 21:24:28     2022 : 66-modification-de-la-generation-des-headers-de-fichiers-c 
 * Wed Jan  5 19:56:07     2022 : 65-changement-de-types-bite 
 * Thu Dec 23 17:53:19     2021 : 64-ameliorations-generation-de-code-bite 
 * Tue Dec 21 20:48:52     2021 : integration-sonar 
 * 
 *
 */
public class PeriodicDataCodeGenerator {

	private XmlConfigParser xmlParser;
		
	private CppWriter writerCpp;
	private CppWriter writerH;
	
	private int data_counter; 
	private int maxParamSize;
	
	/**
	 * Constructeur de la classe
	 * 
	 * @param xmlParser Référence vers l'analyseur XML à utiliser
	 */
	public PeriodicDataCodeGenerator(XmlConfigParser xmlParser) {
		this.xmlParser = xmlParser;
		data_counter = 0;
		maxParamSize = 0;
	}


	/**
	 * Génère le fichier C++ des données périodiques
	 * 
	 * @return TRUE si la génération est finie correctment, FALSE sinon
	 * 
	 */
	public boolean generatePeriodicData() {

		boolean retval = false;

		String path = CppWriter.getCppFilePathFromConfigFile();

		System.out.println("\nGeneration du fichier C++ " + path + "/"
				+ XmlConfigDefinition.PeriodicData.CppFile.Name + ".cpp");
		Logger.write(getClass(), LogLevel.INFO, "Generation du fichier C++ " + path + "/"
				+ XmlConfigDefinition.PeriodicData.CppFile.Name + ".cpp");
		System.out.println("Generation du fichier H " + path + "/"
				+ XmlConfigDefinition.PeriodicData.CppFile.Name + ".h");
		Logger.write(getClass(), LogLevel.INFO, "Generation du fichier H " + path + "/"
				+ XmlConfigDefinition.PeriodicData.CppFile.Name + ".h");
		
		try {
			writerCpp = new CppWriter(path, XmlConfigDefinition.PeriodicData.CppFile.Name + ".cpp");
			writerH = new CppWriter(path, XmlConfigDefinition.PeriodicData.CppFile.Name + ".h");
			
			writerCpp.generateFileHeader(XmlConfigDefinition.PeriodicData.CppFile.HeaderComment);
			writerH.generateFileHeader(XmlConfigDefinition.PeriodicData.CppFile.HeaderComment);
			writerH.generateIfDef(XmlConfigDefinition.PeriodicData.CppFile.Name);
			
			/* Génération fichier Cpp */
			writePeriodicDataIncludes();
			writePeriodicDataTable();
			
			/* Génération fichier H */
			writePeriodicDataConstants();
			
			writePeriodicDataFooter();
			
			writerCpp.close();
			writerH.close();
			System.out.println("Fin de generation des donnees periodiques");
			Logger.write(getClass(), LogLevel.INFO, "Fin de generation des données periodiques");
			
			retval = true;

		} catch (CppWriterFileException | DataParserException | DataAttributeNotFoundException
				| PeriodicDataCppConfigNotFoundException | CommonAttributeNotFoundException e) {
			System.err.println(e.getMessage());
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
			System.out.println("\nErreur lors de la generation des donnees periodiques...");
			Logger.write(getClass(), LogLevel.ERROR, "Erreur lors de la generation des donnees periodiques...");
		}

		return retval;
	
	}
	
	
	/**
	 * Génération du footer du fichier C++ 
	 * @throws CppWriterFileException 
	 */
	private void writePeriodicDataFooter() throws CppWriterFileException {		
		writerH.generateEndif(XmlConfigDefinition.PeriodicData.CppFile.Name);
	}
	
	/**
	 * Génération des include du fichier C++
	 * @throws CppWriterFileException 
	 * @throws PeriodicDataParserException 
	 * @throws PeriodicDataAttributeNotFoundException 
	 * @throws PeriodicDataCppConfigNotFoundException 
	 */
	private void writePeriodicDataIncludes() throws CppWriterFileException {
		List<String> list = new ArrayList<>();
		
		list.add("<avr/io.h>");
		list.add("\"../../../os/service/generic/PeriodicService.h\"");
		list.add("\"../../asw_manager/generic/asw_manager.h\"");
		list.add("\"../generic/BitePeriodicData.h\"");
		
		writerCpp.generateIncludes(list);		
	}
	
	/**
	 * Génération de la configuration commune des données périodiques sous forme de
	 * constantes C++
	 * 
	 * @throws CppWriterFileException
	 * @throws CommonAttributeNotFoundException
	 * @throws PeriodicDataDateTimeConfigNotFoundException
	 */
	private void writePeriodicDataConstants() throws CppWriterFileException, CommonAttributeNotFoundException {

		List<String> type = new ArrayList<>();
		List<String> name = new ArrayList<>();
		List<String> param = new ArrayList<>();
		List<String> comment = new ArrayList<>();
		
		/* Période de transmission */
		type.add(CppWriter.TYPE_PERIOD);
		name.add(XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.TransmissionPeriodDefineName);
		int periodMs = Integer
				.valueOf(xmlParser.getPeriodicDataParser().getCommonAttribute(
						XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.TransmissionPeriodAttributeName))
				* 1000;
		param.add(String.valueOf(periodMs));
		comment.add(XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.TransmissionPeriodDefineComment);

		/* Taille de la table */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.TableSizeDefineName);
		param.add(Integer.toString(data_counter));
		comment.add(XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.TableSizeDefineComment);

		/* Taille param */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.ParamSizeDefineName);
		param.add(Integer.toString(maxParamSize));
		comment.add(XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.ParamSizeDefineComment);

		writerH.generateConstants(type, name, param, comment);
	}
	

	/**
	 * Génération de la table des données périodiques
	 * 
	 * @throws PeriodicDataAttributeNotFoundException
	 * @throws PeriodicDataParserException
	 * @throws CppWriterFileException
	 * @throws CommonAttributeNotFoundException
	 * @throws PeriodicDataCppConfigNotFoundException
	 */
	private void writePeriodicDataTable()
			throws DataAttributeNotFoundException, DataParserException, CppWriterFileException,
			CommonAttributeNotFoundException, PeriodicDataCppConfigNotFoundException {
		
		/* On retourne au début de la liste */
		xmlParser.getPeriodicDataParser().resetCurrentDataIndex();
		
		List<List<String>> data = new ArrayList<>();
		
		/* Tant qu'il y a une donnée à générer */
		do {
			/* Valeur courante */
			List<String> list = new ArrayList<>();
			list.add(String.valueOf(xmlParser.getPeriodicDataParser().getElementID(SensorValueType.CURRENT)));
			String size = xmlParser.getPeriodicDataParser().getDataAttribute("Size");
			updateMaxParamSize(Integer.parseInt(size));
			list.add(size);
			list.add(String.valueOf(xmlParser.getPeriodicDataParser().getPeriodicDataCppConfig().getGroup()));
			list.add(String.valueOf(xmlParser.getPeriodicDataParser().getPeriodicDataCppConfig().getId()));
			int periodInCycles = (xmlParser.getPeriodicDataParser().getPeriodicDataCppConfig().getPeriod()
					/ Integer.valueOf(xmlParser.getPeriodicDataParser().getCommonAttribute(
							XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.TransmissionPeriodAttributeName)));
			list.add(String.valueOf(periodInCycles));
			list.add("CURRENT");
			list.add(xmlParser.getPeriodicDataParser().getPeriodicDataCppConfig().getServiceType());
			list.add(xmlParser.getPeriodicDataParser()
					.getDataAttribute(XmlConfigDefinition.PeriodicData.DisplayTitleAttributeName) + " current");
			data.add(list);	
			data_counter++;

			/* Valeur Max */
			if (xmlParser.getPeriodicDataParser().isSensorMaxValueActive())
				generateMinMaxData(data, "MAX", Integer.parseInt(size));

			/* Valeur Min */
			if (xmlParser.getPeriodicDataParser().isSensorMinValueActive())
				generateMinMaxData(data, "MIN", Integer.parseInt(size));
		}
		while (xmlParser.getPeriodicDataParser().goToNextData());

		writerCpp.generateStructureTable(XmlConfigDefinition.PeriodicData.CppFile.TableConfig.TableType,
				XmlConfigDefinition.PeriodicData.CppFile.TableConfig.TableName, 
				data, true);
		
		List<String> type = new ArrayList<>();
		List<String> name = new ArrayList<>();
		List<String> comment = new ArrayList<>();
		List<Boolean> isConst = new ArrayList<>();
		type.add(XmlConfigDefinition.PeriodicData.CppFile.TableConfig.TableType);
		name.add(XmlConfigDefinition.PeriodicData.CppFile.TableConfig.TableName + "[]");
		comment.add(XmlConfigDefinition.PeriodicData.CppFile.TableConfig.TableComment);
		isConst.add(true);
		writerH.generateExternDeclaration(type, name, comment, isConst);
	}
	
	/**
	 * Génération de la config min/max de la table
	 * 
	 * @param data Liste contenant la table de conf générée
	 * @param type Type de donnée (min/max)
	 * @throws PeriodicDataCppConfigNotFoundException
	 * @throws CommonAttributeNotFoundException
	 * @throws NumberFormatException
	 * @throws DataAttributeNotFoundException
	 */
	private void generateMinMaxData(List<List<String>> data, String type, int size)
			throws PeriodicDataCppConfigNotFoundException, NumberFormatException, CommonAttributeNotFoundException,
			DataAttributeNotFoundException {

		List<String> list = new ArrayList<>();

		if (type.equals("MAX"))
			list.add(String.valueOf(xmlParser.getPeriodicDataParser().getElementID(SensorValueType.MAX)));
		else
			list.add(String.valueOf(xmlParser.getPeriodicDataParser().getElementID(SensorValueType.MIN)));

		int newSize = size + 3;
		updateMaxParamSize(newSize);

		list.add(String.valueOf(newSize));
		list.add(String.valueOf(xmlParser.getPeriodicDataParser().getPeriodicDataCppConfig().getGroup()));
		list.add(String.valueOf(xmlParser.getPeriodicDataParser().getPeriodicDataCppConfig().getId()));

		int periodInCycles = (xmlParser.getPeriodicDataParser().getPeriodicDataCppConfig().getPeriodMinMax()
				/ Integer.valueOf(xmlParser.getPeriodicDataParser().getCommonAttribute(
						XmlConfigDefinition.PeriodicData.CppFile.DefineConfig.TransmissionPeriodAttributeName)));
		list.add(String.valueOf(periodInCycles));

		list.add(type);
		list.add(xmlParser.getPeriodicDataParser().getPeriodicDataCppConfig().getServiceType());
		list.add(xmlParser.getPeriodicDataParser()
				.getDataAttribute(XmlConfigDefinition.PeriodicData.DisplayTitleAttributeName) + " " + type);

		data.add(list);
		data_counter++;
	}

	/**
	 * Mise à jour de la taille max du champ PARAM
	 * 
	 * @param size
	 */
	private void updateMaxParamSize(int size) {
		if (size > maxParamSize)
			maxParamSize = size;
	}
	
}
