/**
 * 
 */
package arduinoXmlConfig.Generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import arduinoTools.ArduinoConfigLoader;
import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;

/**
 * Classe permettant de générer du code C++
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Mon Apr  4 19:43:35     2022 : 73-mise-a-jour-code-auto-suite-a-passage-en-allocation-statique 
 * Sun Jan  9 20:44:34     2022 : 67-lancement-de-la-generation-de-code-en-ligne-de-commande 
 * Fri Jan  7 21:24:27     2022 : 66-modification-de-la-generation-des-headers-de-fichiers-c 
 * Wed Jan  5 19:56:07     2022 : 65-changement-de-types-bite 
 * Thu Dec 23 18:16:51     2021 : 64-ameliorations-generation-de-code-bite 
 * Tue Dec 21 20:48:50     2021 : integration-sonar 
 * 
 *
 */
public class CppWriter {
	
	/**
	 * Reference vers l'onjet File
	 */
	private FileWriter cppFile;

	/**
	 * Chemin du fichier généré
	 */
	private String filePath;
	
	/**
	 * Nom du fichier généré
	 */
	private String fileName;

	private static final String ERROR_WRITING_FILE = "\nErreur lors de l'ecriture dans le fichier ";
	private static final String ERROR_OPENING_FILE = "\nErreur lors de l'ouverture du fichier ";
	private static final String ERROR_CLOSING_FILE = "\nErreur lors de la fermeture du fichier ";

	/**
	 * Type C++ uint8_t
	 */
	public static final String TYPE_UINT8 = "uint8_t";

	/**
	 * Type C++ uint16_t
	 */
	public static final String TYPE_UINT16 = "uint16_t";

	/**
	 * Type C++ t_time_ms
	 */
	public static final String TYPE_PERIOD = "t_time_ms";

	/**
	 * Mot-clé const C++
	 */
	public static final String KEYWORD_CONST = "const ";

	/**
	 * Mot-clé extern C++
	 */
	public static final String KEYWORD_EXTERN = "extern ";

	/**
	 * Mot-clé #define C++
	 */
	public static final String KEYWORD_DEFINE = "#define ";

	/**
	 * Constructeur de la classe. Créée le fichier C++
	 * 
	 * @param path     Répertoire du fichier C++
	 * @param fileName Nom du fichier
	 * @throws CppWriterFileException
	 */
	public CppWriter(String path, String fileName) throws CppWriterFileException {

		this.fileName = fileName;
		this.filePath = arduinoTools.SystemTools.getCurJARPath() + "/" + path;
		File rep = new File(filePath);

		if (!rep.exists())
			rep.mkdir();
		
		filePath = filePath + "/" + fileName;
		
		/* Créer fichier C++ */
		try {
			cppFile = new FileWriter(new File(filePath), false);
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_OPENING_FILE + filePath);
		}
	}
	
	/**
	 * Ferme le fichier C++
	 * @throws CppWriterFileException Exception levée en cas de problème lors de la fermeture du fichier
	 */
	public void close() throws CppWriterFileException {
		
		/* Fermeture du fichier */
		try {
			cppFile.close();
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_CLOSING_FILE + this.filePath);
		}
	}
	
	/**
	 * Génération du #ifdef au début du fichier H
	 * 
	 * @param fileName Name of H file
	 * @throws CppWriterFileException Exception levée en cas de problème lors de l'écriture dans le fichier
	 */
	public void generateIfDef(String fileName) throws CppWriterFileException {
		try {
			cppFile.append("\n\n");
			cppFile.append("#ifndef WORK_ASW_BITE_MANAGER_CONFIG_" + fileName.toUpperCase() + "_H_\n");
			cppFile.append("#define WORK_ASW_BITE_MANAGER_CONFIG_" + fileName.toUpperCase() + "_H_");
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
	}
	
	/**
	 * Génération du #endif à la fin du fichier H
	 * 
	 * @param fileName Name of H file
	 * @throws CppWriterFileException Exception levée en cas de problème lors de l'écriture dans le fichier
	 */
	public void generateEndif(String fileName) throws CppWriterFileException {
		try {
			cppFile.append("\n\n");
			cppFile.append("#endif");
			cppFile.append(" /* WORK_ASW_BITE_MANAGER_CONFIG_" + fileName.toUpperCase() + "_H_ */\n");
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
	}
	
	/**
	 * Génère un commentaire Doxygen
	 * 
	 * @param s Contient le commentaire Doxygen à formater
	 * @throws CppWriterFileException Exception levée en cas de problème lors de l'écriture dans le fichier
	 */
	public void generateDoxygenComment(String s) throws CppWriterFileException {
		try {
			cppFile.append("/*!\n * ");
			
			while(!s.isEmpty()) {	
				if(s.charAt(0) == '@')
					cppFile.append("\n *\n * ");
				
				cppFile.append(s.charAt(0));
				
				s = s.substring(1);
			}
			
			cppFile.append("\n *\n */\n");
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
	}
	
	/**
	 * Génère les includes du fichier C++
	 * 
	 * @param strList Liste des include à insérer
	 * @throws CppWriterFileException Exception levée en cas de problème lors de l'écriture dans le fichier
	 */
	public void generateIncludes(List<String> strList) throws CppWriterFileException {
		try {
			cppFile.append("\n\n");
			cppFile.append("/* Included files */\n");
			for(int i = 0; i< strList.size(); i++) {
				cppFile.append("#include " + strList.get(i) + "\n");
			}
			
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
	}
	
	/**
	 * Génération d'une fonction C++
	 * 
	 * @param returnType Type de retour 
	 * @param name Nom de la fonction
	 * @param paramList Liste des paramètres de la fonction (null si il n'y en a pas)
	 * @param code Code de la fonction
	 * @throws CppWriterFileException En cas d'erreur d'écriture sur le fichier
	 */
	public void generateFunction(String returnType, String name, List<String> paramList, String code, String doxyComment) throws CppWriterFileException {
		try {
			cppFile.append("\n\n");
			generateDoxygenComment(doxyComment);
			cppFile.append(returnType + " " + name + "(");
			
			if(paramList != null) {
				for(int i=0; i< paramList.size(); i++) {
					cppFile.append(paramList.get(i));
					if(i != paramList.size() - 1)
						cppFile.append(", ");
				}
			}
			
			code = code.replace("\n", "\n\t");
			String tab = "\t";
			code = tab.concat(code);
			cppFile.append(")\n{\n" + code + "\n}\n");
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
	}
	
	/**
	 * Génération d'une table de structures
	 * 
	 * @param type    Type de structure
	 * @param name    Nom de la table
	 * @param data    Liste contenant les données de la table
	 * @param isConst Déclaration de la table en constante ou non
	 * @throws CppWriterFileException En cas d'erreur d'écriture sur le fichier
	 */
	public void generateStructureTable(String type, String name, List<List<String>> data, boolean isConst)
			throws CppWriterFileException {
		
		try {
			cppFile.append("\n\n");
			cppFile.append(KEYWORD_EXTERN);

			if (isConst)
				cppFile.append(KEYWORD_CONST);

			cppFile.append(type + " " + name + "[] = \n{\n");
			
			for(int i=0; i<data.size(); i++) {
				cppFile.append("\t{");
				
				for(int j=0; j<data.get(i).size() - 1; j++) {
					cppFile.append(data.get(i).get(j));
					if(j < data.get(i).size()-2)
						cppFile.append(", ");
				}
				
				cppFile.append("}");
				if(i < data.size()-1)
					cppFile.append(",");
				
				writeLineDoxygenComment(data.get(i).get(data.get(i).size()-1));
				
				cppFile.append("\n");
			}
			
			cppFile.append("};\n");
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
	}
	
	/**
	 * Génération des #define
	 * 
	 * @param name Table contenant les noms des defines
	 * @param param Table contenant les parametres des defines
	 * @param doxyComment Commentaires Doxygen associés
	 * @throws CppWriterFileException En cas d'erreur d'écriture sur le fichier
	 */
	public void generateDefine(List<String> name, List<String> param, List<String> doxyComment) throws CppWriterFileException {
		try {
			cppFile.append("\n\n");
			cppFile.append("/* Define part */\n");
			
			for(int i=0; i<name.size(); i++) {
				writeLineDoxygenComment(doxyComment.get(i));
				cppFile.append(KEYWORD_DEFINE + name.get(i));

				if (!param.isEmpty() && param.get(i) != null)
					cppFile.append("\t\t" + param.get(i));

				cppFile.append("\n");
				cppFile.append("\n");
			}
			
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
	}
	
	/**
	 * Génération des constantes
	 * 
	 * @param type        Table contenant les types des constantes
	 * @param name        Table contenant les noms des constantes
	 * @param param       Table contenant les valeurs des constantes
	 * @param doxyComment Commentaires Doxygen associés
	 * @throws CppWriterFileException En cas d'erreur d'écriture sur le fichier
	 */
	public void generateConstants(List<String> type, List<String> name, List<String> param, List<String> doxyComment)
			throws CppWriterFileException {
		try {
			cppFile.append("\n\n");
			cppFile.append("/* Constants declarations */\n");

			for (int i = 0; i < name.size(); i++) {
				writeLineDoxygenComment(doxyComment.get(i));
				cppFile.append(KEYWORD_CONST + type.get(i) + "\t" + name.get(i) + "\t\t = " + param.get(i) + ";\n\n");
			}

		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
	}

	/**
	 * Génère les declarations des variables externes dans le fichier H
	 * 
	 * @param type        Type des variables
	 * @param name        Noms des variables
	 * @param doxyComment Commentaires Doxygen associés
	 * @param isConst     Variable constante ou non
	 * @throws CppWriterFileException En cas d'erreur d'écriture sur le fichier
	 */
	public void generateExternDeclaration(List<String> type, List<String> name, List<String> doxyComment,
			List<Boolean> isConst) throws CppWriterFileException {
		try {
			cppFile.append("\n\n/* External variables */\n");
			
			for(int i=0; i<type.size(); i++)
			{
				writeLineDoxygenComment(doxyComment.get(i));

				cppFile.append(KEYWORD_EXTERN);

				if (isConst.get(i).equals(true))
					cppFile.append(KEYWORD_CONST);

				cppFile.append(type.get(i) + " " + name.get(i) + ";");
				cppFile.append("\n");
			}
			
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}
		
	}
	
	/**
	 * Génère le header du fichier généré
	 * 
	 * @param brief Texte "@brief" du commentaire Doxygen
	 * @throws CppWriterFileException En cas d'erreur d'écriture sur le fichier
	 */
	public void generateFileHeader(String brief) throws CppWriterFileException {

		try {
			cppFile.append("/********************************************/\n");
			cppFile.append("/**  AUTO-GENERATED FILE : DO NOT MODIFY   **/\n");
			cppFile.append("/**  OR IT WON'T MATCH HOST CONFIGURATION  **/\n");
			cppFile.append("/********************************************/\n");
			cppFile.append("\n\n");
		} catch (IOException e) {
			throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
		}

		DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();

		generateDoxygenComment("@file " + fileName + "@brief " + brief + "@date " + format.format(date));

	}

	/**
	 * Génère un commentaire sur une ligne
	 * 
	 * @param comment Commentaire à écrire
	 * @throws CppWriterFileException En cas d'erreur d'écriture sur le fichier
	 */
	public void writeLineComment(String comment) throws CppWriterFileException {
		if (!comment.equals("")) {
			try {
				cppFile.append("/* " + comment + " */\n");
			} catch (IOException e) {
				throw new CppWriterFileException(ERROR_WRITING_FILE + this.filePath);
			}
		}
	}

	private void writeLineDoxygenComment(String text) throws IOException {
		if (!text.equals(""))
			cppFile.append("/*!< " + text + " */\n");
	}
	
	
	/**
	 * Classe gérant les exceptions levées lors de l'ouverture ou la fermeture du fichier C++
	 * 
	 */
	public class CppWriterFileException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		/**
		 * Constructeur
		 * @param s Nom du fichier C++
		 */
		public CppWriterFileException(String s) {
				super(s);
		}
		
	}

	/**
	 * Récupère le répertoire où placer les fichiers C++ générés dans le fichier de
	 * configuration
	 * 
	 * @return Chemin des fichiers générés
	 */
	public static String getCppFilePathFromConfigFile() {

		String path;
		try {
			path = ArduinoConfigLoader.getConfLoader().getConfigValue("CppFilePath");
			Logger.write("CppWriter", LogLevel.INFO,
					"Chemin de generation des fichiers C++ trouve, les fichiers seront generes dans le repertoire "
							+ path);
		} catch (ArduinoConfigLoaderException e1) {
			Logger.write("CppWriter", LogLevel.WARNING,
					"Chemin de generation des fichiers C++ non trouve, utilisation du chemin par defaut");
			return ".";
		}

		return path;
	}
}
