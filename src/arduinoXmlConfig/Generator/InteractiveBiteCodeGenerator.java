/**
 * 
 */
package arduinoXmlConfig.Generator;

import java.util.ArrayList;
import java.util.List;

import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Generator.CppWriter.CppWriterFileException;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataParserException;

/**
 * Classe en charge de la génération du code C++ des données du BITE Interactif
 * à partir de la configuration XML
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Jun  7 17:05:01     2022 : 70-generation-de-la-taille-max-de-la-retval-du-bite-interactif 
 * Tue Apr 12 17:17:20     2022 : 76-rendre-generique-le-code-des-donnees-periodiques 
 * Mon Apr  4 20:04:30     2022 : 73-mise-a-jour-code-auto-suite-a-passage-en-allocation-statique 
 * Sun Jan  9 20:44:35     2022 : 67-lancement-de-la-generation-de-code-en-ligne-de-commande 
 * Fri Jan  7 21:24:28     2022 : 66-modification-de-la-generation-des-headers-de-fichiers-c 
 * Thu Dec 23 17:53:19     2021 : 64-ameliorations-generation-de-code-bite 
 * Tue Dec 21 20:48:51     2021 : integration-sonar 
 * 
 *
 */
public class InteractiveBiteCodeGenerator {

	private XmlConfigParser xmlParser;

	private CppWriter writerCpp;
	private CppWriter writerH;

	/**
	 * Sauvegarde de la taille maximale de la retval
	 */
	private int maxRetvalSize;

	/**
	 * Constructeur de la classe
	 * 
	 * @param xmlParser Référence vers l'analyseur XML à utiliser
	 */
	public InteractiveBiteCodeGenerator(XmlConfigParser xmlParser) {
		this.xmlParser = xmlParser;
		this.maxRetvalSize = 0;
	}

	/**
	 * Génère le fichier C++ des données du BITE Interactif
	 * 
	 * @throws CppWriterFileException           En cas d'erreur d'écriture dans les
	 *                                          fichiers
	 * @throws CommonAttributeNotFoundException En cas d'erreur dans la
	 *                                          configuration XML
	 * @throws DataParserException              En cas d'erreur dans la
	 *                                          configuration XML
	 * @throws DataAttributeNotFoundException   En cas d'erreur dans la
	 *                                          configuration XML
	 * 
	 * @return TRUE si la génération est finie correctment, FALSE sinon
	 */
	public boolean generateInteractiveData() {

		boolean retval = false;
		String path = CppWriter.getCppFilePathFromConfigFile();

		System.out.println("\nGeneration du fichier C++ " + path + "/"
				+ XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.Name + ".cpp");
		Logger.write(getClass(), LogLevel.INFO, "Generation du fichier C++ " + path + "/"
				+ XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.Name + ".cpp");
		System.out.println("Generation du fichier H " + path + "/"
				+ XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.Name + ".h");
		Logger.write(getClass(), LogLevel.INFO, "Generation du fichier H " + path + "/"
				+ XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.Name + ".h");
		
		try {
			writerCpp = new CppWriter(path, XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.Name + ".cpp");
			writerH = new CppWriter(path, XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.Name + ".h");

			writerCpp.generateFileHeader(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.HeaderComment);
			writerH.generateFileHeader(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.HeaderComment);
			writerH.generateIfDef(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.Name);

			/* Génération fichier C++ */
			writeInteractiveDataIncludes();
			writeInteractiveDataTable();

			/* Génération fichier H */
			writeInteractiveDataConstants();

			writeInteractiveDataFooter();

			writerCpp.close();
			writerH.close();
			System.out.println("Fin de generation des donnees du BITE interactif");
			Logger.write(getClass(), LogLevel.INFO, "Fin de generation des donnees du BITE interactif");

			retval = true;
		}
		catch (CppWriterFileException | DataParserException
				| DataAttributeNotFoundException e) {
			System.err.println(e.getMessage());
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
			System.out.println("\nErreur lors de la generation des donnees periodiques...");
			Logger.write(getClass(), LogLevel.ERROR, "Erreur lors de la generation des donnees periodiques...");
		}

		return retval;
	}


	/**
	 * Génération du footer du fichier C++
	 * 
	 * @throws CppWriterFileException
	 */
	private void writeInteractiveDataFooter() throws CppWriterFileException {
		writerH.generateEndif(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.Name);
	}

	/**
	 * Génération des include du fichier C++
	 * 
	 * @throws CppWriterFileException
	 */
	private void writeInteractiveDataIncludes() throws CppWriterFileException {
		List<String> list = new ArrayList<>();

		list.add("<avr/io.h>");
		list.add("\"../../../os/service/generic/PeriodicService.h\"");
		list.add("\"../../../bsw/usart/generic/Usart.h\"");
		list.add("\"../../../bsw/dio/generic/Dio.h\"");
		list.add("\"../generic/BiteFrame.h\"");
		list.add("\"../generic/BiteInteractive.h\"");
		list.add("\"BiteInteractive_cnf.h\"");
		list.add("\"BiteInteractive_actions.h\"");

		writerCpp.generateIncludes(list);
	}

	/**
	 * Génération de la table des actions du BITE intéractif
	 * 
	 * @throws CppWriterFileException
	 * @throws DataParserException
	 * @throws DataAttributeNotFoundException
	 */
	private void writeInteractiveDataTable()
			throws CppWriterFileException, DataParserException, DataAttributeNotFoundException {

		/* On retourne au début de la liste */
		xmlParser.getInteractiveDataActionsParser().resetCurrentDataIndex();
		
		List<List<String>> data = new ArrayList<>();
		
		/* Commentaire formatter off */
		writerCpp.writeLineComment("@formatter:off");
		
		/* Tant qu'il y a une donnée à générer */
		do {
			List<String> list = new ArrayList<>();
			list.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.IdMacroName
					+ xmlParser.getInteractiveDataActionsParser()
							.getDataAttribute(
									XmlConfigDefinition.InteractiveData.ActionsConfig.ActionFunctionAttributeName)
							.toUpperCase());
			list.add("&" + xmlParser.getInteractiveDataActionsParser()
					.getDataAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.ActionFunctionAttributeName));

			int retvalSize = Integer.parseInt(xmlParser.getInteractiveDataActionsParser()
					.getDataAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.RetSizeAttributeName));
			if (retvalSize > maxRetvalSize)
				maxRetvalSize = retvalSize;
			list.add(String.valueOf(retvalSize));

			list.add("ID: "
					+ xmlParser.getInteractiveDataActionsParser()
							.getDataAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.CmdIdAttributeName)
					+ ", Function to call: "
					+ xmlParser.getInteractiveDataActionsParser().getDataAttribute(
							XmlConfigDefinition.InteractiveData.ActionsConfig.ActionFunctionAttributeName));
			data.add(list);	
		}
		while (xmlParser.getInteractiveDataActionsParser().goToNextData());
		
		writerCpp.generateStructureTable(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.TableType,
				XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.TableName, 
				data, true);

		/* Commentaire formatter on */
		writerCpp.writeLineComment("@formatter:on");

		/* Déclaration extern fichier H */
		List<String> type = new ArrayList<>();
		List<String> name = new ArrayList<>();
		List<String> comment = new ArrayList<>();
		List<Boolean> isConst = new ArrayList<>();
		type.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.TableType);
		name.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.TableName + "[]");
		comment.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.TableComment);
		isConst.add(true);
		writerH.generateExternDeclaration(type, name, comment, isConst);
	}

	/**
	 * Génération de la configuration des données du BITE interactif sous forme de
	 * constantes C++
	 * 
	 * @throws CppWriterFileException
	 * @throws CommonAttributeNotFoundException
	 * @throws DataAttributeNotFoundException
	 * @throws DataParserException
	 */
	private void writeInteractiveDataConstants()
			throws CppWriterFileException, DataAttributeNotFoundException,
			DataParserException {
		List<String> type = new ArrayList<>();
		List<String> name = new ArrayList<>();
		List<String> param = new ArrayList<>();
		List<String> comment = new ArrayList<>();

		/* Taille de la table */
		type.add("uint8_t");
		name.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.TableSizeDefineName);
		param.add(Integer.toString(xmlParser.getInteractiveDataActionsParser().getDataCount()));
		comment.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.TableSizeDefineComment);

		/* Taille max de la retval */
		type.add("uint8_t");
		name.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.RetvalSizeDefineName);
		param.add(String.valueOf(maxRetvalSize));
		comment.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.TableConfig.RetvalSizeDefineComment);

		/* Definition des ID sous forme de constantes */
		/* On retourne au début de la liste */
		xmlParser.getInteractiveDataActionsParser().resetCurrentDataIndex();

		do {
			type.add("uint8_t");
			name.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.IdMacroName
					+ xmlParser.getInteractiveDataActionsParser()
							.getDataAttribute(
									XmlConfigDefinition.InteractiveData.ActionsConfig.ActionFunctionAttributeName)
							.toUpperCase());
			param.add(xmlParser.getInteractiveDataActionsParser()
					.getDataAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.CmdIdAttributeName));
			comment.add(XmlConfigDefinition.InteractiveData.ActionsConfig.CppFile.IdMacroComment);
		} while (xmlParser.getInteractiveDataActionsParser().goToNextData());

		writerH.generateConstants(type, name, param, comment);
	}

}
