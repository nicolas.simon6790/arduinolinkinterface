/**
 * 
 */
package arduinoXmlConfig.Generator;

import java.util.ArrayList;
import java.util.List;

import javax.management.AttributeNotFoundException;

import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Generator.CppWriter.CppWriterFileException;
import arduinoXmlConfig.Parser.UsartConfigParser;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.XmlConfigParser.PrimaryAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;

/**
 * Classe en charge de la génération du code C++ des données de trame 
 * à partir de la configuration XML
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Sun Jan  9 20:44:35     2022 : 67-lancement-de-la-generation-de-code-en-ligne-de-commande 
 * Fri Jan  7 21:24:28     2022 : 66-modification-de-la-generation-des-headers-de-fichiers-c 
 * Thu Dec 23 17:25:02     2021 : 64-ameliorations-generation-de-code-bite 
 * Tue Dec 21 20:48:51     2021 : integration-sonar 
 * Sun Dec 19 19:07:16     2021 : 61-compatibilite-de-version-et-identification-arduino 
 * 
 *
 */
public class FrameDataCodeGenerator {

	private XmlConfigParser xmlParser;
	
	private CppWriter writerH;
	
	


	/**
	 * Constructeur de la classe
	 * 
	 * @param xmlParser Référence vers l'analyseur XML à utiliser
	 */
	public FrameDataCodeGenerator(XmlConfigParser xmlParser) {
		this.xmlParser = xmlParser;
	}
	
	
	/**
	 * Génère le fichier H des données de trame
	 * 
	 * @return TRUE si la génération est finie correctment, FALSE sinon
	 * 
	 */
	public boolean generateFrameData() {

		boolean retval = false;

		String path = CppWriter.getCppFilePathFromConfigFile();

		System.out.println("\nGeneration du fichier H " + path + "/"
				+ XmlConfigDefinition.FrameConfig.CppFile.Name + ".h");
		Logger.write(getClass(), LogLevel.INFO, "Generation du fichier H " + path + "/"
				+ XmlConfigDefinition.FrameConfig.CppFile.Name + ".h");
		
		try {
			writerH = new CppWriter(path, XmlConfigDefinition.FrameConfig.CppFile.Name + ".h");
			
			writerH.generateFileHeader(XmlConfigDefinition.FrameConfig.CppFile.HeaderComment);
			writerH.generateIfDef(XmlConfigDefinition.FrameConfig.CppFile.Name);
			
			/* Génération fichier H */
			writeXmlVersionConsts();
			writeFrameDataConsts();
			writeUsartDataConsts();

			writeFrameDataFooter();
			
			writerH.close();
			System.out.println("Fin de generation des donnees de trame");
			Logger.write(getClass(), LogLevel.INFO, "Fin de generation des donnees de trame");
			
			retval = true;

		} catch (CppWriterFileException | CommonAttributeNotFoundException | PrimaryAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
			System.out.println("\nErreur lors de la generation des donnees de trame...");
			Logger.write(getClass(), LogLevel.ERROR, "Erreur lors de la generation des donnees de trame...");
		}
	
		return retval;
	}
	
	
	/**
	 * Génération du footer du fichier C++ 
	 * @throws CppWriterFileException 
	 */
	private void writeFrameDataFooter() throws CppWriterFileException {		
		writerH.generateEndif(XmlConfigDefinition.FrameConfig.CppFile.Name);
	}
	
	
	/**
	 * Génération de la configuration des trames sous forme de constantes C++
	 * 
	 * @throws CppWriterFileException
	 * @throws FrameConfigAttributeNotFoundException
	 */
	private void writeFrameDataConsts() throws CppWriterFileException, CommonAttributeNotFoundException {
		List<String> type = new ArrayList<>();
		List<String> name = new ArrayList<>();
		List<String> param = new ArrayList<>();
		List<String> comment = new ArrayList<>();
		
		/* STX */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.STXDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getSTX()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.STXComment);
		
		/* ETX */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.ETXDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getETX()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.ETXComment);
		
		/* TYPE_DATA */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.TypeDataDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getTypeData()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.TypeDataComment);
		
		/* TYPE_REQ */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.TypeReqDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getTypeReq()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.TypeReqComment);

		/* TYPE_ANS */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.TypeAnsDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getTypeAns()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.TypeAnsComment);

		/* TYPE_READY */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.TypeReadyDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getTypeReady()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.TypeReadyComment);

		/* TYPE_DEBUG */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.TypeDebugDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getTypeDebug()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.TypeDebugComment);

		/* STATUS_ACK */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.StatusAckDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getStatusAck()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.StatusAckComment);

		/* STATUS_NAK */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.StatusNakDefineName);
		param.add(Integer.toString(xmlParser.getFrameConfigParser().getStatusNak()));
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.StatusNakComment);

		writerH.generateConstants(type, name, param, comment);
	}

	/**
	 * Génération de la configuration USART sous forme de constantes C++
	 * 
	 * @throws CppWriterFileException
	 * @throws FrameConfigAttributeNotFoundException
	 */
	private void writeUsartDataConsts() throws CppWriterFileException {
		List<String> type = new ArrayList<>();
		List<String> name = new ArrayList<>();
		List<String> param = new ArrayList<>();
		List<String> comment = new ArrayList<>();

		UsartConfigParser parser = xmlParser.getUsartConfigParser();

		if (parser != null) {
			try {
				/* Baudrate */
				type.add(XmlConfigDefinition.UsartConfig.CppFile.baudrateType);
				name.add(XmlConfigDefinition.UsartConfig.CppFile.baudrateConstName);
				param.add("BAUD_" + Integer.toString(parser.getUsartConfiguration().getBaudrate()));
				comment.add(XmlConfigDefinition.UsartConfig.CppFile.baudrateCommentName);

				/* Data bits */
				type.add(XmlConfigDefinition.UsartConfig.CppFile.dataBitsType);
				name.add(XmlConfigDefinition.UsartConfig.CppFile.dataBitsConstName);
				param.add("DATA_" + Integer.toString(parser.getUsartConfiguration().getDatabits()));
				comment.add(XmlConfigDefinition.UsartConfig.CppFile.dataBitsCommentName);

				/* Stop bits */
				type.add(XmlConfigDefinition.UsartConfig.CppFile.stopBitsType);
				name.add(XmlConfigDefinition.UsartConfig.CppFile.stopBitsConstName);
				param.add("STOP_" + Integer.toString(parser.getUsartConfiguration().getStopbits()));
				comment.add(XmlConfigDefinition.UsartConfig.CppFile.stopBitsCommentName);

				/* Parity */
				type.add(XmlConfigDefinition.UsartConfig.CppFile.parityType);
				name.add(XmlConfigDefinition.UsartConfig.CppFile.parityConstName);
				param.add("PARITY_" + parser.getUsartConfiguration().getParityString());
				comment.add(XmlConfigDefinition.UsartConfig.CppFile.parityCommentName);

				writerH.generateConstants(type, name, param, comment);

			} catch (CommonAttributeNotFoundException e) {
				/* Pas d'erreur à lever, on ne génère pas le code */
				System.out.println("Configuration USART incomplete, pas de generation");
				Logger.write(getClass(), LogLevel.WARNING, "Configuration USART incomplete, pas de generation");
			}
		} else {
			System.out.println("Configuration USART non trouvee, pas de generation");
			Logger.write(getClass(), LogLevel.WARNING, "Configuration USART non trouvee, pas de generation");
		}


	}

	/**
	 * Génération de la version XML sous forme de constantes C++
	 * 
	 * @throws CppWriterFileException
	 * @throws AttributeNotFoundException
	 * @throws PrimaryAttributeNotFoundException
	 */
	private void writeXmlVersionConsts() throws CppWriterFileException, PrimaryAttributeNotFoundException {
		List<String> type = new ArrayList<>();
		List<String> name = new ArrayList<>();
		List<String> param = new ArrayList<>();
		List<String> comment = new ArrayList<>();

		/* MAJOR */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.XmlVersionMajorDefineName);
		param.add(xmlParser.getXmlMajorVersion());
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.XmlVersionMajorComment);

		/* MINOR */
		type.add(CppWriter.TYPE_UINT8);
		name.add(XmlConfigDefinition.FrameConfig.CppFile.XmlVersionMinorDefineName);
		param.add(xmlParser.getXmlMinorVersion());
		comment.add(XmlConfigDefinition.FrameConfig.CppFile.XmlVersionMinorComment);

		writerH.generateConstants(type, name, param, comment);
	}
}
