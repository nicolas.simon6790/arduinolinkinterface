package arduinoLinkInterface.Communication.DebugData;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import arduinoTools.ArduinoConfigLoader;
import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;

/**
 * Classe générant l'affichage des infos de debug
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 20:48:46     2021 : integration-sonar 
 * 
 *
 */
public class DebugDataDisplay {

	/**
	 * Référence vers le formattage de l'heure
	 */
	private SimpleDateFormat timeFormat;

	/**
	 * Nombre de messages à afficher
	 */
	int msgCount;
	
	/**
	 * Nombre maximal de message affichables
	 */
	 int tableSize;

	/**
	 * Table contenant les messages à afficher
	 */
	String[] msgTable;

	/**
	 * Constructeur de la classe
	 */
	public DebugDataDisplay() {

		this.timeFormat = new SimpleDateFormat("HH:mm:ss,SSS");

		msgCount = 0;
		try {
			tableSize = Integer
					.parseInt(ArduinoConfigLoader.getConfLoader().getConfigValue("NumberOfDisplayedDebugData"));
			msgTable = new String[tableSize];
		} catch (ArduinoConfigLoaderException e) {
			Logger.write(getClass(), LogLevel.ERROR, "Nombre d'elements a afficher non trouve");
		}

	}

	/**
	 * Ajoute un message de debug à la table
	 * 
	 * @param msg Message de debug à ajouter
	 */
	public void addDebugData(String msg) {
		
		/* Décalage de tous les éléments vers la gauche si la table est pleine */
		if(msgCount == tableSize) {
			for (int i = 0; i < msgCount - 1; i++)
				msgTable[i] = msgTable[i + 1];
		}
		
		/* Ajout du nouveau message */
		msgCount++;
		if (msgCount > tableSize)
			msgCount = tableSize;

		msgTable[msgCount - 1] = timeFormat.format(new Date()) + " - " + msg;
			
	}
	
	/**
	 * Renvoie la liste des messages enregistrés
	 * 
	 * @return Tableau contenant les messages
	 */
	public String[] getDebugData() {
		return Arrays.copyOf(msgTable, msgCount);
	}

}
