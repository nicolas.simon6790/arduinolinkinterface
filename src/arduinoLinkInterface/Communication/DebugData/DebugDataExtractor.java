/**
 * 
 */
package arduinoLinkInterface.Communication.DebugData;

import arduinoLinkInterface.FrameManager.CommFrame;

/**
 * Classe utilisée pour extraire les infos de debug de la trame obtenue
 * 
 * @author Nicolas SIMON
 *
 */
public class DebugDataExtractor {

	/**
	 * Sauvegarde du message extrait
	 */
	String debugMsg;

	/**
	 * Constructeur de la classe
	 * 
	 * @param frame Trame contenant les infos de debug
	 */
	public DebugDataExtractor(CommFrame frame) {

		char[] text = new char[frame.getParamTableSize()];
		for (int i = 0; i < text.length; i++)
			text[i] = (char) frame.getParam(i);

		debugMsg = new String(text);
	}

	/**
	 * @return the debugMsg
	 */
	public String getDebugMsg() {
		return debugMsg;
	}


}
