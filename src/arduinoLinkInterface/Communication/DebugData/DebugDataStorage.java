/**
 * 
 */
package arduinoLinkInterface.Communication.DebugData;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import arduinoTools.ArduinoConfigLoader;
import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;

/**
 * Classe utilisée pour enregistrer les infos de debug dans un fichier
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 19:45:26     2021 : integration-sonar 
 * 
 *
 */
public class DebugDataStorage {

	private FileWriter debugFile;

	/**
	 * Référence vers le formattage de l'heure
	 */
	private SimpleDateFormat timeFormat;

	/**
	 * Référence vers le formattage de la date
	 */
	private DateFormat fullDateFormat;

	/**
	 * Constructeur de la classe
	 */
	public DebugDataStorage() {

		/* Création du fichier */
		try {
			debugFile = new FileWriter(new File(getDebugFilename()), false);

			Logger.write(getClass(), LogLevel.INFO,
					"Enregistrement des infos de debug dans le fichier " + getDebugFilename());

			this.timeFormat = new SimpleDateFormat("HH:mm:ss,SSS");
			this.fullDateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
			debugFile.append("Debut de log : " + fullDateFormat.format(new Date()) + "\n\n");
		} catch (IOException e) {
			Logger.write(getClass(), LogLevel.ERROR, "\nErreur lors de l'ouverture du fichier " + getDebugFilename());
		}

	}

	/**
	 * Sauvegarde le message de debug reçu
	 * 
	 * @param msg Message de debug à écrire
	 */
	public void writeDebugData(String msg) {
		try {
			debugFile.append(timeFormat.format(new Date()) + " - " + msg + "\n");
		} catch (IOException e) {
			Logger.write(getClass(), LogLevel.ERROR, "Erreur lors de l'ecriture dans le fichier " + getDebugFilename());
		}
	}

	/**
	 * Renvoie le chemin du fichier de sauvegarde des infos de debug
	 * 
	 * @return Nom du fichier
	 */
	private String getDebugFilename() {

		String filePath;
		try {
			filePath = arduinoTools.SystemTools.getCurJARPath() + "\\"
					+ ArduinoConfigLoader.getConfLoader().getConfigValue("DebugDataStorageFile");
		} catch (ArduinoConfigLoaderException e) {
			Logger.write(getClass(), LogLevel.ERROR,
					"Nom du fichier de sauvegarde des infos de debug non trouve, utilisation de la valeur pas defaut");
			return "arduinoDebug.txt";
		}
		return filePath;
	}

	/**
	 * Ferme le fichier de log
	 */
	public void close() {
		try {
			debugFile.append("\nFin de log : " + fullDateFormat.format(new Date()));
			debugFile.close();
		} catch (IOException e) {
			Logger.write(getClass(), LogLevel.ERROR, "\nErreur lors de la fermeture du fichier " + debugFile);
		}
	}
}
