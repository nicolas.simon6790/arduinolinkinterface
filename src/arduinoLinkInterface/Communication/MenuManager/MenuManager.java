/**
 * 
 */
package arduinoLinkInterface.Communication.MenuManager;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import arduinoLinkInterface.FrameManager.CommFrame;
import arduinoLinkInterface.PortManager.SerialPortManager;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.DataParser.DataParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataIDNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataParserException;
import arduinoXmlConfig.Parser.DataParser.InteractiveActionParser;
import arduinoXmlConfig.Parser.DataParser.InteractiveActionParser.AnswerType;
import arduinoXmlConfig.Parser.DataParser.InteractiveActionParser.InteractiveActionAnswerConfig;
import arduinoXmlConfig.Parser.DataParser.InteractiveActionParser.InteractiveActionAnswerConfigNotFoundException;

/**
 * Classe principale utilisée pour gérer les menus du mode intéractif
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 17:31:16     2022 : 84-envoi-requete-avec-parametre-configurable 
 * Tue May 10 17:15:20     2022 : 83-bug-generation-de-l-arbre-de-menu 
 * Wed Jan 26 20:38:56     2022 : 68-corrections-sonar 
 * Tue Dec 21 19:45:28     2021 : integration-sonar 
 * Wed Dec  1 17:09:26     2021 : 60-ajout-changement-valeur-wdg-dans-bite 
 * Thu Oct 28 12:16:43     2021 : 41-corrections-affichage-2 
 * 
 *
 */
public class MenuManager {

	/**
	 * Arborescence des menus
	 */
	private MenuTree tree;

	/**
	 * Contenu de la ligne d'information temporaire
	 */
	private String tempInfo;

	/**
	 * Compteur du nombre de cycles d'affiche de l'info temporaire
	 */
	private int tempInfoCycles;

	/**
	 * Parser XML vers les actions du BITE interactif
	 */
	private InteractiveActionParser actionParser;

	/**
	 * Driver du port serie à utiliser
	 */
	private SerialPortManager serialPort;

	/**
	 * Caractère STX à utiliser pour l'émission des trames
	 */
	private byte STX;

	/**
	 * Caractère ETX à utiliser pour l'émission des trames
	 */
	private byte ETX;

	/**
	 * Caractère indiquant le type REQ de la trame
	 */
	private byte type_REQ;

	/**
	 * Caractère indiquant le type ANS de la trame
	 */
	private byte type_ANS;

	/**
	 * Caractère indiquant le statut ACK de la trame
	 */
	private byte status_ACK;

	/**
	 * Caractère indiquant le statut NAK de la trame
	 */
	private byte status_NAK;

	/**
	 * Booléen indiquant si une réponse est en attente
	 */
	private boolean isAnswerAwaited;

	/**
	 * Booléen indiquant si un paramètre utilisateur est en attente
	 */
	private boolean isUserParamAwaited;

	/**
	 * Mémorisation du paramètre utilisateur
	 */
	private int userParamMem;

	/**
	 * Référence vers la classe d'attente et de gestion de la trame de réponse
	 */
	private AnswerFrameWaiter frameWaiter;

	/**
	 * Référence vers l'object exécutant périodiquement le frameWaiter
	 */
	private ScheduledExecutorService executor;

	/**
	 * Référence vers le séquenceur de taches liées à l'exécutor
	 */
	@SuppressWarnings("unused")
	private ScheduledFuture<?> task;


	/**
	 * Constructeur de la classe. Génère l'arborescence des menus à partir du
	 * fichier XML
	 * 
	 */
	public MenuManager(XmlConfigParser xmlParser, SerialPortManager port) {

		this.tree = new MenuTree();
		this.serialPort = port;
		this.isAnswerAwaited = false;
		this.isUserParamAwaited = false;
		this.userParamMem = 0;
		
		setTempInfo("");

		this.frameWaiter = new AnswerFrameWaiter();
		this.executor = null;
		this.task = null;

		this.actionParser = xmlParser.getInteractiveDataActionsParser();

		try {
			this.STX = (byte) xmlParser.getFrameConfigParser().getSTX();
			this.ETX = (byte) xmlParser.getFrameConfigParser().getETX();
			this.type_REQ = (byte) xmlParser.getFrameConfigParser().getTypeReq();
			this.type_ANS = (byte) xmlParser.getFrameConfigParser().getTypeAns();
			this.status_ACK = (byte) xmlParser.getFrameConfigParser().getStatusAck();
			this.status_NAK = (byte) xmlParser.getFrameConfigParser().getStatusNak();
		} catch (CommonAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}

		/* Création de l'arborescence */
		try {
			buildTree(xmlParser);
		} catch (DataParserException | DataAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}
		
	}

	/**
	 * @return the tempInfo
	 */
	public String getTempInfo() {
		tempInfoCycles++;
		return tempInfo;
	}

	/**
	 * Réinitialise la ligne temporaire, seulement si la ligne est affichée depuis
	 * plus de 10 cycles de rafraichissement
	 */
	public void resetTempInfo() {
		if (tempInfoCycles > 10)
			setTempInfo("");
	}

	/**
	 * @return the tree
	 */
	public MenuTree getTree() {
		return tree;
	}

	/**
	 * Vérifie si un sous-menu existe et revient en arrière s'il n'y en a pas
	 */
	private void checkSubMenu() {
		/* Si il n'y a pas de sous-menu, on revient en arrière */
		if (tree.getCurrentItem().getSubMenuCount() == 0)
			tree.goToPreviousItem();
	}

	/**
	 * En fonction de la sélection, exécute l'action associée et/ou se déplace dans
	 * le sous-menu correspondant
	 * 
	 * @param index Index de l'action choisie
	 */
	public void processSelection(int index) {

		/* User parameter received */
		if (isUserParamAwaited) {
			userParamMem = index + 1;
			executeAction();
			isUserParamAwaited = false;
			checkSubMenu();

		} else if (!isAnswerAwaited) {
			/* On se déplace vers le sous-menu */
			if (tree.goToNextItem(index)) {

				/* Suppression du message temporaire */
				setTempInfo("");

				/* Si paramètre utilisateur */
				if (tree.getCurrentItem().isUserParamActive()) {
					isUserParamAwaited = true;
					setTempInfo(tree.getCurrentItem().getUserParamText());
					Logger.write(getClass(), LogLevel.INFO,
							"Attente parametre utilisateur pour action " + tree.getCurrentItem().getActionID());
				} else {
					executeAction();
					checkSubMenu();
				}
			} else
				setTempInfo("Selection non valide");
		} else
			setTempInfo("Execution en cours... Patientez");

	}

	/**
	 * Retourne au menu précédent
	 * 
	 * @return FALSE si on est au menu principal, TRUE dans les autres cas
	 */
	public boolean goToPreviousMenu() {
		if (!isAnswerAwaited) {
			setTempInfo("");

			if (tree.goToPreviousItem()) {
				executeAction();
				return true;
			} else
				return false;
		}
		else {
			setTempInfo("Execution en cours... Patientez");
			return false;
		}
	}

	/**
	 * Creation de l'arborescence des menus à partir de la conf XML
	 * 
	 * @param xmlParser
	 * @throws DataParserException
	 * @throws DataAttributeNotFoundException
	 */
	private void buildTree(XmlConfigParser xmlParser) throws DataParserException, DataAttributeNotFoundException {

		int stackDepth = 0;
		DataParser[] parserStack = new DataParser[XmlConfigDefinition.InteractiveData.MenuConfig.MaxStackDepth];
		parserStack[0] = xmlParser.getInteractiveMenuParser();
		int[] treeStack = new int[XmlConfigDefinition.InteractiveData.MenuConfig.MaxStackDepth];
		boolean inProgress = true;
		parserStack[stackDepth].resetCurrentDataIndex();

		while (inProgress) {

			/* On vérifie qu'il y a des items à ce niveau */
			if (parserStack[stackDepth].getDataCount() > 0) {

				/* Création d'un item dans l'arbre pour l'élément actuel */
				String name = parserStack[stackDepth]
						.getDataAttribute(XmlConfigDefinition.InteractiveData.MenuConfig.ItemNameAttribute);

				String actionID = getActionId(parserStack[stackDepth]);

				int actionParam = getRequestParam(parserStack[stackDepth]);
				boolean paramActive = true;
				boolean userParamActive = isUserParamActive(parserStack[stackDepth]);
				String userParamText = getUserParamtext(parserStack[stackDepth]);

				if (actionParam == -1)
					paramActive = false;
				else
					userParamActive = false;

				tree.addNewItem(
						new MenuItem(name, Byte.parseByte(actionID), (byte) actionParam, paramActive, userParamActive,
								userParamText));

				/* On se déplace sur l'item créé */
				parserStack[stackDepth + 1] = parserStack[stackDepth].getSubElementDataParser();
				tree.goToNextItem(treeStack[stackDepth]);
				stackDepth++;
				resetCurrentParser(parserStack[stackDepth]);
			}
			/* Sinon on remonte dans la pile et on se déplace à l'item suivant */
			else {
				boolean goUp = true;

				while (goUp) {
					stackDepth--;
					tree.goToPreviousItem();
					
					/* Effacement des index des étages en-dessous */
					for (int i = stackDepth + 1; i < treeStack.length; i++)
						treeStack[i] = 0;

					/* On essaye de passer à l'item suivant */
					if (parserStack[stackDepth].goToNextData()) {
						treeStack[stackDepth]++;
						goUp = false;
					}
					/* Si on est déjà en haut de la pile, on a terminé l'analyse */
					else if (stackDepth == 0) {
						inProgress = false;
						goUp = false;
					}
				}
			}
		}
	}


	/**
	 * Tente de réinitialiser le parser courant
	 * 
	 * @param parser
	 */
	private void resetCurrentParser(DataParser parser) {
		try {
			parser.resetCurrentDataIndex();
		} catch (DataParserException e) {
			/* Pas de traitement de l'exception */
		}
	}

	/**
	 * Renvoie l'ID de l'action associée à l'item en cours
	 * 
	 * @param parser
	 * @return ID
	 */
	private String getActionId(DataParser parser) {

		String actionID;

		try {
			actionID = parser.getDataAttribute(XmlConfigDefinition.InteractiveData.MenuConfig.ActionIDAttribute);
		} catch (DataAttributeNotFoundException e) {
			actionID = "0";
		}

		return actionID;
	}

	/**
	 * Execute l'action liée à l'item courant
	 */
	private void executeAction() {

		/* Si une action est configurée, on l'exécute */
		try {
			CommFrame frame = new CommFrame(STX, ETX);
			byte actionID = tree.getCurrentItem().getActionID();
			actionParser.findDataByID(actionID);
			frame.setCmd(actionID);
			frame.setType(type_REQ);

			if (tree.getCurrentItem().isActionParamActive()) {
				frame.createParamTable(1);
				frame.setParam(0, tree.getCurrentItem().getActionParam());
			}
			else if (tree.getCurrentItem().isUserParamActive())
				frame.setParamInt(userParamMem);

			frame.computeAndSetChecksum();

			Logger.write(getClass(), LogLevel.INFO, "Envoi de la commande " + actionID);
			Logger.write(getClass(), LogLevel.VERB, "Envoi de la trame : " + frame);

			/* Si la commande demande une réponse */
			if (isCommandAnswerNeeded()) {
				this.executor = Executors.newScheduledThreadPool(1);
				this.task = executor.scheduleAtFixedRate(frameWaiter, 5, 5, SECONDS);
				isAnswerAwaited = true;
				setTempInfo("Execution en cours...");
			}

			/* Emission et sauvegarde de la trame */
			frameWaiter.saveRequestFrame(frame, Integer.parseInt(actionParser
					.getDataAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.RetSizeAttributeName)));
			serialPort.writeBytes(frame.getByteTab());

		} catch (DataIDNotFoundException | NumberFormatException | DataAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}
	}

	/**
	 * Vérifie que la trame reçue est bien une trame de réponse valide et la stocke
	 * 
	 * @param frame Trame reçue
	 * @throws AnswerFrameNotValidException     Si la trame reçue n'est pas une
	 *                                          réponse ou que le statut est inconnu
	 * @throws CommonAttributeNotFoundException Si l'attribut XML n'est pas trouvé
	 */
	public void processAnswerFrame(CommFrame frame)
			throws AnswerFrameNotValidException {

		if (frame.getType() != type_ANS)
			throw new AnswerFrameNotValidException("la trame reçue n'est pas une trame de reponse");

		if ((frame.getStatus() != status_ACK) && frame.getStatus() != status_NAK)
			throw new AnswerFrameNotValidException("la trame de reponse reçue n'est pas valide");

		Logger.write(getClass(), LogLevel.INFO, "Réponse recue : " + frame);

		if (isAnswerAwaited)
			frameWaiter.processReceivedFrame(frame);
	}

	/**
	 * 
	 * @return TRUE si la commande demande une réponse, FALSE sinon
	 */
	private boolean isCommandAnswerNeeded() {
		boolean answer = true;

		try {
			if (actionParser.getDataAttribute(
					XmlConfigDefinition.InteractiveData.ActionsConfig.NeedAnswerAttributeName).equals("false"))
				answer = false;
		} catch (DataAttributeNotFoundException e) {
			return true;
		}

		return answer;
	}

	/**
	 * Mets à jour le message d'info temporaire et réinitialise le compteur
	 * 
	 * @param str
	 */
	private void setTempInfo(String str) {
		tempInfo = str;
		tempInfoCycles = 0;
	}

	/**
	 * Recherche le paramètre optionnel à envoyer avec la requête
	 * 
	 * @return paramètre à envoyer avec la requête (-1 si l'attribut n'est pas
	 *         configuré)
	 */
	private int getRequestParam(DataParser parser) {

		int param;
		try {
			param = Integer.valueOf(
					parser.getDataAttribute(XmlConfigDefinition.InteractiveData.MenuConfig.ActionParamAttribute));
		} catch (DataAttributeNotFoundException e) {
			return -1;
		}

		return param;
	}

	/**
	 * Recherche le paramètre optionnel d'activation du paramètre utilisateur
	 * 
	 * @param parser
	 * @return True/False
	 */
	private boolean isUserParamActive(DataParser parser) {
		boolean userParam;
		try {
			userParam = Boolean.valueOf(parser.getDataAttribute(XmlConfigDefinition.InteractiveData.MenuConfig.UserParamAttribute));
		}
		catch (DataAttributeNotFoundException e) {
			return false;
		}

		return userParam;
	}

	/**
	 * Recherche le paramètre optionnel contenant le text de paramètre utilisateur.
	 * Dans le cas ou le paramètre utilisateur n'est pas actif, retourne un texte
	 * générique.
	 * 
	 * @param parser
	 * @return Texte à afficher
	 */
	private String getUserParamtext(DataParser parser) {

		String userParamText = "Entrez paramètre utilisateur : ";

		if (isUserParamActive(parser)) {
			try {
				userParamText = parser
						.getDataAttribute(XmlConfigDefinition.InteractiveData.MenuConfig.UserParamTextAttribute);
			} catch (DataAttributeNotFoundException e) {
			}
		}

		return userParamText;
	}



	/**
	 * 
	 * Classe gérant l'exception levée quand la trame de réponse analysée n'est pas
	 * valide
	 *
	 */
	public class AnswerFrameNotValidException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Constructeur de l'exception
		 * 
		 * @param s Message d'erreur
		 */
		public AnswerFrameNotValidException(String s) {
			super("Trame de reponse non valide : " + s);
		}
	}

	/**
	 * Classe servant à attendre l'arrivée de la trame de réponse. La trame de
	 * requête est renvoyée toutes les 5 secondes en cas de non réponse (max 5
	 * fois). Quand la réponse arrive, on réautorise la navigation dans le menu.
	 * 
	 */
	private class AnswerFrameWaiter implements Runnable {

		/**
		 * Compte le nombre de ré-émissions de la requête
		 */
		private int counter;

		/**
		 * Sauvegarde de la trame envoyée
		 */
		private CommFrame requestFrame;

		/**
		 * Taille du champ PARAM de la reponse
		 */
		private int answerSize;

		/**
		 * Mémorisation de la configuration de la trame de réponse
		 */
		private InteractiveActionAnswerConfig answerConfig;

		public AnswerFrameWaiter() {
			this.counter = 0;
			this.answerSize = 0;
			this.requestFrame = null;
			this.answerConfig = null;
		}

		/**
		 * Réceptionne la trame de réponse et analyse son contenu. Déclare la trame
		 * reçue et stoppe l'exéution périodique de la méthode "run".
		 * 
		 * @param frame Trame de réponse reçue
		 */
		public void processReceivedFrame(CommFrame frame) {

				executor.shutdownNow();

				/* Si la trame est valide */
				if (frame.getStatus() == status_ACK) {
					/* Si une config de réponse existe */
					if (isAnswerProcessingConfigured()) {
						/* En fonction du type de réponse */
						if (answerConfig.getType() == AnswerType.BOOL) {
							if (frame.getParam(0) == ((byte) 0xFF))
								setTempInfo(answerConfig.getPattern().replace("%v", answerConfig.getAnswerTrue()));
							else
								setTempInfo(answerConfig.getPattern().replace("%v", answerConfig.getAnswerFalse()));
						}

						else if (answerConfig.getType() == AnswerType.INTEGER) {
							setTempInfo(answerConfig.getPattern().replace("%v",
									Integer.toString(frame.getParamIntFormat(answerSize))));
						}
					} else
						setTempInfo("Action executee avec succes");
				}
				else
					setTempInfo("Action non executee");

				isAnswerAwaited = false;
				counter = 0;
		}


		/**
		 * Vérifie périodiquement si la trame de réponse a été reçue. Renvoie la requête
		 * si la réponse n'a pas été reçue.
		 */
		public void run() {

			if (isAnswerAwaited) {
				if (counter < 5) {
					serialPort.writeBytes(requestFrame.getByteTab());
					counter++;
					setTempInfo("Aucune reponse a la demande... Renvoi de la trame " + counter);
				} else {
					executor.shutdownNow();
					counter = 0;
					isAnswerAwaited = false;
					setTempInfo("Aucune reponse a la demande... Abandon");
				}
			} else {
				executor.shutdownNow();
				counter = 0;
			}
		}

		/**
		 * Sauvegarde la trame de requête en cours d'envoie
		 * 
		 * @param frame
		 */
		public void saveRequestFrame(CommFrame frame, int size) {
			requestFrame = frame;
			answerSize = size;
		}

		private boolean isAnswerProcessingConfigured() {

			try {
				answerConfig = actionParser.getAnswerConfig();
			} catch (InteractiveActionAnswerConfigNotFoundException e) {
				return false;
			}

			return true;
		}
	}


}
