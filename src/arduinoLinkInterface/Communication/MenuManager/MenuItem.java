/**
 * 
 */
package arduinoLinkInterface.Communication.MenuManager;

import java.util.ArrayList;

/**
 * Représente un élément de menu
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 17:31:16     2022 : 84-envoi-requete-avec-parametre-configurable 
 * Tue Dec 21 19:45:27     2021 : integration-sonar 
 * Wed Dec  1 16:56:12     2021 : 60-ajout-changement-valeur-wdg-dans-bite 
 * 
 *
 */
public class MenuItem {

	/**
	 * Nom affiché de l'élément
	 */
	private String name;

	/**
	 * ID de l'action à effectuer
	 */
	private byte actionID;

	private byte actionParam;
	private boolean actionParamActive;

	/**
	 * User-defined parameter activation flag
	 */
	private boolean userParamActive;

	/**
	 * User parameter text
	 */
	private String userParamText;

	/**
	 * Liste contenant les sous-menus
	 */
	private ArrayList<MenuItem> submenuList;

	/**
	 * Référence vers l'item parent
	 */
	private MenuItem prevItem;

	/**
	 * Constructeur de la classe
	 */
	public MenuItem() {
		this.name = "";
		this.actionID = 0;
		this.actionParam = 0;
		this.actionParamActive = false;
		this.submenuList = new ArrayList<>();
		this.prevItem = null;
		this.userParamActive=false;
		this.userParamText = null;
	}

	/**
	 * Constructeur de la classe
	 * 
	 * @param name
	 * @param actionID
	 * @param prevItem
	 */
	public MenuItem(String name, byte actionID) {
		super();
		this.name = name;
		this.actionID = actionID;
		this.submenuList = new ArrayList<>();
		this.prevItem = null;
		this.userParamActive = false;
		this.actionParam = 0;
		this.actionParamActive = false;
		this.userParamText = null;
	}

	/**
	 * Constructeur de la classe
	 * 
	 * @param name
	 * @param actionID
	 * @param actionParam
	 * @param actionParamActive
	 * @param prevItem
	 */
	public MenuItem(String name, byte actionID, byte actionParam, boolean actionParamActive) {
		super();
		this.name = name;
		this.actionID = actionID;
		this.actionParam = actionParam;
		this.actionParamActive = actionParamActive;
		this.submenuList = new ArrayList<>();
		this.prevItem = null;
		this.userParamActive = false;
		this.userParamText = null;
	}

	/**
	 * 
	 * Constructeur de la classe
	 * 
	 * @param name
	 * @param actionID
	 * @param actionParam
	 * @param actionParamActive
	 * @param userParamActive
	 * @param userParamText
	 * @param submenuList
	 * @param prevItem
	 */
	public MenuItem(String name, byte actionID, byte actionParam, boolean actionParamActive, boolean userParamActive,
			String userParamText) {
		super();
		this.name = name;
		this.actionID = actionID;
		this.actionParam = actionParam;
		this.actionParamActive = actionParamActive;
		this.userParamActive = userParamActive;
		this.userParamText = userParamText;
		this.submenuList = new ArrayList<>();
		this.prevItem = null;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the actionID
	 */
	public byte getActionID() {
		return actionID;
	}

	/**
	 * @return the actionParam
	 */
	public byte getActionParam() {
		return actionParam;
	}

	/**
	 * @return the actionParamActive
	 */
	public boolean isActionParamActive() {
		return actionParamActive;
	}

	/**
	 * 
	 * @param index
	 * @return Item du sous-menu sélectionné
	 */
	public MenuItem getSubmenuItem(int index) {
		return submenuList.get(index);
	}

	/**
	 * @return the prevItem
	 */
	public MenuItem getPrevItem() {
		return prevItem;
	}

	/**
	 * 
	 * @return Nombre de sous-menus disponibles
	 */
	public int getSubMenuCount() {
		return submenuList.size();
	}

	/**
	 * @param prevItem the prevItem to set
	 */
	public void setPrevItem(MenuItem prevItem) {
		this.prevItem = prevItem;
	}

	/**
	 * Ajoute un élément de sous-menu
	 * 
	 * @param newItem
	 */
	public void addSubMenuItem(MenuItem newItem) {
		submenuList.add(newItem);
	}

	/**
	 * @return the userParamActive
	 */
	public boolean isUserParamActive() {
		return userParamActive;
	}

	/**
	 * @return the userParamText
	 */
	public String getUserParamText() {
		return userParamText;
	}

}
