/**
 * 
 */
package arduinoLinkInterface.Communication.MenuManager;

/**
 * Arborescence contenant tous les menus. Construit à partir de la configuration
 * XML.
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 19:45:29     2021 : integration-sonar 
 * 
 *
 */
public class MenuTree {

	/**
	 * Début de l'arborescence
	 */
	private MenuItem startItem;

	/**
	 * Référence vers l'item actuellement sélectionné
	 */
	private MenuItem currentItem;

	/**
	 * Constructeur de la classe
	 */
	public MenuTree() {
		this.startItem = new MenuItem("Menu principal", (byte) 0);
		this.currentItem = startItem;
	}

	/**
	 * 
	 * @return Référence vers l'item sélectionné
	 */
	public MenuItem getCurrentItem() {
		return currentItem;
	}

	/**
	 * Sélectionne le parent de l'élément sélectionné
	 * 
	 * @return TRUE si on est bien revenu au menu précédent, FALSE si on est au menu
	 *         principal
	 */
	public boolean goToPreviousItem() {
		if (currentItem.getPrevItem() != null) {
			currentItem = currentItem.getPrevItem();
			return true;
		} else
			return false;
	}

	/**
	 * Sélectionne un des sous-menu de l'item courant
	 * 
	 * @param index Numéro du sous-menu à sélectionner
	 * @return TRUE si la sélection a réussi, FAUX sinon
	 */
	public boolean goToNextItem(int index) {
		if (index < currentItem.getSubMenuCount() && index >= 0) {
			currentItem = currentItem.getSubmenuItem(index);
			return true;
		} else
			return false;
	}

	/**
	 * Sélectionne l'item de départ
	 * 
	 * @return Référence vers l'item de départ
	 */
	public MenuItem goToStartItem() {
		currentItem = startItem;
		return currentItem;
	}

	/**
	 * Ajoute un nouvel item de sous-menu à l'item courant
	 * 
	 * @param item Nouvel item à ajouter
	 */
	public void addNewItem(MenuItem item) {
		item.setPrevItem(currentItem);
		currentItem.addSubMenuItem(item);
	}

	/**
	 * 
	 * @return TRUE si on est actuellement positionné sur l'élément de départ, FALSE
	 *         sinon
	 */
	public boolean isStartItem() {
		return (startItem == currentItem);
	}


}
