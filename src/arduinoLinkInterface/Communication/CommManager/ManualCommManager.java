/**
 * 
 */
package arduinoLinkInterface.Communication.CommManager;

import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.Parser.XmlConfigParser;

/**
 * Classe gérant la communication manuelle avec l'Arduino
 * 
 * @author Nicolas
 *
 */
public class ManualCommManager extends CommManager{

	/**
	 * Constructeur de la classe. 
	 * 
	 * @param parser Référence vers le parser XML à utiliser
	 */
	public ManualCommManager(XmlConfigParser parser) {
		super(parser);
		Logger.write(getClass(), LogLevel.INFO, "Démarrage communication manuelle");
	}

	
	/**
	 * Transfère la chaine de caractères reçue sur le laision série. Transforme
	 * d'abord la String en tableau de bytes.
	 * 
	 * @param str String à envoyer
	 * @throws ManualTransmissionNotValidException Quand la trame à envoyer n'est
	 *                                             pas valide
	 * @throws ManualTransmissionEmptyException    Quand la trame à envoyer est vide
	 */
	public void sendCommand(String str) throws ManualTransmissionNotValidException, ManualTransmissionEmptyException {

		if (str == null || str.isEmpty())
			throw new ManualTransmissionEmptyException();

		String[] tab = str.split(" ");

		if (tab.length >= 256)
			throw new ManualTransmissionNotValidException(
					"Longueur de trame supérieure à " + 256);

		byte[] buffer = new byte[tab.length];

		for (int i = 0; i < tab.length; i++) {
			int value;
			try {
				value = Integer.parseInt(tab[i]);
			} catch (NumberFormatException e) {
				throw new ManualTransmissionNotValidException("Caractère #" + i + 1 + " n'est pas un chiffre");
			}
			if (value < 256)
				buffer[i] = (byte) value;
			else
				throw new ManualTransmissionNotValidException("Caractère #" + i + 1 + " n'est pas un byte");
		}

		serialPort.writeBytes(buffer);
		System.out.println("Trame envoyee");
	}

	/**
	 * Exception levée quand la trame à envoyer est incorrecte.
	 * 
	 * @author Nicolas
	 *
	 */
	public class ManualTransmissionNotValidException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Constructeur de la classe
		 * 
		 * @param msg Message d'erreur
		 */
		public ManualTransmissionNotValidException(String msg) {
			super("Trame non valide : " + msg);
		}
	}

	/**
	 * Exception levée quand la trame à envoyer est vide.
	 * 
	 * @author Nicolas
	 *
	 */
	public class ManualTransmissionEmptyException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Constructeur de la classe
		 * 
		 * @param msg Message d'erreur
		 */
		public ManualTransmissionEmptyException() {
			super("Rien à envoyer !!!");
		}
	}

}
