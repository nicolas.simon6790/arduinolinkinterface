/**
 * 
 */
package arduinoLinkInterface.Communication.CommManager;

import java.time.Instant;

import arduinoLinkInterface.Communication.DebugData.DebugDataExtractor;
import arduinoLinkInterface.Communication.InteractiveRequest.BiteInteractiveRequest;
import arduinoLinkInterface.Communication.InteractiveRequest.BiteInteractiveRequest.ReceivedRequestFrameNotValidException;
import arduinoLinkInterface.Communication.MenuManager.MenuManager;
import arduinoLinkInterface.Communication.MenuManager.MenuManager.AnswerFrameNotValidException;
import arduinoLinkInterface.Communication.PeriodicData.PeriodicDataManager.PeriodicDataFrameNotValidException;
import arduinoLinkInterface.Communication.PeriodicData.PeriodicDataManager.TimeDataFrameNotValidException;
import arduinoLinkInterface.FrameManager.CommFrame;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.XmlConfigParser.PrimaryAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataIDNotFoundException;

/**
 * Classe gérant la communication avec l'Arduino
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Sat Apr 30 19:18:34     2022 : 77-detection-automatique-de-la-deconnexion-du-bite 
 * Tue Apr 12 18:28:10     2022 : 76-rendre-generique-le-code-des-donnees-periodiques 
 * Tue Jan 18 11:45:12     2022 : 58-reception-de-plusieurs-trames-en-meme-temps 
 * Mon Dec 20 16:01:08     2021 : 62-ajout-d-un-timer-sur-bite-ready 
 * Sun Dec 19 20:03:34     2021 : 61-compatibilite-de-version-et-identification-arduino 
 * Thu Oct 28 12:16:42     2021 : 41-corrections-affichage-2 
 * 
 *
 */
public class AutoCommManager extends CommManager {
	
	/**
	 * Référence vers l'objet gérant les menus du mode interactif
	 */
	private MenuManager menuManager;

	/**
	 * Date de la derniere reception de la trame BITE_READY avec statut OK
	 */
	private Instant readyFrameTS;

	/**
	 * Version majeure du BITE
	 */
	private String versionMajor;

	/**
	 * version mineure du BITE
	 */
	private String versionMinor;

	/**
	 * Constructeur de la classe.
	 * 
	 * @param parser Référence vers le parser XML à utiliser
	 */
	public AutoCommManager(XmlConfigParser parser) {
		super(parser);
		menuManager = new MenuManager(parser, serialPort);

		readyFrameTS = Instant.now().minusSeconds(10);

		try {
			versionMajor = xmlParser.getXmlMajorVersion();
			versionMinor = xmlParser.getXmlMinorVersion();
		} catch (PrimaryAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}

	}
	
	
	/**
	 * Retourne la ligne d'information temporaire et la réinitialise
	 * 
	 * @return Ligne d'information temporaire
	 */
	public String getTempInfo() {
		String str = menuManager.getTempInfo();
		menuManager.resetTempInfo();
		return str;
	}


	/**
	 * Transfère les octets reçus vers l'assembleur de trames et les transfère à la
	 * méthode de décodage. Surcharge la méthode abstraite de la superclasse.
	 * 
	 * @param rcvBytes Tableau de bytes contenant les données reçues sur le bus
	 *                 série
	 */ 
	@Override
	public void transferReceivedData(byte[] rcvBytes) {
		try {
			assembler.addBytesToList(rcvBytes);
			while (!assembler.getFrameFifo().isEmpty()) {

				/* On transfère la trame reçue en fonction de son type */
				CommFrame frame = assembler.getFrameFifo().remove();
				byte type = frame.getType();

				if (type == xmlParser.getFrameConfigParser().getTypeData())
					periodicDataManager.analyseFrame(frame);
				else if (type == xmlParser.getFrameConfigParser().getTypeAns())
					menuManager.processAnswerFrame(frame);
				else if ((type == xmlParser.getFrameConfigParser().getTypeReq()) && isBiteActive()) {
					BiteInteractiveRequest request = new BiteInteractiveRequest(xmlParser, frame, serialPort);
					request.processRequest();
				}
				else if (type == xmlParser.getFrameConfigParser().getTypeDebug()) {
					String msg = (new DebugDataExtractor(frame)).getDebugMsg();
					if (storeDebugData)
						debugDataStorage.writeDebugData(msg);

					if (displayDebugData)
						debugDataDisplay.addDebugData(msg);
				}
				else if (type == xmlParser.getFrameConfigParser().getTypeReady()) {
					Logger.write(getClass(), LogLevel.INFO, "Trame BITE_READY recue : statut = " + frame.getStatus());
					if (frame.getStatus() == 1)
						readyFrameTS = Instant.now();

					sendBiteStartFrame();
				}

			}
		} catch (PeriodicDataFrameNotValidException
				| CommonAttributeNotFoundException | AnswerFrameNotValidException | TimeDataFrameNotValidException
				| ReceivedRequestFrameNotValidException | DataIDNotFoundException | DataAttributeNotFoundException
				| NumberFormatException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}
	}

	/**
	 * Détermine si le BITE est actif ou non, il est actif si la trame BITE_READY a
	 * été reçue depuis moins de 8s Si le BITE est inactif, envoie la trame de
	 * démarrage du BITE (type BITE_READY et CMD=1)
	 * 
	 * @return TRUE si le BITE est actif, FALSE sinon
	 */
	public boolean isBiteActive() {
		return Instant.now().minusSeconds(8).isBefore(readyFrameTS);
	}

	/**
	 * Envoi de la trame de démarrage du BITE
	 */
	private void sendBiteStartFrame() {
		try {
			Logger.write(getClass(), LogLevel.INFO,
					"Envoi de la trame de démarrage du BITE : version " + versionMajor + "." + versionMinor);

			CommFrame frame = new CommFrame((byte) xmlParser.getFrameConfigParser().getSTX(),
					(byte) xmlParser.getFrameConfigParser().getETX());
			frame.setType((byte) xmlParser.getFrameConfigParser().getTypeReady());
			frame.setCmd((byte) 1);
			frame.createParamTable(2);
			frame.setParam(0, Byte.parseByte(versionMajor));
			frame.setParam(1, Byte.parseByte(versionMinor));
			frame.computeAndSetChecksum();
			serialPort.writeBytes(frame.getByteTab());
		} catch (CommonAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}
	}

	/**
	 * Envoie la trame d'arrêt au BITE pour cesser toutes les transmissions ((type
	 * BITE_READY et CMD=0)
	 */
	public void stopBite() {
		if (isBiteActive()) {
			CommFrame frame;
			try {
				frame = new CommFrame((byte) xmlParser.getFrameConfigParser().getSTX(),
						(byte) xmlParser.getFrameConfigParser().getETX());
				frame.setType((byte) xmlParser.getFrameConfigParser().getTypeReady());
				frame.setCmd((byte) 0);
				frame.computeAndSetChecksum();
				serialPort.writeBytes(frame.getByteTab());
			} catch (CommonAttributeNotFoundException e) {
				Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
			}
		}
	}


	/**
	 * 
	 * @return Tableau avec les différents items du menu, l'item 0 est le titre.
	 */
	public String[] getCurrentMenuText() {

		String[] menu = new String[menuManager.getTree().getCurrentItem().getSubMenuCount() + 1];

		menu[0] = menuManager.getTree().getCurrentItem().getName();

		for (int i = 0; i < menuManager.getTree().getCurrentItem().getSubMenuCount(); i++)
			menu[i + 1] = menuManager.getTree().getCurrentItem().getSubmenuItem(i).getName();

		return menu;
	}


	/**
	 * En fonction de la sélection, exécute l'action associée et/ou se déplace dans
	 * le sous-menu correspondant
	 * 
	 * @param index Index de l'action choisie
	 */
	public void processSelection(int index) {
		menuManager.processSelection(index - 1);
	}


	/**
	 * Retourne au menu précédent
	 * @return FALSE si on est au menu principal, TRUE dans les autres cas
	 */
	public boolean goToPreviousMenu() {
		return menuManager.goToPreviousMenu();
	}





	/**
	 * 
	 * @return TRUE si on est actuellement positionné sur le menu principal, FALSE
	 *         sinon
	 */
	public boolean isStartItem() {
		return menuManager.getTree().isStartItem();
	}
}
