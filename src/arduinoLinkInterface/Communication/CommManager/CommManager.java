/**
 * 
 */
package arduinoLinkInterface.Communication.CommManager;

import arduinoLinkInterface.Communication.DebugData.DebugDataDisplay;
import arduinoLinkInterface.Communication.DebugData.DebugDataExtractor;
import arduinoLinkInterface.Communication.DebugData.DebugDataStorage;
import arduinoLinkInterface.Communication.PeriodicData.PeriodicDataManager;
import arduinoLinkInterface.Communication.PeriodicData.PeriodicDataManager.PeriodicDataFrameNotValidException;
import arduinoLinkInterface.Communication.PeriodicData.PeriodicDataManager.TimeDataFrameNotValidException;
import arduinoLinkInterface.Communication.PeriodicData.PeriodicDataStorage.PeriodicDataStorageElement;
import arduinoLinkInterface.Communication.PeriodicData.TimePeriodicData;
import arduinoLinkInterface.FrameManager.CommFrame;
import arduinoLinkInterface.FrameManager.FrameAssembler;
import arduinoLinkInterface.PortManager.SerialPortConfiguration;
import arduinoLinkInterface.PortManager.SerialPortManager;
import arduinoTools.ArduinoConfigLoader;
import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;

/**
 * Superclasse de communication. Definit les principales méthodes pour communiquer sur la liaison série
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Mon May  9 15:45:06     2022 : 81-ajout-parametre-de-configuration 
 * Fri May  6 19:21:23     2022 : 79-modifications-affichage-arduino 
 * Tue Jan 18 11:45:12     2022 : 58-reception-de-plusieurs-trames-en-meme-temps 
 * Tue Dec 21 19:45:24     2021 : integration-sonar 
 * Mon Nov 29 20:43:26     2021 : 59-enregistrement-de-la-valeur-des-capteurs 
 * 
 *
 */
public abstract class CommManager {
	/**
	 * Référence vers le parser xml
	 */
	protected XmlConfigParser xmlParser;

	/**
	 * Driver du port serie à utiliser
	 */
	protected SerialPortManager serialPort;
	
	/**
	 * Référence vers l'assembleur de trames 
	 */
	protected FrameAssembler assembler;
	
	/**
	 * Référence vers le gestionnaire de données périodiques
	 */
	protected PeriodicDataManager periodicDataManager;
	
	/**
	 * Flag d'affichage des infos de debug
	 */
	protected boolean displayDebugData;

	/**
	 * Flag de sauvegarde des infos de debug
	 */
	protected boolean storeDebugData;

	/** Référence vers le stockage des infos de debug */
	protected DebugDataStorage debugDataStorage;

	/**
	 * Référence vers l'affichage des infos de debug
	 */
	protected DebugDataDisplay debugDataDisplay;

	/**
	 * Constructeur de la classe. Instancie le driver du port série, l'assembleur de
	 * trame et le gestionnaire de données périodiques.
	 * 
	 * @param parser Référence vers le parser XML à utiliser
	 */
	protected CommManager(XmlConfigParser parser) {
		this.xmlParser = parser ;
		this.assembler = new FrameAssembler(parser.getFrameConfigParser());

		/* Récupération de la configuration pour le traitement des données de debug */
		displayDebugData = getDebugDataConfig("DisplayDebugData");
		storeDebugData = getDebugDataConfig("StoreDebugData");

		if (storeDebugData)
			debugDataStorage = new DebugDataStorage();

		if (displayDebugData)
			debugDataDisplay = new DebugDataDisplay();

		/*
		 * La configuration Usart est lue dans le fichier XML, si elle n'est pas valide
		 * on utilise celle par défaut
		 */
		SerialPortConfiguration portConfig = null;
		try {
			if (xmlParser.getUsartConfigParser() != null)
				portConfig = xmlParser.getUsartConfigParser().getUsartConfiguration();
			else
				portConfig = configureSerialDefault();
		} catch (CommonAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.WARNING, e.getMessage());
			portConfig = configureSerialDefault();
		}
		this.serialPort = new SerialPortManager(portConfig, this);
		this.periodicDataManager = new PeriodicDataManager(xmlParser);
	}
	
	/**
	 * Renvoie la configuration du traitement des infos de debug
	 * 
	 * @param cfgName Nom du parametre a chercher
	 * @return Valeur du parametre de configuration
	 */
	private boolean getDebugDataConfig(String cfgName) {

		boolean cfgValue = false;

		try {
			cfgValue = ArduinoConfigLoader.getConfLoader().getConfigValueBool(cfgName);
		} catch (ArduinoConfigLoaderException e) {
			Logger.write(getClass(), LogLevel.ERROR,
					"Configuration du traitement des infos de debug non trouvee, utilisation des valeurs pas defaut");
			return false;
		}

		return cfgValue;
	}
	
	/**
	 * @return the serialPort
	 */
	public SerialPortManager getSerialPort() {
		return serialPort;
	}


	/**
	 * Retourne toutes les données périodiques mémorisées
	 * 
	 * @return Tableau contenant les donnée périodiques mémorisées
	 */
	public PeriodicDataStorageElement[] getAllStoredPeriodicData() {
		return periodicDataManager.getDataStorage().getAllPeriodicData();
	}

	/**
	 * Retourne la date et l'heure de l'Arduino
	 * 
	 * @return Objet contenant la date et l'heure
	 */
	public TimePeriodicData getStoredArduinoTime() {
		return periodicDataManager.getDataStorage().getTimeData();
	}

	/**
	 * Termine la communication en libérant le port série
	 */
	public void closeComm() {
		serialPort.close();
		periodicDataManager.stopPeriodicData();

		/* Arrêt du log de debug */
		if (storeDebugData)
			debugDataStorage.close();
	}
	
	
	/**
	 * @return the periodicDataManager
	 */
	protected PeriodicDataManager getPeriodicDataManager() {
		return periodicDataManager;
	}

	/**
	 * Transfère les octets reçus vers l'assembleur de trames et les envoie à la
	 * méthode de décodage.
	 * 
	 * @param rcvBytes Tableau de bytes contenant les données reçues sur le bus
	 *                 série
	 */
	public void transferReceivedData(byte[] rcvBytes) {
		try {
			assembler.addBytesToList(rcvBytes);
			while (!assembler.getFrameFifo().isEmpty()) {
				
				CommFrame frame = assembler.getFrameFifo().remove();
				
				/* Si donnée périodique */
				if (frame.getType() == xmlParser.getFrameConfigParser().getTypeData())
					periodicDataManager.analyseFrame(frame);
				/* Si debug */
				else if (frame.getType() == xmlParser.getFrameConfigParser().getTypeDebug()) {
					String msg = (new DebugDataExtractor(frame)).getDebugMsg();
					if (storeDebugData)
						debugDataStorage.writeDebugData(msg);

					if (displayDebugData)
						debugDataDisplay.addDebugData(msg);
				}
				/*
				 * Si ce n'est pas une donnée périodique ni du debug, affichage de la trame
				 * reçue
				 */
				else {
					String str = "Trame recue : ";
					for (int i = 0; i < frame.getByteTab().length; i++)
						str = str.concat(String.valueOf(frame.getByteTab()[i])).concat(" ");
					System.out.println(str);
					Logger.write(getClass(), LogLevel.INFO, str);
				}
			}
		} catch (PeriodicDataFrameNotValidException
				| CommonAttributeNotFoundException | NumberFormatException | TimeDataFrameNotValidException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}
	}

	/**
	 * Fonction générant la configuration série par défaut
	 *
	 * @return Configuration par défaut
	 */
	private SerialPortConfiguration configureSerialDefault() {
		Logger.write(getClass(), LogLevel.INFO, "Utilisation de la configuration USART par défaut");

		return new SerialPortConfiguration(XmlConfigDefinition.UsartConfig.UsartDefaultConfig.port,
				XmlConfigDefinition.UsartConfig.UsartDefaultConfig.baudrate,
				XmlConfigDefinition.UsartConfig.UsartDefaultConfig.dataBits,
				XmlConfigDefinition.UsartConfig.UsartDefaultConfig.stopBits,
				XmlConfigDefinition.UsartConfig.UsartDefaultConfig.parity);
	}

	/**
	 * @return the displayDebugData
	 */
	public boolean isDisplayDebugData() {
		return displayDebugData;
	}

	/**
	 * @return the debugDataDisplay
	 */
	public DebugDataDisplay getDebugDataDisplay() {
		return debugDataDisplay;
	}
}
