/**
 * 
 */
package arduinoLinkInterface.Communication.InteractiveRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;

/**
 * Classe contenant toutes les méthodes statiques utilisées pour triater les
 * requetes de l'Arduino
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri May  6 19:21:51     2022 : 79-modifications-affichage-arduino 
 * Tue Dec 21 19:45:27     2021 : integration-sonar 
 * 
 *
 */
public class BiteInteractiveActions {

	/**
	 * Constructeur privé (classe abstraite)
	 */
	private BiteInteractiveActions() {

	}

	/**
	 * Renvoie la date et l'heure formattées dans un tableau de bytes
	 * 
	 * @param size Taille du tableau à renvoyer
	 * @return Tableau contenant la date et l'heure
	 */
	public static byte[] dateSynchro_request(int size) {

		byte[] table = new byte[size];
		
		SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
		String time = timeformat.format(new Date());
		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy/u");
		String date = dateformat.format(new Date());

		Logger.write("arduinoLinkInterface.Communication.InteractiveRequest.BiteInteractiveActions", LogLevel.VERB,
				"Date et heure courante : " + date + " " + time);

		String[] splittedTime = time.split(":");
		table[0] = Byte.parseByte(splittedTime[0]);
		table[1] = Byte.parseByte(splittedTime[1]);
		table[2] = Byte.parseByte(splittedTime[2]);

		String[] splittedDate = date.split("/");
		table[3] = Byte.parseByte(splittedDate[0]);
		table[4] = Byte.parseByte(splittedDate[1]);
		table[5] = (byte) (Integer.parseInt(splittedDate[2]) / 100);
		table[6] = (byte) (Integer.parseInt(splittedDate[2]) % 100);
		table[7] = (byte) ((Integer.parseInt(splittedDate[3])) - 1);

		return table;
	}

}

