/**
 * 
 */
package arduinoLinkInterface.Communication.InteractiveRequest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import arduinoLinkInterface.FrameManager.CommFrame;
import arduinoLinkInterface.PortManager.SerialPortManager;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.XmlConfigDefinition;
import arduinoXmlConfig.Parser.FrameConfigParser;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataIDNotFoundException;
import arduinoXmlConfig.Parser.DataParser.InteractiveActionParser;

/**
 * Classe gérant une requête du BITE interactif. Elle traite la requete et
 * renvoie la réponse.
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Wed Oct 27 17:49:28     2021 : 41-corrections-affichage-2 
 * 
 *
 */
public class BiteInteractiveRequest {

	/**
	 * Trame à taiter
	 */
	CommFrame frame;

	/**
	 * Référence vers le parser de la trame
	 */
	private FrameConfigParser frameConfigParser;

	/**
	 * Référence evrs le parser des actions du BITE interactif
	 */
	private InteractiveActionParser actionParser;

	/**
	 * Driver du port serie à utiliser
	 */
	private SerialPortManager serialPort;

	/**
	 * Constructeur de la classe
	 * 
	 * @param xmlParser Parser XML
	 * @param frame     Trame recue
	 * @param port      Driver du port série
	 * @throws ReceivedRequestFrameNotValidException
	 * @throws CommonAttributeNotFoundException
	 */
	public BiteInteractiveRequest(XmlConfigParser xmlParser, CommFrame frame, SerialPortManager port)
			throws ReceivedRequestFrameNotValidException, CommonAttributeNotFoundException {
		this.frame = frame;
		this.frameConfigParser = xmlParser.getFrameConfigParser();
		this.actionParser = xmlParser.getInteractiveDataActionsParser();
		this.serialPort = port;

		if (frame.getType() != frameConfigParser.getTypeReq())
			throw new ReceivedRequestFrameNotValidException("la trame reçue n'est pas une requete de l'Arduino.");
	}

	/**
	 * Traite la requete reçue
	 * 
	 * @throws DataIDNotFoundException
	 * @throws DataAttributeNotFoundException
	 * @throws CommonAttributeNotFoundException
	 */
	public void processRequest()
			throws DataIDNotFoundException, DataAttributeNotFoundException, CommonAttributeNotFoundException {

		/* On recherche l'ID de la trame dans le config XML */
		actionParser.findDataByID(frame.getCmd());

		String actionName = actionParser
				.getDataAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.ActionFunctionAttributeName);
		int datasize = Integer.parseInt(
				actionParser.getDataAttribute(XmlConfigDefinition.InteractiveData.ActionsConfig.RetSizeAttributeName));
		
		/* Appel de la fonction à partir de son nom */
		byte[] answer = null;
		try {
			Logger.write(getClass(), LogLevel.INFO,
					"Execution de l'action en reponse a l'ID #" + frame.getCmd() + " : " + actionName);
			Method methodToCall = BiteInteractiveActions.class.getMethod(actionName, int.class);
			answer = (byte[]) methodToCall.invoke(null, datasize);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getCause().toString());
		}

		/* Envoi de la réponse */
		CommFrame answerFrame = new CommFrame((byte) frameConfigParser.getSTX(), (byte) frameConfigParser.getETX());
		answerFrame.setCmd(frame.getCmd());
		answerFrame.setType((byte) frameConfigParser.getTypeAns());
		answerFrame.setStatus((byte) frameConfigParser.getStatusAck());
		answerFrame.createParamTable(datasize);

		if (answer != null) {
			for (int i = 0; i < answer.length; i++)
				answerFrame.setParam(i, answer[i]);
		}

		Logger.write(getClass(), LogLevel.VERB, "Envoi de la reponse a l'ID #" + frame.getCmd());

		answerFrame.computeAndSetChecksum();
		serialPort.writeBytes(answerFrame.getByteTab());

	}



	/**
	 * 
	 * Classe gérant l'exception levée quand la trame de requete recue de l'Arduino
	 * n'est pas valide
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Wed Oct 27 17:49:28     2021 : 41-corrections-affichage-2 
 * 
	 *
	 */
	public class ReceivedRequestFrameNotValidException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Constructeur de l'exception
		 * 
		 * @param s Message d'erreur
		 */
		public ReceivedRequestFrameNotValidException(String s) {
			super("Trame de requete non valide : " + s);
		}
	}

}
