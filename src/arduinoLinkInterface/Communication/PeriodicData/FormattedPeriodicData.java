/**
 * 
 */
package arduinoLinkInterface.Communication.PeriodicData;

import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser.SensorValueType;

/**
 * Classe représentant une donnée périodique formattée pour l'affichage dans le "front"
 * 
 * @author Nicolas
 *
 */
public class FormattedPeriodicData {

	private String title;
	private String data;
	private String suffix;
	private boolean isValid;
	private TimePeriodicData time;
	private SensorValueType type;
	
	/**
	 * @param title
	 * @param data
	 * @param suffix
	 * @param valid
	 * @param time
	 * @param type
	 */
	public FormattedPeriodicData(String title, String data, String suffix, boolean valid, TimePeriodicData time,
			SensorValueType type) {
		super();
		this.title = title;
		this.data = data;
		this.suffix = suffix;
		this.isValid = valid;
		this.time = time;
		this.type = type;
	}

	/**
	 * @param title
	 * @param data
	 * @param suffix
	 * @param valid
	 * @param type
	 */
	public FormattedPeriodicData(String title, String data, String suffix, boolean valid) {
		super();
		this.title = title;
		this.data = data;
		this.suffix = suffix;
		this.isValid = valid;
		this.type = SensorValueType.CURRENT;
	}

	/**
	 * @return the title
	 */
	public String getDisplayTitle() {
		return title;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * @return the isValid
	 */
	public boolean isValid() {
		return isValid;
	}

	/**
	 * @return the type
	 */
	public SensorValueType getType() {
		return type;
	}

	/**
	 * @return the time
	 */
	public TimePeriodicData getTime() {
		return time;
	}

	@Override
	public String toString() {
		return "FormattedPeriodicData [title=" + title + ", data=" + data + ", suffix=" + suffix + ", isValid="
				+ isValid + ", time=" + time + ", type=" + type + "]";
	}


	
}
