/**
 * 
 */
package arduinoLinkInterface.Communication.PeriodicData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import arduinoTools.ArduinoConfigLoader;
import arduinoTools.ArduinoConfigLoader.ArduinoConfigLoaderException;
import arduinoTools.CSVWriter;
import arduinoTools.CSVWriter.CsvWriterFileException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataParserException;
import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser;

/**
 * Classe stockant les valeurs reçues dans un fichier CSV
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Mon May  9 15:45:06     2022 : 81-ajout-parametre-de-configuration 
 * Tue Dec 21 19:45:30     2021 : integration-sonar 
 * Mon Nov 29 23:02:29     2021 : 59-enregistrement-de-la-valeur-des-capteurs 
 * 
 *
 */
public class PeriodicDataCsvWriter {

	/**
	 * référence vers le writer CSV
	 */
	private CSVWriter writer;

	/**
	 * Liste contenant les titres des colonnes
	 */
	private List<String> titlelist;

	/**
	 * Constructeur de la classe
	 */
	public PeriodicDataCsvWriter(PeriodicDataParser parser) {

		/* Récupération du chemin du fichier */
		String path = getCsvFilePathFromConfigFile();
		String fileName = getCsvFileNameFromConfigFile();
		try {
			writer = new CSVWriter(path, fileName);
		} catch (CsvWriterFileException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}

		/* Récupération de la liste des données périodiques configurées */
		titlelist = new ArrayList<>();
		titlelist.add("Heure");
		try {
			parser.resetCurrentDataIndex();
			do {
				if (PeriodicDataManager.isDataDisplayed(parser))
					titlelist.add(parser.getDataAttribute("DisplayTitle"));
			} while (parser.goToNextData());

		} catch (DataParserException | DataAttributeNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}

		try {
			writer.createTitleLine(titlelist);
		} catch (CsvWriterFileException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}

	}

	/**
	 * Récupère le répertoire où placer les fichiers CSV générés dans le fichier de
	 * configuration
	 * 
	 * @return Chemin des fichiers générés
	 */
	private String getCsvFilePathFromConfigFile() {

		String path;
		try {
			path = ArduinoConfigLoader.getConfLoader().getConfigValue("PeriodicDataStoreCsvFilePath");
			Logger.write(getClass(), LogLevel.INFO,
					"Chemin de generation des fichiers CSV trouve, les fichiers seront generes dans le repertoire "
							+ path);
		} catch (ArduinoConfigLoaderException e1) {
			Logger.write(getClass(), LogLevel.WARNING,
					"Chemin de generation des fichiers CSV non trouve, utilisation du chemin par defaut");
			return ".";
		}

		return path;
	}


	/**
	 * Récupère le nom du fichier CSV dans le fichier de configuration
	 * 
	 * @return Nom du fichier
	 */
	private String getCsvFileNameFromConfigFile() {

		String file;
		try {
			file = ArduinoConfigLoader.getConfLoader().getConfigValue("PeriodicDataStoreCsvFileName");
		} catch (ArduinoConfigLoaderException e1) {
			return "arduinoPeriodicData.csv";
		}

		return file;
	}

	public static boolean getCsvActiveFromConfigFile() {
		boolean value;
		try {
			value = ArduinoConfigLoader.getConfLoader().getConfigValueBool("PeriodicDataStoreCsvActive");
		} catch (ArduinoConfigLoaderException e1) {
			return false;
		}

		return value;
	}

	/**
	 * Ferme le fichier CSV
	 */
	public void stop() {
		try {
			writer.close();
		} catch (CsvWriterFileException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}
	}

	/**
	 * Trouve à quel index dans la liste se trouve la donnée
	 * @param text
	 * @return index
	 */
	private int getIndex(String text) {
		int index=0;
		while(!titlelist.get(index).equals(text))
			index++;
			
		return index;
	}

	/**
	 * Ajoute une valeur dans le fichier CSV
	 * 
	 * @param type  Type de capteur associé à la donnée
	 * @param date  Date de réception
	 * @param value Valeur du capteur
	 */
	public void addValue(String type, Date date, String value) {

		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

		String[] line = new String[titlelist.size()];
		for (int i = 0; i < titlelist.size(); i++)
			line[i] = "";

		line[0] = timeFormat.format(date);
		line[getIndex(type)] = value.replace(".", ",");

		try {
			writer.addLine(line);
		} catch (CsvWriterFileException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}
	}
}
