/**
 * 
 */
package arduinoLinkInterface.Communication.PeriodicData;



import java.util.Date;

import arduinoLinkInterface.FrameManager.CommFrame;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.Parser.FrameConfigParser;
import arduinoXmlConfig.Parser.XmlConfigParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataAttributeNotFoundException;
import arduinoXmlConfig.Parser.DataParser.DataParser.DataIDNotFoundException;
import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser;
import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser.PeriodicDataCppConfigNotFoundException;
import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser.SensorValueType;

/**
 * Cette classe gère la réception des données périodiques
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri May  6 19:38:14     2022 : 79-modifications-affichage-arduino 
 * Tue Apr 12 18:28:10     2022 : 76-rendre-generique-le-code-des-donnees-periodiques 
 * Tue Dec 21 19:45:31     2021 : integration-sonar 
 * Mon Nov 29 20:43:26     2021 : 59-enregistrement-de-la-valeur-des-capteurs 
 * 
 *
 */
public class PeriodicDataManager {

	/**
	 * Référence vers le parser de données périodiques
	 */
	private PeriodicDataParser parser;
	
	/**
	 * Référence vers le parser de la trame
	 */
	private FrameConfigParser frameConfigParser;
	
	/**
	 * Référence vers le stockage des donnéées périodiques reçues
	 */
	private PeriodicDataStorage dataStorage;

	/**
	 * Référence vers l'objet de création du fichier CSV
	 */
	private PeriodicDataCsvWriter csvWriter;



	/**
	 * Constructeur de la classe
	 * 
	 * @param xmlParser référence vers le parser xml utilisé
	 */
	public PeriodicDataManager(XmlConfigParser xmlParser) {
		this.parser = xmlParser.getPeriodicDataParser();
		this.frameConfigParser = xmlParser.getFrameConfigParser();
		this.dataStorage = new PeriodicDataStorage();

		if (PeriodicDataCsvWriter.getCsvActiveFromConfigFile())
			this.csvWriter = new PeriodicDataCsvWriter(parser);
		else
			this.csvWriter = null;

	}
	
	/**
	 * Analayse la trame reçue et génère les données pour le "front"
	 * 
	 * @param frame Trame contenant une données périodique
	 * @throws PeriodicDataFrameNotValidException          Quand une erreur est
	 *                                                     trouvée dans la trame
	 * 
	 * @throws CommonAttributeNotFoundException
	 * @throws TimeDataFrameNotValidException
	 * @throws PeriodicDataDateTimeConfigNotFoundException
	 * @throws NumberFormatException
	 */
	public void analyseFrame(CommFrame frame)
			throws PeriodicDataFrameNotValidException, CommonAttributeNotFoundException,
			TimeDataFrameNotValidException, NumberFormatException {

		if(frame.getType() != frameConfigParser.getTypeData())
			throw new PeriodicDataFrameNotValidException("la trame reçue n'est pas une donnée périodique.");

		/* On recherche l'ID de la trame dans le config XML */
		try {
			parser.findDataByID(frame.getCmd());
		} catch (DataIDNotFoundException e1) {
			throw new PeriodicDataFrameNotValidException(e1.getMessage());
		}

		try {
			switch (parser.getServiceType()) {
			case SENSORS:
				processSensorData(frame);
				break;
			case DATE_TIME:
				processDateTimeData(frame);
				break;
			default:
				break;
			}
		} catch (PeriodicDataCppConfigNotFoundException e) {
			Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
		}

	}

	private void processDateTimeData(CommFrame frame) throws TimeDataFrameNotValidException {

		/* Si la donnée est valide */
		if (frame.getStatus() == (byte) 0xFF) {

			/* La table PARAM doit avoir une taille de 8 octets */
			if (frame.getParamTableSize() != 8)
				throw new TimeDataFrameNotValidException(
						"la trame a une taille de " + frame.getParamTableSize() + " au lieu de 8 octets");

			/* Extraction et sauvegarde des données */
			TimePeriodicData lastDate = new TimePeriodicData(frame.getParam(0), frame.getParam(1), frame.getParam(2),
					frame.getParam(3), frame.getParam(4), frame.getParam(5) * 100 + frame.getParam(6),
					frame.getParam(7));
			dataStorage.storeTime(lastDate);

			Logger.write(getClass(), LogLevel.VERB, "Date recue : " + lastDate.toString());
		} else {

			/* Invalidation de la date sauvegardée */
			if (dataStorage.getTimeData() != null)
				dataStorage.getTimeData().setDateInvalid();

			Logger.write(getClass(), LogLevel.VERB, "Date recue invalide");
		}
	}

	/**
	 * Traitement de la trame contenant une donnée de capteur
	 * 
	 * @param frame Trame reçue
	 * 
	 * @throws PeriodicDataFrameNotValidException Si la trame n'est pas valide
	 */
	private void processSensorData(CommFrame frame) throws PeriodicDataFrameNotValidException {

		String title = "";
		String data = "";
		String suffix = "";
		SensorValueType valueType;
		boolean validity = false;
		float coef;
		float arrondi;

		/* On vérifie si cette donnée doit être affichée */
		if (isDataDisplayed(parser)) {
			try {
				/* Récupération de la validité */
				if (frame.getStatus() == (byte) 0xFF)
					validity = true;

				/* Formatage des données */
				title = parser.getDataAttribute("DisplayTitle");

				/* Récupération et conversion du coef */
				coef = Float.parseFloat(parser.getDataAttribute("Coef"));

				/* Récupération de la data */
				if (validity) {
					arrondi = (float) Math.round(frame.getParamIntFormat(2) * coef * 100) / 100;
					data = String.valueOf(arrondi);
				} else
					data = "0";

				getDataSuffix();

				/* Récupération du type de valeur */
				valueType = parser.getCurrentValueType();

			} catch (DataAttributeNotFoundException e) {
				throw new PeriodicDataFrameNotValidException(e.getMessage());
			}

			FormattedPeriodicData lastReceivedData;

			if (valueType == SensorValueType.CURRENT) {
				lastReceivedData = new FormattedPeriodicData(title, data, suffix, validity);
				dataStorage.storeData(parser.getElementID(SensorValueType.CURRENT), lastReceivedData);
				csvWriter.addValue(title, new Date(), data);

				Logger.write(getClass(), LogLevel.VERB, "Donnee recue : " + lastReceivedData.toString());
			} else {
				/* Extraction du timestamp à partir de la trame */
				if (validity) {
					TimePeriodicData ts = new TimePeriodicData(frame.getParam(2), frame.getParam(3), frame.getParam(4));
					lastReceivedData = new FormattedPeriodicData(title, data, suffix, validity, ts, valueType);
					dataStorage.storeData(parser.getElementID(SensorValueType.CURRENT), lastReceivedData);
					Logger.write(getClass(), LogLevel.VERB, "Donnee recue : " + lastReceivedData.toString());
				} else
					Logger.write(getClass(), LogLevel.VERB, "Donnee recue : min/max invalide");
			}
		}
	}
	
	/**
	 * Récupération du suffixe de la donnée à partir de la conf XML
	 * 
	 * @return suffix
	 */
	private String getDataSuffix() {

		String suffix;

		/*
		 * Récupération du suffixe, si il n'est pas défini, on catch l'exception pour ne
		 * pas afficher le message d'erreur et on crée un suffixe vide
		 */
		try {
			suffix = parser.getDataAttribute("DisplaySuffix");
		} catch (DataAttributeNotFoundException e) {
			suffix = "";
		}

		return suffix;
	}

	/**
	 * Stoppe toutes les actions liées aux données périodiques
	 */
	public void stopPeriodicData() {
		csvWriter.stop();
	}

	/**
	 * @return the dataStorage
	 */
	public PeriodicDataStorage getDataStorage() {
		return dataStorage;
	}

	/**
	 * Méthode vérifiant si la donnée périodique doit être affichée ou non
	 * 
	 * @param parser
	 * @return TRUE si la donnée doit être affichée, FALSE sinon
	 */
	public static boolean isDataDisplayed(PeriodicDataParser parser)
	{
		boolean retval = true;
		try {
			String isDisplayedstr = parser.getDataAttribute("isDisplayed");
			if (isDisplayedstr.equals("false"))
				retval = false;
			else
				retval = true;
		} catch (DataAttributeNotFoundException e) {
			/* L'attribut est optionel, on retourne TRUE si il n'existe pas */
			return true;
		}

		return retval;
	}

	/**
	 * 
	 * Classe gérant l'exception levée quand la trame analysée n'est pas valide
	 *
	 */
	public class PeriodicDataFrameNotValidException extends Exception{
		
		private static final long serialVersionUID = 1L;
		
		/**
		 * Constructeur de l'exception
		 * @param s Message d'erreur
		 */
		public PeriodicDataFrameNotValidException(String s) {
			super("Trame de données périodiques non valide : " + s);
		}
	}

	/**
	 * 
	 * Classe gérant l'exception levée quand la trame de date n'est pas valide
	 *
	 */
	public class TimeDataFrameNotValidException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Constructeur de l'exception
		 * 
		 * @param s Message d'erreur
		 */
		public TimeDataFrameNotValidException(String s) {
			super("Trame de date non valide : " + s);
		}
	}
}
