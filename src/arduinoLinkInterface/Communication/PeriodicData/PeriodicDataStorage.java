/**
 * 
 */
package arduinoLinkInterface.Communication.PeriodicData;

import java.util.ArrayList;
import java.util.List;

import arduinoXmlConfig.Parser.DataParser.PeriodicDataParser.SensorValueType;

/**
 * Classe gérant le stockage des données périodiques recues. On stocke la
 * dernière valeur reçue pour chaque donnée périodique.
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Sat Apr 30 19:36:01     2022 : 78-exception-au-lancement-du-mode-automatique 
 * Tue Dec 21 19:45:32     2021 : integration-sonar 
 * 
 *
 */
public class PeriodicDataStorage {

	/**
	 * Table de stockage des données périodiques
	 */
	private List<PeriodicDataStorageElement> storageTable;

	/**
	 * Stockage de la date et heure de l'Arduino
	 */
	private TimePeriodicData timeData;

	/**
	 * Constructeur de la classe
	 * 
	 */
	public PeriodicDataStorage() {
		storageTable = new ArrayList<>();
		timeData = null;
	}

	/**
	 * Enregistre la nouvelle donnée périodique reçue.
	 * 
	 * @param id
	 * @param data Nouvelle donnée péériodique
	 */
	public void storeData(int id, FormattedPeriodicData data) {

		/* Recherche de l'index où est stockée la donnée */
		int i = 0;
		while (i < storageTable.size() && storageTable.get(i).getId() != id)
			i++;

		/*
		 * Si la donnée n'a pas été trouvée, on ajoute un nouvel élément à la liste,
		 * seulement si c'est un type CURRENT
		 */
		if (i == storageTable.size()) {
			if (data.getType() == SensorValueType.CURRENT) {
				PeriodicDataStorageElement newitem = new PeriodicDataStorageElement(data, id);
				storageTable.add(newitem);
			}
		} else {
			switch (data.getType()) {
			case CURRENT:
				storageTable.get(i).setData(data);
				break;
			case MAX:
				storageTable.get(i).setData_max(data);
				break;
			case MIN:
				storageTable.get(i).setData_min(data);
				break;
			default:
				break;
			}
		}

	}

	/**
	 * Enregistre la date et l'heure
	 * 
	 * @param data
	 */
	public void storeTime(TimePeriodicData data) {
		this.timeData = data;
	}

	/**
	 * 
	 * @return Date et heure de l'Arduino
	 */
	public TimePeriodicData getTimeData() {
		return timeData;
	}

	/**
	 * Renvoie l'élément de donnée périodique correspondant à l'ID donné
	 * 
	 * @param id
	 * @return Donnée périodique formatée
	 * @throws PeriodicDataElementNotStoredException Si aucune donnée correspondant
	 *                                               à l'id donné n'est trouvée
	 */
	public PeriodicDataStorageElement getPeriodicData(int id) throws PeriodicDataElementNotStoredException {

		int i = 0;
		while (i < storageTable.size() && storageTable.get(i).getId() != id)
			i++;

		/* Si la donnée n'a pas été trouvée */
		if (i == storageTable.size())
			throw new PeriodicDataElementNotStoredException(id);

		return storageTable.get(i);
	}


	/**
	 * Renvoie une table avec toutes les donnée périodiques
	 * 
	 * @return Table with all stored periodic data
	 */
	public PeriodicDataStorageElement[] getAllPeriodicData() {

		PeriodicDataStorageElement[] tab = new PeriodicDataStorageElement[storageTable.size()];

		for (int i = 0; i < storageTable.size(); i++)
			tab[i] = storageTable.get(i);

		return tab;
	}

	/**
	 * Représente le stockage d'une donnée périodique.
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Sat Apr 30 19:36:01     2022 : 78-exception-au-lancement-du-mode-automatique 
 * Tue Dec 21 19:45:32     2021 : integration-sonar 
 * 
	 *
	 */
	public class PeriodicDataStorageElement {

		/**
		 * Données "utiles" utilisées par les autres classes
		 */
		private FormattedPeriodicData data_current;
		private FormattedPeriodicData data_max;
		private FormattedPeriodicData data_min;

		/**
		 * Identifiant de la donnée périodique
		 */
		private int id;

		/**
		 * @param data_current
		 * @param id
		 */
		public PeriodicDataStorageElement(FormattedPeriodicData data, int id) {
			super();
			this.data_current = data;
			this.id = id;
			this.data_max = null;
			this.data_min = null;
		}

		/**
		 * @return the data
		 */
		public FormattedPeriodicData getData() {
			return data_current;
		}

		/**
		 * @return the data_max
		 */
		public FormattedPeriodicData getData_max() {
			return data_max;
		}

		/**
		 * @return the data_min
		 */
		public FormattedPeriodicData getData_min() {
			return data_min;
		}

		/**
		 * @param data the data to set
		 */
		public void setData(FormattedPeriodicData data) {
			this.data_current = data;
		}

		/**
		 * @param data_max the data_max to set
		 */
		public void setData_max(FormattedPeriodicData data_max) {
			this.data_max = data_max;
		}

		/**
		 * @param data_min the data_min to set
		 */
		public void setData_min(FormattedPeriodicData data_min) {
			this.data_min = data_min;
		}

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}

	}

	/**
	 * Classe gérant l'exception levée quand la donnée recherchée n'est pas stockée
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Sat Apr 30 19:36:01     2022 : 78-exception-au-lancement-du-mode-automatique 
 * Tue Dec 21 19:45:32     2021 : integration-sonar 
 * 
	 *
	 */
	public class PeriodicDataElementNotStoredException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Constructeur de l'exception
		 * 
		 * @param id ID de la donnée recherchée
		 */
		public PeriodicDataElementNotStoredException(int id) {
			super("Aucune donnée périodique avec l'id " + id + " n'a été recue");
		}
	}
}
