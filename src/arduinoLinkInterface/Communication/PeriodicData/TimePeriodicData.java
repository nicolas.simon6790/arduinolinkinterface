/**
 * 
 */
package arduinoLinkInterface.Communication.PeriodicData;

/**
 * Cette classe représente la date et l'heure de l'Arduino reçue par les données
 * périodiques
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri May  6 19:44:25     2022 : 79-modifications-affichage-arduino 
 * 
 *
 */
public class TimePeriodicData {

	private static String[] daysStr = { "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche" };
	private static String[] monthsStr = { "Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout",
			"Septembre", "Octobre", "Novembre", "Decembre" };
	private int hours;
	private int minutes;
	private int seconds;
	private int day;
	private int month;
	private int year;
	private int dayName;

	/**
	 * Flag de validité
	 */
	private boolean isTimeValid;
	private boolean isDateValid;

	/**
	 * Constructeur de la classe
	 */
	public TimePeriodicData() {
		this.hours = 0;
		this.minutes = 0;
		this.seconds = 0;
		this.day = 1;
		this.month = 1;
		this.year = 1990;
		this.dayName = 0;
		this.isTimeValid = false;
		this.isDateValid = false;
	}

	/**
	 * @param hours
	 * @param minutes
	 * @param seconds
	 * @param day
	 * @param month
	 * @param year
	 * @param dayName
	 */
	public TimePeriodicData(int hours, int minutes, int seconds, int day, int month, int year, int dayName) {
		super();
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		this.day = day;
		this.month = month;
		this.year = year;
		this.dayName = dayName;
		this.isTimeValid = true;
		this.isDateValid = true;
	}

	/**
	 * @param hours
	 * @param minutes
	 * @param seconds
	 */
	public TimePeriodicData(int hours, int minutes, int seconds) {
		super();
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		this.day = 1;
		this.month = 1;
		this.year = 1990;
		this.dayName = 0;
		this.isTimeValid = true;
		this.isDateValid = false;
	}

	/**
	 * Defini la date comme invalide
	 */
	public void setDateInvalid() {
		isTimeValid = false;
		isDateValid = false;
	}

	/**
	 * @return the hours
	 */
	public int getHours() {
		return hours;
	}

	/**
	 * @return the minutes
	 */
	public int getMinutes() {
		return minutes;
	}

	/**
	 * @return the seconds
	 */
	public int getSeconds() {
		return seconds;
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}


	/**
	 * @return the dayName
	 */
	public int getDayName() {
		return dayName;
	}

	/**
	 * @return the isTimeValid
	 */
	public boolean isTimeValid() {
		return isTimeValid;
	}

	/**
	 * @return the isDateValid
	 */
	public boolean isDateTimeValid() {
		return (isDateValid && isTimeValid);
	}

	@Override
	public String toString() {
		return "TimePeriodicData [hours=" + hours + ", minutes=" + minutes + ", seconds=" + seconds + ", day=" + day
				+ ", month=" + month + ", year=" + year + ", dayName=" + dayName + ", isTimeValid=" + isTimeValid
				+ ", isDateValid=" + isDateValid + "]";
	}

	/**
	 * Renvoie le nom du jour de la semaine
	 * 
	 * @param day
	 * @return
	 */
	public static String getDayName(int day) {
		return daysStr[day];
	}

	/**
	 * Renvoie le nom du mois
	 * 
	 * @param month
	 * @return
	 */
	public static String getMonthName(int month) {
		return monthsStr[month];
	}
}
