/**
 * 
 */
package arduinoLinkInterface.PortManager;

import arduinoLinkInterface.Communication.CommManager.CommManager;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

/**
 * Classe de reception des données arrivant sur le port série
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Dec 21 19:45:34     2021 : integration-sonar 
 * 
 *
 */
public class SerialPortReader implements SerialPortEventListener {

	/**
	 * Référence vers le portManager qui utilise la classe 
	 */
	private SerialPortManager portManager;
	
	/**
	 * Référence vers l'objet commManager
	 */
	private CommManager commManager;
	
	

	/**
	 * @param portManager
	 * @param commManager
	 */
	public SerialPortReader(SerialPortManager portManager, CommManager commManager) {
		super();
		this.portManager = portManager;
		this.commManager = commManager;
	}



	/**
	 * Méthode appelée quand un évènement se produit sur le port série. Si des données sont reçues sur le bus, 
	 * la méthode les récupère et les transmets à l'objet AutoCommManager. 
	 * 
	 * @param event SerialPortEvent déclenché par le port série quand des données sont reçues sur le bus
	 */
	@Override
	public void serialEvent(SerialPortEvent event) {
		
        if(event.isRXCHAR() && event.getEventValue() > 0) {
            try {
                byte[] receivedData = portManager.getSerialPort().readBytes(event.getEventValue());
                
				String str = "Reception : ";
				for (int i = 0; i < receivedData.length; i++)
					str = str.concat(String.valueOf(receivedData[i])).concat(" ");

				Logger.write(getClass(), LogLevel.VERB, str);
                
                commManager.transferReceivedData(receivedData);
            }
            catch (SerialPortException ex) {
				Logger.write(getClass(), LogLevel.ERROR, "Erreur de reception");
            }
        }
    }
}
