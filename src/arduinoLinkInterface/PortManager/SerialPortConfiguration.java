package arduinoLinkInterface.PortManager;

/**
 * Classe contenant la configuration d'un port série. 
 * 
 * @author Nicolas
 */
public class SerialPortConfiguration {
	
	private String port;
	private int baudrate;
	private int databits;
	private int stopbits;
	private int parity;
	
	/**
	 * Constructeur de la classe SerialPortConfiguration. 
	 * 
	 * @param baudrate
	 * @param databits
	 * @param stopbits
	 * @param parity
	 */
	public SerialPortConfiguration(String port, int baudrate, int databits, int stopbits, int parity) {
		super();
		this.port = port;
		this.baudrate = baudrate;
		this.databits = databits;
		this.stopbits = stopbits;
		this.parity = parity;
	}

	/**
	 * @return the baudrate
	 */
	public int getBaudrate() {
		return baudrate;
	}

	/**
	 * @param baudrate the baudrate to set
	 */
	public void setBaudrate(int baudrate) {
		this.baudrate = baudrate;
	}

	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the databits
	 */
	public int getDatabits() {
		return databits;
	}

	/**
	 * @param databits the databits to set
	 */
	public void setDatabits(int databits) {
		this.databits = databits;
	}

	/**
	 * @return the stopbits
	 */
	public int getStopbits() {
		return stopbits;
	}

	/**
	 * @param stopbits the stopbits to set
	 */
	public void setStopbits(int stopbits) {
		this.stopbits = stopbits;
	}

	/**
	 * @return the parity
	 */
	public int getParity() {
		return parity;
	}

	public String getParityString() {
		String str;

		switch (parity) {
		case 0:
		default:
			str = "NONE";
			break;
		case 1:
			str = "ODD";
			break;
		case 2:
			str = "EVEN";
			break;
		}

		return str;
	}

	/**
	 * @param parity the parity to set
	 */
	public void setParity(int parity) {
		this.parity = parity;
	}

	/** 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Configuration port serie " + port + ": \n"
				+ "Baudrate : " + baudrate + "\n"
				+ "Data bits : " + databits + "\n"
				+ "Stops bits : " + stopbits + "\n"
				+ "Parite : " + parity + "\n";
	}
	
	
}
