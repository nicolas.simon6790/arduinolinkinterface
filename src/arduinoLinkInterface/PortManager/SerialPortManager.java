package arduinoLinkInterface.PortManager;

import arduinoLinkInterface.Communication.CommManager.CommManager;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import jssc.SerialPort;
import jssc.SerialPortException;

/**
 * Classe gérant un port série
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jan 21 11:37:33     2022 : 68-corrections-sonar 
 * Tue Dec 21 20:48:48     2021 : integration-sonar 
 * 
 *
 */
public class SerialPortManager {

	/**
	 * Configuration du port serie
	 */
	private SerialPortConfiguration portConfig;
	
	/**
	 * Reference vers l'objet SerialPort utilisé
	 */
	private SerialPort serialPort;
	
	/**
	 * Référence vers l'objet AutoCommManager
	 */
	private CommManager commManager;

	/**
	 * Indicateur ouverture port
	 */
	private boolean openPort = false ; 
	
	
	/**
	 * @return the openPort
	 */
	public boolean getOpenPort() {
		return openPort;
	}

	/**
	 * @return the serialPort
	 */
	public SerialPort getSerialPort() {
		return serialPort;
	}

	/**
	 * @param portConfig
	 * @param commManager
	 */
	public SerialPortManager(SerialPortConfiguration portConfig, CommManager commManager) {
		super();
		this.portConfig = portConfig;
		this.commManager = commManager;

		openPort();		
		
	}
	
	
	
	/**
	 * Initialise le port série avec la configuration stockée dans portConfig
	 * 
	 */
	private void openPort(){
		
		serialPort = new SerialPort(portConfig.getPort());
		
		try {
			
			serialPort.openPort();
			serialPort.setParams(portConfig.getBaudrate(), 
					portConfig.getDatabits(), 
					portConfig.getStopbits(), 
					portConfig.getParity());
			serialPort.addEventListener(new SerialPortReader(this, commManager), SerialPort.MASK_RXCHAR);
			this.openPort = true ;
			Logger.write(getClass(), LogLevel.INFO,
					"Connexion au port " + portConfig.getPort() + " reussie !\n\n" + portConfig);
		} catch (SerialPortException e) {
			Logger.write(getClass(), LogLevel.ERROR, "Erreur lors de l'ouverture du port " + portConfig.getPort());
			System.err.println("Erreur lors de l'ouverture du port " + portConfig.getPort());
		}

	}
	
	/**
	 * Ecrit une série de bytes sur le port série
	 * 
	 * @param buffer Tableau de bytes à écrire sur le port
	 */
	public void writeBytes(byte[] buffer)
	{
		try {
			String str = "Envoi : ";
			for (int i = 0; i < buffer.length; i++)
				str = str.concat(String.valueOf(buffer[i])).concat(" ");

			Logger.write(getClass(), LogLevel.VERB, str);
			serialPort.writeBytes(buffer);
		} catch (SerialPortException e) {
			Logger.write(getClass(), LogLevel.ERROR,
					"ERREUR : impossible d'ecrire sur le port " + portConfig.getPort());
		}
	}
	
	/**
	 * Libère le port série
	 */
	public void close()
	{
		try {
			serialPort.closePort();
			Logger.write(getClass(), LogLevel.INFO, "Liberation du port " + portConfig.getPort());
		} catch (SerialPortException e) {
			Logger.write(getClass(), LogLevel.ERROR, "ERREUR : impossible de liberer le port " + portConfig.getPort());
		}
	}
	
}
