/**
 * 
 */
package arduinoLinkInterface.FrameManager;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Classe définissant une trame
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Mon May 23 20:13:30     2022 : 84-envoi-requete-avec-parametre-configurable 
 * Tue Jan 18 11:45:12     2022 : 58-reception-de-plusieurs-trames-en-meme-temps 
 * 
 *
 */
public class CommFrame {

	private byte type;
	private byte status;
	private byte cmd;
	private byte size;
	private byte[] param;
	private byte cks;
	
	private byte STX;
	private byte ETX;
	
	private boolean isValid;
	
	/**
	 * Constructeur
	 */
	public CommFrame(byte STX, byte ETX) {
		param = null;
		isValid = false;
		this.STX = STX;
		this.ETX = ETX;
		this.size = 0;
	}

	/**
	 * @param cks the cks to set
	 */
	public void setCks(byte cks) {
		this.cks = cks;
	}

	/**
	 * @return the isValid
	 */
	public boolean isValid() {
		return isValid;
	}

	/**
	 * @return the type
	 */
	public byte getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(byte type) {
		this.type = type;
	}

	/**
	 * @return the status
	 */
	public byte getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(byte status) {
		this.status = status;
	}

	/**
	 * @return the cmd
	 */
	public byte getCmd() {
		return cmd;
	}

	/**
	 * @param cmd the cmd to set
	 */
	public void setCmd(byte cmd) {
		this.cmd = cmd;
	}

	/**
	 * @return the size
	 */
	public byte getSize() {
		return size;
	}



	/**
	 * Instancie une table pour stocker le champ PARAM
	 * @param size Taille de la table
	 */
	public void createParamTable(int size) {
		this.size = (byte) size;
		param = new byte[size];
	}
	
	/**
	 * Retourne la taille de la table PARAM
	 * @return Taille de la table
	 */
	public int getParamTableSize() {
		return param.length;
	}
	
	/**
	 * Retourne la valeur de la case "index" de la table PARAM
	 * @param index 
	 * @return Valeur de la case
	 */
	public byte getParam(int index) {
		return param[index];
	}
	
	/**
	 * Formate les octets de la table PARAM en un entier
	 * 
	 * @param size Taille de la donnée à récupérer
	 * @return Valeur du la table PARAM formatée en integer
	 */
	public int getParamIntFormat(int size) {
		ByteBuffer buffer = ByteBuffer.allocate(4);
		for (int i = 0; i < size; i++)
			buffer.put(3-i, param[i]);
		
		return buffer.getInt(0);
	}
	
	/**
	 * Remplit la case ""index"" de la table PARAM avec ""data""
	 * @param index
	 * @param data
	 */
	public void setParam(int index, byte data) {
		if(index < param.length)
			param[index] = data;
	}

	/**
	 * Remplit la table PARAM avec un int de valeur ""data""
	 * 
	 * @param data
	 */
	public void setParamInt(int data) {
		createParamTable(4);
		byte[] table = ByteBuffer.allocate(4).putInt(data).array();
		param = Arrays.copyOf(table, 4);
	}

	/**
	 * Vérifie si la trame est valide ou non en utilisant la checksum
	 * @return TRUE si la trame est valide, FALSE sinon
	 * @throws FrameNotValidException Quand la trame n'est pas valide
	 */
	public boolean validate() throws FrameNotValidException {
		
		if (computeChecksum() == cks)
			isValid = true;
		else
			throw new FrameNotValidException();
		
		return isValid;
	}
	
	/**
	 * Calcule la CKS de la trame et met à jour l'attribut de la classe
	 */
	public void computeAndSetChecksum() {
		cks = computeChecksum();
	}

	/**
	 * 
	 * @return Checksum de la trame
	 */
	private byte computeChecksum() {
		byte chksum = (byte) (STX + +(byte) 0xFF + ETX + type + status + cmd + size);

		if (param != null) {
			for (int i = 0; i < param.length; i++)
				chksum += param[i];
		}

		return chksum;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CommFrame [type=" + type + ", status=" + status + ", cmd=" + cmd + ", size=" + size + ", param="
				+ Arrays.toString(param) + ", cks=" + cks + ", isValid=" + isValid + "]";
	}

	/**
	 *
	 * @return Trame formatée sous la forme d'un tableau de bytes
	 */
	public byte[] getByteTab() {
		int length = 8 + size;
		byte[] tab = new byte[length];
		tab[0] = STX;
		tab[1] = type;
		tab[2] = status;
		tab[3] = cmd;
		tab[4] = size;
		tab[length - 3] = cks;
		tab[length - 2] = (byte) 0xFF;
		tab[length - 1] = ETX;

		if (size > 0) {
			for (int i = 0; i < param.length; i++)
				tab[5 + i] = param[i];
		}

		return tab;
	}

	/**
	 * Classe gérant l'exception levée quand la trame n'est pas valide, 
	 * c'est à dire que la checksum reçue ne correspond pas à celle calculée.
	 * 
	 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Mon May 23 20:13:30     2022 : 84-envoi-requete-avec-parametre-configurable 
 * Tue Jan 18 11:45:12     2022 : 58-reception-de-plusieurs-trames-en-meme-temps 
 * 
	 *
	 */
	public class FrameNotValidException extends Exception{
		
		private static final long serialVersionUID = 1L;

		/**
		 * Quand la trame reçue n'est pas valide, on enregistre le message d'erreur 
		 * et on marque la trame comme non valide.
		 */
		public FrameNotValidException() {
			super("Trame recue non valide...");
			isValid = false;
		}
	}
	
	
}
