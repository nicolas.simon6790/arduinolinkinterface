/**
 * 
 */
package arduinoLinkInterface.FrameManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import arduinoLinkInterface.FrameManager.CommFrame.FrameNotValidException;
import arduinoTools.Logger;
import arduinoTools.Logger.LogLevel;
import arduinoXmlConfig.Parser.FrameConfigParser;
import arduinoXmlConfig.Parser.DataParser.DataParser.CommonAttributeNotFoundException;

/**
 * Classe reconstituant les trames à partir des tableaux de bytes reçus sur le port série
 * 
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jan 21 21:16:04     2022 : 68-corrections-sonar 
 * Tue Jan 18 11:52:17     2022 : 58-reception-de-plusieurs-trames-en-meme-temps 
 * Tue Dec 21 20:48:47     2021 : integration-sonar 
 * 
 *
 */
public class FrameAssembler {
	
	/**
	 * Position du byte SIZE dans la trame
	 */
	public static final int pos_SIZE = 4;

	/**
	 * Contient tous les octets reçus en cours de traitement.
	 */
	private List<Byte> receptionList;
	
	/**
	 * Flag d'erreur ETX, mis à TRUE si une erreur ETX a déjà été levée
	 */
	private boolean etx_error_flag;
	
	/**
	 * Référence vers le parseur des données de trame
	 */
	private FrameConfigParser frameParser;
	
	/**
	 * File d'attente des trames en attente de traitement
	 */
	private Queue<CommFrame> frameFifo;

	/**
	 * Constructeur de la classe FrameAssembler
	 */
	public FrameAssembler(FrameConfigParser parser)
	{
		receptionList = new ArrayList<>();
		etx_error_flag = false;
		frameParser = parser;
		frameFifo = new LinkedList<>();
	}
	
	/**
	 * Ajoute les octets reçus à la liste et provoque une analyse de la liste pour
	 * détecter une nouvelle trame complète
	 * 
	 * @param byteTab Tableau contenant les octets reçus
	 * @throws FrameConfigAttributeNotFoundException
	 */
	public void addBytesToList(byte[] byteTab)
			throws CommonAttributeNotFoundException
	{
		for (int i = 0; i < byteTab.length; i++)
			receptionList.add(byteTab[i]);

		analyseList();
	}
	
	/**
	 * @return the frameFifo
	 */
	public Queue<CommFrame> getFrameFifo() {
		return frameFifo;
	}

	/**
	 * Remplit la file d'attente des trames en analysant la liste de reception
	 * 
	 * @throws FrameConfigAttributeNotFoundException
	 * 
	 */
	private void analyseList() throws CommonAttributeNotFoundException
	{
		CommFrame newFrame = null;

		do {
			try {
				newFrame = findFrame();
			} catch (ReceptionListNotConsistentException e) {
				Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
				receptionList.clear();
			}

			try {
				if ((newFrame != null) && (newFrame.validate())) {
					frameFifo.add(newFrame);
					Logger.write(getClass(), LogLevel.VERB, "Trame recue : " + newFrame);
				}
			} catch (FrameNotValidException e) {
				Logger.write(getClass(), LogLevel.ERROR, e.getMessage());
			}
		} while (newFrame != null);

	}


	/**
	 * Analyse la liste pour détecter si une trame complète est disponible. Si oui,
	 * construit un nouvel objet de type trame.
	 * 
	 * @return La trame trouvée, null si aucune trame n'est trouvée
	 * @throws CommonAttributeNotFoundException
	 * @throws ReceptionListNotConsistentException
	 */
	private CommFrame findFrame() throws CommonAttributeNotFoundException, ReceptionListNotConsistentException {

		CommFrame frame;

		/* Recherche du premier STX disponible */
		int posSTX = findSTX();
		
		/* On a atteint la fin de la liste et STX n'a pas été trouvé, on quitte la fonction */
		if (posSTX == receptionList.size())
			return null;
		
		/*
		 * Le STX est trouvé, on recherche le ETX
		 *
		 * Position du ETX = STX + 4 (TYPE, STATUS, CMD, SIZE) + SIZE + 3 (CKS + PRE_ETX
		 * + ETX)
		 */
		
		/* Si la fin de la liste est atteinte avant le caractère SIZE, on quitte la fonction */
		byte param_size;
		int posETX;
		if (posSTX + pos_SIZE < receptionList.size()) {
			param_size = receptionList.get(posSTX + pos_SIZE);
			posETX = posSTX + 7 + param_size;
			
			/* Si la fin de la liste est atteinte avant le ETX, on quitte la fonction */
			if(posETX < receptionList.size()) {
				/* On vérifie si le ETX est bien présent */
				if (!checkETX(posETX))
					return null;
			}
			else
				return null;
		}
		else
			return null;
		
	
		/* Une trame est disponible, on supprime ce qui est avant le STX */
		int char_count = 0;
		while(char_count <= posSTX) {
			receptionList.remove(0);
			char_count++;
		}
		
		/* Creation d'un objet CommFrame */
		frame = new CommFrame((byte)frameParser.getSTX(), (byte)frameParser.getETX());
		
		/* Sauvegarde de la trame trouvée */
		
		/* 2e caractere : TYPE */
		byte car = receptionList.remove(0);
		frame.setType(car);
		
		/* 3e caractere : STATUS */
		car = receptionList.remove(0);
		frame.setStatus(car);
		
		/* 4e caractere : CMD */
		car = receptionList.remove(0);
		frame.setCmd(car);

		/* Sauvegarde du tableau PARAM */
		receptionList.remove(0); /* On saute le caractère SIZE */
		frame.createParamTable(param_size);
		for(int i =0; i<param_size;i++)
			frame.setParam(i, receptionList.remove(0));
		
		/* N-1 caractere : CKS */
		car = receptionList.remove(0);
		frame.setCks(car);
		
		return frame;
	}
	
	/**
	 * Recherche du premier caractère STX disponible
	 * 
	 * @return Position du STX
	 * @throws ReceptionListNotConsistentException
	 * @throws CommonAttributeNotFoundException
	 * 
	 * @return Position du STX
	 */
	private int findSTX() throws CommonAttributeNotFoundException, ReceptionListNotConsistentException {

		int charCount = 0;
		while ((charCount < receptionList.size()) && (receptionList.get(charCount) != (byte) frameParser.getSTX())) {
			charCount++;

			/* Si le STX n'est pas trouvé, on lève une exception */
			if (charCount > 256)
				throw new ReceptionListNotConsistentException("Caractere STX non trouve...");
		}

		return charCount;
	}
	
	/**
	 * Vérification de la présence du caractère ETX à l'emplacement prévu
	 * 
	 * @param pos_ETX Position attendue de ETX
	 * @return TRUE si le caractère est trouvé, FALSE sinon
	 * @throws ReceptionListNotConsistentException
	 * @throws CommonAttributeNotFoundException
	 */
	private boolean checkETX(int posETX) throws ReceptionListNotConsistentException, CommonAttributeNotFoundException {

		if (receptionList.get(posETX) != (byte) frameParser.getETX()) {
			/*
			 * On tolère une erreur ETX sans lever d'exception (erreur quasi systématique au
			 * démarrage)
			 */
			if (etx_error_flag)
				throw new ReceptionListNotConsistentException("Caractere ETX non trouve...");
			else {
				etx_error_flag = true;
				receptionList.clear();
				return false;
			}
		}
		else {
			/* On vérifie que le caractère 0xFF est présent avant le ETX */
			if (receptionList.get(posETX - 1) != (byte) 0xFF)
				throw new ReceptionListNotConsistentException("Caractere 0xFF non trouve avant le ETX...");
			else
				return true;
		}
	}




	/**
	 * Classe gérant l'exception levée quand l'analyse de la liste de réception ne se termine pas correctement
	 * 
	 *
	 */
	public class ReceptionListNotConsistentException extends Exception{
	
		private static final long serialVersionUID = 1L;
		
		/**
		 * Quand l'analyse de la liste de réception ne se termine pas correctement, 
		 * on enregistre le message d'erreur et on vide la table de réception.
		 * 
		 * @param s Message d'erreur
		 */
		public ReceptionListNotConsistentException(String s) {
			super("\nErreur d'analyse de la table de reception : " + s);
			receptionList.clear();
		}
	}
	
}


